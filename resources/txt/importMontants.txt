L'importation d'information sur les montants permet d'importer des montants de taxes afin de pouvoir par derrière visualiser et exporter des tableaux d'informations à destination des communes.
La première étape pour l'importation est de récupérer ces montants depuis ADS 2007. Pour cela allez dans la partie "Taxes" sur ADS puis allez sur "Supervision CHORUS - Elements d'information sur les taxes".

.\\resources\\png\\taxes1.png


Afin d'obtenir les montants décidés sur une année, vous devez récupérez les dates d'échéances théoriques des deux prochaines années (cela peut s'avérer long à la quantité de montants enregistrés). Par exemple si vous voulez les montants décidés en 2017 il vous faudra les dates d'échéances comprises entre 2018 et 2019.
Pour récupérer au mieux ces montants, vous pouvez les récupérer par intervalle de 10 jours (comme ADS 2007 n'exporte que 500 lignes au maximum à la fois. (voir image ci-dessous).

.\\resources\\png\\taxes2.png


Placez ensuite tous les fichiers dans un répertoire et cliquez sur le bouton "..." de l'application. Choisissez alors les fichiers CSV précédemment récupérés. Pour réaliser une multi sélection vous pouvez appuyer sur ctrl + clic sur les fichiers voulu ou alors vous pouvez appuyer sur Maj + le dernier fichier de la liste.
Lorsque vous arrivez sur la vue principale, vous pouvez voir la liste des communes. Parmis cette liste vous pouvez définir un groupe de communes qui vous permettra de n'affichez que celles-ci dans les différentes fonctionnalités si vous le voulez.
Pour ajouter une commune au groupe de communes, cochez la case correspondant à la ligne de la commune dans la colonne "Groupe de communes".
En bas à droite vous avez le nombre de communes comprises dans le groupe de communes, et vous pouvez remettre ce groupe à 0 en cliquant sur le bouton "Tout décocher".

A gauche, vous avez le menu, avec les différentes fonctionnalités, ce menu a été déplacé afin d'être rendu plus accessible et afin de rendre l'application plus ergonomique.
Enfin, en bas à gauche vous avez la possibilité de décider si vous enregistrerez les données apportées à l'application lorsque vous déciderez de la fermer.
Pour ne pas enregistrer les données, décochez la case "Sauvegarder les données".

Pour utiliser l'application de manière utiles, commencez par importer une liste de communes, puis une liste de dossiers et/ou de montants.
package com.controler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.model.Menu;
import com.model.*;
import com.view.java.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Kilian Paquier
 * Classe du controler (actions des boutons et menu de la vue)
 */
public class Controler {
    private MainView view;
    private DDT ddt;
    private File cheminCourant;
    private Properties properties;
    private Documentation documentation;
    private JDialog openeddialog;

    /**
     * Contructeur de confort prenant en paramètre le nom de l'application
     *
     * @param name Le nom de l'application
     */
    public Controler(String name) {
        UIManager.put("FileChooser.readOnly", Boolean.TRUE); // On règme des JFileChooser sur de la lecture
        view = new MainView(name); // On crée la vue
        ddt = new DDT(); // On crée la DDT
        documentation = new Documentation("Documentation");
        properties = new Properties();
        initJTree();
        initActions();
        try {
            cheminCourant = new File(".").getCanonicalFile(); // On récupère le chemin courant pour les JFileChooser
            ddt.deserializeCommunes(); // On restaure les données précédentes
            setModelTableCommunes(); // On initialise la table des communes
        } catch (IOException e) {
            ddt.getLogs().append(Arrays.toString(e.getStackTrace())).append("\n");
            new Notification(view, "Erreur", e.getMessage());
        }
    }

    /**
     * Cette fonction permet d'enregistrer les données dans un fichier ser
     *
     * @param bool Le booléan qui permet de dire si on travaille sur le programme en lecture ou en écriture
     */
    public void serializeData(boolean bool) {
        if (bool) {
            try {
                new File(new File(".").getCanonicalFile() + "\\TAGEDOFISlock.lock").delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (view.getDataBox().isSelected()) {
            //view.dispose();
            ViewSave save = new ViewSave("Sauvegarde des données");
            save.open();
            SwingWorker worker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    save.getTimer().start();
                    setProgress(0);
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    setProgress(10);
                    ddt.setLogs(new StringBuilder());
                    setProgress(15);
                    String objet = gson.toJson(ddt);
                    setProgress(35);
                    Writer writer = new OutputStreamWriter(new FileOutputStream("dataTAGEDOFIS.json", false), StandardCharsets.UTF_8);
                    setProgress(45);
                    writer.write(objet);
                    setProgress(75);
                    writer.close();
                    setProgress(90);
                    properties.storeToXML(new FileOutputStream("settings.xml"), "Sauvegarde " + LocalDate.now().toString());
                    setProgress(100);

                    save.getTimer().stop();
                    save.getProgressBar().setValue(100);
                    save.getLabel().setText("Sauvegarde des données veuillez patienter ... Terminé !");
                    save.dispose();
                    System.exit(0);
                    return null;
                }
            };
            worker.addPropertyChangeListener(evt -> save.getProgressBar().setValue(worker.getProgress()));
            worker.execute();
        }
    }

    /**
     * Cette fonction retourne la vue
     *
     * @return La vue de l'application
     */
    public MainView getView() {
        return view;
    }

    /**
     * Retourne le titre de l'application
     *
     * @return Le titre de l'app
     */
    public String getAppTitle() {
        return ddt.getTitreApp();
    }

    /**
     * Cette fonction initialise toutes les actions de l'application
     */
    private void initActions() {
        /*
        On ajoute le mouse adapter pour la modification d'une commune (EPCI ou non)
        */
        view.getTableCommunes().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int column = view.getTableCommunes().getSelectedColumn(); // On regarde la colonne sélectionnée
                if (column == 3) { // Si c'est celle définissant l'EPCI
                    int row = view.getTableCommunes().getSelectedRow(); // On regarde la ligne (commune) sélectionnée
                    Object bool = view.getTableCommunes().getValueAt(row, column);
                    String numeroINSEE = String.valueOf(view.getTableCommunes().getValueAt(row, 1)); // On recupère le numéro INSEE
                    String department = String.valueOf(view.getTableCommunes().getValueAt(row, 0)); // On récupère le département
                    if (bool.toString().equals("false")) { // Si la commune passe en non EPCI
                        ddt.rechercherCommuneINSEE(numeroINSEE, department).setEpci(false); // On recherche la commune et on set l'EPCI (booléen)
                        int defaut = Integer.valueOf(view.getNbCommunesEPCI().getText());
                        defaut--;
                        int nb = ddt.nbECPI();
                        if (defaut != nb)
                            view.getNbCommunesEPCI().setText(String.valueOf(nb));
                        else
                            view.getNbCommunesEPCI().setText(String.valueOf(defaut));
                    } else {
                        ddt.rechercherCommuneINSEE(numeroINSEE, department).setEpci(true);
                        int defaut = Integer.valueOf(view.getNbCommunesEPCI().getText());
                        defaut++;
                        int nb = ddt.nbECPI();
                        if (defaut != nb)
                            view.getNbCommunesEPCI().setText(String.valueOf(nb));
                        else
                            view.getNbCommunesEPCI().setText(String.valueOf(defaut));
                    }
                }
            }
        });

        view.getToutDecocherButton().addActionListener(e -> {
            for (Commune commune : ddt.getCommunes()) {
                commune.setEpci(false);
            }
            setModelTableCommunes();
        });

        try {
            properties.loadFromXML(new FileInputStream("settings.xml"));
        } catch (IOException e) {
            ddt.getLogs().append(Arrays.toString(e.getStackTrace())).append("\n");
            new Notification(view, "Erreur", "Les paramètres de chemins n'ont pas pu être chargés");
        }
    }

    /**
     * Fonction permettant d'ajouter une exception à une commune
     *
     * @param viewExceptions La fenêtre contenant les informations de l'exception
     * @throws IOException une exception si il n'y a aucune commune
     */
    private void ajouterException(ViewExceptions viewExceptions) throws IOException {
        if (ddt.getCommunes().size() == 0)
            throw new IOException("Il n'existe pas de communes");
        viewExceptions.getFieldNumber().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if ((e.getKeyCode() < KeyEvent.VK_NUMPAD0 || e.getKeyCode() > KeyEvent.VK_NUMPAD9) && e.getKeyCode() != 8 && e.getKeyCode() != KeyEvent.VK_ENTER)
                    viewExceptions.getFieldNumber().setText(viewExceptions.getFieldNumber().getText().substring(0, viewExceptions.getFieldNumber().getText().length() - 1));
                if (viewExceptions.getFieldNumber().getText().length() > 4)
                    viewExceptions.getFieldNumber().setText(viewExceptions.getFieldNumber().getText().substring(0, viewExceptions.getFieldNumber().getText().length() - 1));
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    viewExceptions.getExceptionButton().doClick();
            }
        });

        viewExceptions.getExceptionButton().addActionListener(e -> {
            Commune commune = ddt.rechercherCommuneINSEE(viewExceptions.getBoxCommunes().getSelectedItem().toString().split(" ")[1], viewExceptions.getBoxCommunes().getSelectedItem().toString().split(" ")[0]);
            if (viewExceptions.getFieldNumber().getText().equals(""))
                new Notification(viewExceptions, "Information", "Vous n'avez pas défini de numéro de début pour les dossiers");
            else {
                NumberException numberException = new NumberException(viewExceptions.getFieldNumber().getText(), viewExceptions.getBoxType().getSelectedItem().toString());
                if (commune.exceptionExists(numberException) == null) {
                    commune.addException(numberException);
                    new Notification(viewExceptions, "Information", "Exception correctement ajoutée");
                    viewExceptions.getFieldNumber().setText("");
                    viewExceptions.getBoxType().setSelectedIndex(0);
                } else
                    new Notification(viewExceptions, "Information", "L'exception pour ce type et cette commune existe déjà");
            }
        });

        viewExceptions.ouverture();
    }

    /**
     * Cette fonction définie les paramètres d'ouverture d'une JDialog
     *
     * @param dialog La JDialog à ouvrir
     */
    private void openDialog(JDialog dialog) {
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialog.pack();
        dialog.setSize(750, 500);
        dialog.setLocationRelativeTo(view);
        dialog.setVisible(true);
    }

    /**
     * Cette fonction permet de modifier l'entête de l'exportation des fichiers
     *
     * @param viewParam La vue contenant les nouveaux paramètres d'exportation
     */
    private void modifierEntete(ViewParam viewParam) {
        viewParam.getModifierLEnteteDButton().addActionListener(e -> {
            ddt.setEntete(viewParam.getTextFieldEntete().getText() + "\n" + viewParam.getTextFieldProvenance().getText() + "\n");
            ddt.setTitreApp(viewParam.getTextFieldTitre().getText() + viewParam.getTextFieldTitre2().getText());
            view.setTitle(ddt.getTitreApp());
            new Notification(viewParam, "Information", "Entête modifiée");
        });

        viewParam.getTextFieldEntete().setText(ddt.getEntete().split("\n")[0]);
        viewParam.getTextFieldProvenance().setText(ddt.getEntete().split("\n")[1]);
        viewParam.getTextFieldTitre().setText(ddt.getTitreApp().substring(0, 12));
        viewParam.getTextFieldTitre2().setText(ddt.getTitreApp().substring(12));
        viewParam.getFieldVersion().setText(viewParam.getFieldVersion().getText() + " - " + ddt.getVersion());

        //openDialog(viewParam);
    }

    /**
     * Cette fonction permet d'ajouter une commune à la liste des communes
     *
     * @param viewAddCommune La vue contenant les informations sur la commune à ajouter
     */
    private void ajouterCommuneComponent(ViewAddCommune viewAddCommune) {

        viewAddCommune.getAjouterLaCommuneButton().addActionListener(e -> {
            Boolean isEPCI = viewAddCommune.getEPCICheckBox().isSelected();
            String nomCommune = viewAddCommune.getNomCommuneField().getText();
            String numeroINSEE = viewAddCommune.getNumeroINSEEField().getText();
            String departement = viewAddCommune.getDepartementField().getText();
            if (numeroINSEE.length() == 3 && departement.length() == 3) {
                if (ddt.rechercherCommuneINSEE(numeroINSEE, departement) != null)
                    new Notification(viewAddCommune, "Information", "La commune existe déjà");
                else {
                    ddt.ajouterCommune(new Commune(numeroINSEE, nomCommune, departement, isEPCI));
                    ddt.triCommunes();
                    setModelTableCommunes();

                    viewAddCommune.getDepartementField().setText("");
                    viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                    viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                    viewAddCommune.getNumeroINSEEField().setText("");
                    viewAddCommune.getNomCommuneField().setText("");
                    viewAddCommune.getEPCICheckBox().setSelected(false);
                }
            } else
                new Notification(viewAddCommune, "Information", "Veuillez vérifier que le département et le numéro INSEE soient composés de trois chiffres");
        });

        viewAddCommune.getNumeroINSEEField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (viewAddCommune.getNumeroINSEEField().getText().length() > 3) {
                    String text = viewAddCommune.getNumeroINSEEField().getText().substring(0, viewAddCommune.getNumeroINSEEField().getText().length() - 1);
                    viewAddCommune.getNumeroINSEEField().setText(text);
                }

                if (e.getKeyChar() < '0' || e.getKeyChar() > '9') {
                    if (e.getKeyCode() != 8) {
                        String text = viewAddCommune.getNumeroINSEEField().getText().substring(0, viewAddCommune.getNumeroINSEEField().getText().length() - 1);
                        viewAddCommune.getNumeroINSEEField().setText(text);
                    }
                }

                if (viewAddCommune.getNumeroINSEEField().getText().length() == 3 && viewAddCommune.getDepartementField().getText().length() == 3) {
                    if (ddt.rechercherCommuneINSEE(viewAddCommune.getNumeroINSEEField().getText(), viewAddCommune.getDepartementField().getText()) != null) {
                        viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.RED));
                        viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.RED));
                    } else {
                        viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.GREEN));
                        viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.GREEN));
                    }
                }

                if (viewAddCommune.getNumeroINSEEField().getText().length() < 3 || viewAddCommune.getDepartementField().getText().length() < 3) {
                    viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                    viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                }
            }
        });


        viewAddCommune.getDepartementField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (viewAddCommune.getDepartementField().getText().length() > 3) {
                    String text = viewAddCommune.getDepartementField().getText().substring(0, viewAddCommune.getDepartementField().getText().length() - 1);
                    viewAddCommune.getDepartementField().setText(text);
                }

                if (e.getKeyChar() < '0' || e.getKeyChar() > '9') {
                    if (e.getKeyCode() != 8) {
                        String text = viewAddCommune.getDepartementField().getText().substring(0, viewAddCommune.getDepartementField().getText().length() - 1);
                        viewAddCommune.getDepartementField().setText(text);
                    }
                }

                if (viewAddCommune.getNumeroINSEEField().getText().length() == 3 && viewAddCommune.getDepartementField().getText().length() == 3) {
                    if (ddt.rechercherCommuneINSEE(viewAddCommune.getNumeroINSEEField().getText(), viewAddCommune.getDepartementField().getText()) != null) {
                        viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.RED));
                        viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.RED));
                    } else {
                        viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.GREEN));
                        viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.GREEN));
                    }
                }

                if (viewAddCommune.getNumeroINSEEField().getText().length() < 3 || viewAddCommune.getDepartementField().getText().length() < 3) {
                    viewAddCommune.getNumeroINSEEField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                    viewAddCommune.getDepartementField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
                }
            }
        });


    }

    /**
     * Cette fonction permet de définir les fields à chaque nouvelle recherche de dossier à modifier
     * Elle permet aussi de modifier le dossier à modifier
     *
     * @param viewModifierDossier La vue des fields avec les informations sur le dossier à modifier
     */
    private void modifierDossier(ViewModifierDossier viewModifierDossier) {
        List<String> historiqueRecherche = new ArrayList<>();
        AtomicReference<String> numeroDossier = new AtomicReference<>("");
        historiqueRecherche.add(numeroDossier.toString());

        AtomicReference<Integer> positionHistorique = new AtomicReference<>(0);

        AtomicReference<String> ancienNumeroDossier = new AtomicReference<>();

        /*
        Initialisation des actions de la CheckBox du nom du demandeur
         */
        viewModifierDossier.getModifiableCheckBoxDemandeur().addChangeListener(e1 -> {
            if (viewModifierDossier.getModifiableCheckBoxDemandeur().isSelected()) { // Si la check box est selectionnée
                viewModifierDossier.getDemandeurField().setEnabled(true); // On ouvre le copier coller
                viewModifierDossier.getDemandeurField().setEditable(true); // On ouvre l'édition
            } else {
                viewModifierDossier.getDemandeurField().setEnabled(false); // Sinon les copier coller et éditions sont fermées
                viewModifierDossier.getDemandeurField().setEditable(false);
            }
        });

        viewModifierDossier.getNumeroDossierField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (viewModifierDossier.getNumeroDossierField().getText().length() > 5)
                    viewModifierDossier.getNumeroDossierField().setText(viewModifierDossier.getNumeroDossierField().getText().substring(0, 5));
                if (viewModifierDossier.getNumeroDossierField().getText().length() == 5) {
                    try {
                        if (ddt.rechercherDossier(viewModifierDossier.getNumDossierField().getText().replaceAll(" ", "") + viewModifierDossier.getNumeroDossierField().getText()) != null) {
                            viewModifierDossier.getNumeroDossierField().setBorder(BorderFactory.createLineBorder(Color.RED));
                        } else
                            viewModifierDossier.getNumeroDossierField().setBorder(BorderFactory.createLineBorder(Color.GREEN));
                    } catch (IOException e1) {
                        ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                        new Notification(viewModifierDossier, "Erreur", e1.getMessage());
                    }
                } else
                    viewModifierDossier.getNumeroDossierField().setBorder(BorderFactory.createLineBorder(Color.lightGray));
            }
        });

        /*
        Initialisation des actions de la checkBox de l'état du dossier
         */
        viewModifierDossier.getModifiableCheckBoxEtat().addChangeListener(e1 -> {
            if (viewModifierDossier.getModifiableCheckBoxEtat().isSelected()) { // Si la check box est selectionnée
                viewModifierDossier.getEtatDossierField().setEnabled(true);
                viewModifierDossier.getEtatDossierField().setEditable(true);
            } else {
                viewModifierDossier.getEtatDossierField().setEnabled(false);
                viewModifierDossier.getEtatDossierField().setEditable(false);
            }
        });

        /*
        Initialisation des actions de la checkBox du numéro du dossier
         */
        viewModifierDossier.getModifiableCheckBoxNum().addChangeListener(e1 -> {
            if (viewModifierDossier.getModifiableCheckBoxNum().isSelected()) { // Si la check box est selectionnée
                viewModifierDossier.getNumeroDossierField().setEnabled(true);
                viewModifierDossier.getNumeroDossierField().setEditable(true);
            } else {
                viewModifierDossier.getNumeroDossierField().setEnabled(false);
                viewModifierDossier.getNumeroDossierField().setEditable(false);
            }
        });

        viewModifierDossier.getRechercherField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    if (positionHistorique.get() > 0) {
                        positionHistorique.set(positionHistorique.get() - 1);
                        viewModifierDossier.getRechercherField().setText(historiqueRecherche.get(positionHistorique.get()));
                    }
                }

                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    if (positionHistorique.get() < historiqueRecherche.size() - 1) {
                        positionHistorique.set(positionHistorique.get() + 1);
                        viewModifierDossier.getRechercherField().setText(historiqueRecherche.get(positionHistorique.get()));
                    }
                }
            }
        });

        /*
        Initialisation de la recherche d'un dossier à modifier
         */
        viewModifierDossier.getRechercherButton().addActionListener(e -> {
            numeroDossier.set(viewModifierDossier.getRechercherField().getText());
            try {
                viewModifierDossier.getModifiableCheckBoxDemandeur().setSelected(false);
                viewModifierDossier.getModifiableCheckBoxEtat().setSelected(false);
                viewModifierDossier.getModifiableCheckBoxNum().setSelected(false);
                viewModifierDossier.getNumeroDossierField().setText("");
                viewModifierDossier.getNumDossierField().setText("");
                viewModifierDossier.getDemandeurField().setText("");
                viewModifierDossier.getEtatDossierField().setText("");

                Dossier dossier;
                if (numeroDossier.get().length() != 15)
                    throw new IOException("Veuillez saisir le numéro de dossier complet"); // Le dossier doit avoir une taille de 15 caractères
                dossier = ddt.rechercherDossier(numeroDossier.get());
                if (dossier == null)
                    throw new IOException("Dossier inexistant"); // Le dossier n'existe pas donc pas besoin d'aller plus loin

                viewModifierDossier.getDemandeurField().setText(dossier.getNomDemandeur()); // On règle le texte du nom du demandeur
                viewModifierDossier.getEtatDossierField().setText(dossier.getEtatDossier()); // On règle le texte de l'état du dossier
                viewModifierDossier.getNumDossierField().setText(dossier.getNumeroDossier().substring(0, 14));
                viewModifierDossier.getNumeroDossierField().setText(dossier.getNumeroDossier().substring(14)); // On règle le text du numéro de dossier
                ancienNumeroDossier.set(dossier.getNumeroDossier());

                viewModifierDossier.getRechercherField().setText("");
                viewModifierDossier.getNumeroDossierField().setBorder(BorderFactory.createLineBorder(Color.RED));
                if (!historiqueRecherche.get(historiqueRecherche.size() - 1).equals(numeroDossier.toString())) {
                    historiqueRecherche.add(numeroDossier.toString());
                    positionHistorique.set(historiqueRecherche.size());
                }
            } catch (IOException e1) {
                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                new Notification(viewModifierDossier, "Erreur", e1.getMessage());
            }
        });

        /*
        Action de recherche d'un dossier à l'aide de la touche entrée
         */
        viewModifierDossier.getRechercherField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    viewModifierDossier.getRechercherButton().doClick();
                }
            }
        });

        /*
        Action de la modification d'un dossier
         */
        viewModifierDossier.getModifierButton().addActionListener(e -> {
            if (viewModifierDossier.getNumeroDossierField().getText().equals("")) { // Si aucun dossier n'a été recherché
                new Notification(viewModifierDossier, "Erreur", "Vous n'avez pas recherché de numéro de dossier");
            } else {
                try {
                    Dossier dossier = ddt.rechercherDossier(ancienNumeroDossier.toString().replaceAll(" ", "")); // On récupère le dossier
                    if (viewModifierDossier.getNumeroDossierField().getText().length() < 5)
                        throw new IOException("Numéro de dossier impossible");
                    if (ddt.rechercherDossier(viewModifierDossier.getNumDossierField().getText().replaceAll(" ", "") + viewModifierDossier.getNumeroDossierField().getText()) != null)
                        throw new IOException("Le dossier existe déjà");
                    dossier.setEtatDossier(viewModifierDossier.getEtatDossierField().getText()); // On change l'état même s'il n'a pas été modifié
                    dossier.setNomDemandeur(viewModifierDossier.getDemandeurField().getText()); // On change le nom du demandeur même s'il n'a pas été modifié
                    dossier.setNumeroDossier(viewModifierDossier.getNumDossierField().getText() + viewModifierDossier.getNumeroDossierField().getText()); // On change le numéro du dossier même s'il n'a pas été modifié

                    new Notification(viewModifierDossier, "Erreur", "Modification du dossier terminée");
                    viewModifierDossier.getRechercherField().setText("");
                    viewModifierDossier.getEtatDossierField().setText("");
                    viewModifierDossier.getNumeroDossierField().setText("");
                    viewModifierDossier.getDemandeurField().setText("");
                    viewModifierDossier.getNumDossierField().setText("");
                } catch (IOException e1) {
                    ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                    new Notification(viewModifierDossier, "Erreur", e1.getMessage());
                }
            }
        });
    }

    /**
     * Cette fonction permet de modifier le nom d'une commune
     *
     * @param viewModifierCommune La vue contenant les communes et la nouveau non de la commune
     * @throws IOException une exception s'il n'y a aucune commune
     */
    private void modifierCommune(ViewModifierCommune viewModifierCommune) throws IOException {
        if (ddt.getCommunes().size() == 0)
            throw new IOException("Il n'existe pas de communes");

        initComboBoxCommunes(viewModifierCommune.getCommuneBox());

        Commune commune = ddt.rechercherCommuneINSEE(viewModifierCommune.getCommuneBox().getSelectedItem().toString().split(" ")[1], viewModifierCommune.getCommuneBox().getSelectedItem().toString().split(" ")[0]);
        viewModifierCommune.getNumeroINSEEField().setText(commune.getNumeroINSEE());
        viewModifierCommune.getDepartementField().setText(commune.getDepartment());
        viewModifierCommune.getNomField().setText(commune.getNomCommune());

        viewModifierCommune.getCommuneBox().addItemListener(e -> {
            if (viewModifierCommune.getCommuneBox().getSelectedItem() != null) {
                Commune commune1 = ddt.rechercherCommuneINSEE(viewModifierCommune.getCommuneBox().getSelectedItem().toString().split(" ")[1], viewModifierCommune.getCommuneBox().getSelectedItem().toString().split(" ")[0]);
                viewModifierCommune.getNumeroINSEEField().setText(commune1.getNumeroINSEE());
                viewModifierCommune.getDepartementField().setText(commune1.getDepartment());
                viewModifierCommune.getNomField().setText(commune1.getNomCommune());
            }
        });

        viewModifierCommune.getModifierLaCommuneButton().addActionListener(e -> {
            Commune commune2 = ddt.rechercherCommuneINSEE(viewModifierCommune.getNumeroINSEEField().getText(), viewModifierCommune.getDepartementField().getText());
            commune2.setNomCommune(viewModifierCommune.getNomField().getText());

            setModelTableCommunes();
            initComboBoxCommunes(viewModifierCommune.getCommuneBox());
            new Notification(viewModifierCommune, "Erreur", "Nom de la commune modifée");
        });

        //openDialog(viewModifierCommune);
    }

    /**
     * Cette fonction permet de supprimer un dossier
     *
     * @throws IOException Si le dossier n'existe pas ou qu'il n'est pas complet
     */
    private void supprimerDossier() throws IOException {
        Object numeroDossier = JOptionPane.showInputDialog(view, "Numéro du dossier à supprimer (sans espaces) : ", "Supprimer un dossier", JOptionPane.QUESTION_MESSAGE);
        if (numeroDossier != null) {
            if (String.valueOf(numeroDossier).length() != 15)
                throw new IOException("Veuillez remplir l'intégralité du numéro de dossier"); // On saisi l'intégralité du dossier sans espace
            ddt.supprimerDossier(String.valueOf(numeroDossier));
            new Notification(view, "Erreur", "Suppression réalisée");
        }
    }

    /**
     * Cette fonction permet de supprimer une commune ainsi que toutes les informations qu'elle contenait
     *
     * @throws IOException une exception s'il n'y a aucune communes
     */
    private void supprimerCommune() throws IOException {
        if (ddt.getCommunes().size() == 0)
            throw new IOException("Il n'existe pas de communes");
        String[] listeCommunes = new String[ddt.getCommunes().size()];
        int index = 0;
        for (Commune commune : ddt.getCommunes()) {
            listeCommunes[index] = commune.getDepartment() + commune.getNumeroINSEE() + " " + commune.getNomCommune();
            index++;
        }

        Object choixSuppression = JOptionPane.showInputDialog(view, "Choix de la commune à supprimer", "Supprimer une commune", JOptionPane.QUESTION_MESSAGE, null,
                listeCommunes, listeCommunes[0]);
        if (choixSuppression != null) {
            String numeroINSEE = choixSuppression.toString().split(" ")[0].substring(3, 6);
            String departement = choixSuppression.toString().split(" ")[0].substring(0, 3);
            ddt.supprimerCommune(numeroINSEE, departement);
            setModelTableCommunes();

            new Notification(view, "Erreur", "La commune a été supprimée");
        }
    }

    /**
     * Cette fonction permet de supprimer une exception
     *
     * @throws IOException une exception
     */
    private void supprimerException() throws IOException {
        List<String> exceptions = new ArrayList<>();
        int index = 0;
        for (Commune commune : ddt.getCommunes()) {
            if (commune.getExceptions() == null)
                continue;
            for (NumberException exception : commune.getExceptions()) {
                exceptions.add(commune.getDepartment() + commune.getNumeroINSEE() + " " + commune.getNomCommune() + " " + exception.getType() + " : " + exception.getNumber());
            }
        }

        String[] listeException = new String[exceptions.size()];
        for (String exception : exceptions) {
            listeException[index] = exception;
            index++;
        }

        if (listeException.length == 0)
            throw new IOException("Pas d'exceptions à supprimer");
        Object choixSuppression = JOptionPane.showInputDialog(view, "Choix de la commune à supprimer", "Supprimer une commune", JOptionPane.QUESTION_MESSAGE, null,
                listeException, listeException[0]);
        if (choixSuppression != null) {
            String numeroINSEE = choixSuppression.toString().split(" ")[0].substring(3, 6);
            String departement = choixSuppression.toString().split(" ")[0].substring(0, 3);
            String typeException = choixSuppression.toString().split(" ")[2];
            Commune commune = ddt.rechercherCommuneINSEE(numeroINSEE, departement);
            commune.getExceptions().remove(commune.getException(typeException));
            new Notification(view, "Information", "L'exception du type \"" + typeException + "\" pour \"" + commune.getNomCommune() + "\" a été supprimée");
        }
    }

    /**
     * Cette fonction permet de supprimer une année complète de dossiers si l'utilisateur n'en a plus besoin
     */
    private void supprimerListeDossiers() {
        // Selection de l'année traitée
        Object[] annees = new Object[10];
        LocalDate date = LocalDate.now();
        int index = 0;
        for (int i = date.getYear(); i > date.getYear() - 10; i--) { // On initialise sur les 10 dernières années
            annees[index] = i;
            index++;
        }
        Object selection = JOptionPane.showInputDialog(view, "Saisir l'année à supprimer", "Supprimer une année de dossiers", JOptionPane.QUESTION_MESSAGE, null,
                annees, annees[0]);

        if (selection != null) {
            ddt.supprimerListeDossiers(String.valueOf(selection));
            new Notification(view, "Information", "Suppression réalisée");
        }
    }

    /**
     * Cette fonction permet de supprimer une année complète d'informations sur les dossiers taxés
     */
    private void supprimerAnneeInfo() {
        // Selection de l'année traitée
        Object[] annees = new Object[10];
        LocalDate date = LocalDate.now();
        int index = 0;
        for (int i = date.getYear(); i > date.getYear() - 10; i--) { // On initialise sur les 10 dernières années
            annees[index] = i;
            index++;
        }
        Object selection = JOptionPane.showInputDialog(view, "Saisir l'année à supprimer", "Supprimer une année d'informations TA", JOptionPane.QUESTION_MESSAGE, null,
                annees, annees[0]);

        if (selection != null) {
            ddt.supprimerListeInfo(selection.toString());
            new Notification(view, "Information", "Suppression réalisée");
        }
    }

    /**
     * Cette fonction permet d'importer une liste de commune à l'aide d'un fichier csv contenant le département (trois chiffres)
     * avec le numéro INSEE (trois chiffres) dans une colonne et le nom de la commune dans la seconde colonne
     */
    private void importerCommunes() {
        AtomicReference<String> fichierCommune = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Importation d'une liste de communes", false);
        ((ViewImport) openeddialog).getFieldAnnee().setVisible(false);
        ((ViewImport) openeddialog).getComboBoxAnnee().setVisible(false);

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Texte CSV (*.csv)", "csv");

            // Choix du fichier des communes
            JFileChooser fichierCommunes = new JFileChooser();
            fichierCommunes.setDialogTitle("Choix du fichier des communes");
            fichierCommunes.setCurrentDirectory(properties.getProperty("importCommunes").equals("") || !new File(properties.getProperty("importCommunes")).exists() ? cheminCourant : new File(properties.getProperty("importCommunes")));
            fichierCommunes.setAcceptAllFileFilterUsed(false);
            fichierCommunes.addChoosableFileFilter(fileFilter);
            int valeur = fichierCommunes.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                properties.setProperty("importCommunes", fichierCommunes.getSelectedFile().getParent());
                fichierCommune.set(fichierCommunes.getSelectedFile().toString());
                ((ViewImport) openeddialog).getTextFieldFiles().setText(fichierCommune.get());
            }
        });

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichierCommune.get() != null) {
                try {
                    ddt.parseCommunes(fichierCommune.toString());
                    new Notification(view, "Information", "Importation des communes terminée");
                    setModelTableCommunes();
                    openeddialog.dispose();
                } catch (IOException e1) {
                    ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                    new Notification(view, "Erreur", e1.getMessage());
                }
            } else {
                new Notification(view, "Information", "Vous n'avez pas ouvert de fichier communes");
            }
        });
        ((ViewImport) openeddialog).ouvrirPetit();
    }

    /**
     * Cette fonction permet d'importer une liste de dossiers depuis un fichier csv
     */
    private void importerDossiers() {
        AtomicReference<File[]> fichiersDossiers = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Importation d'une liste de dossiers", false);
        initComboBoxYear(((ViewImport) openeddialog).getComboBoxAnnee());

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Texte CSV (*.csv)", "csv");

            // Choix des fichiers contenant les dossiers
            JFileChooser fichiersCSV = new JFileChooser();
            fichiersCSV.setDialogTitle("Choix des fichiers contenant les dossiers à importer");
            fichiersCSV.setCurrentDirectory(properties.getProperty("importDossiers").equals("") || !new File(properties.getProperty("importDossiers")).exists() ? cheminCourant : new File(properties.getProperty("importDossiers")));
            fichiersCSV.setMultiSelectionEnabled(true);
            fichiersCSV.setAcceptAllFileFilterUsed(false);
            fichiersCSV.addChoosableFileFilter(fileFilter);
            int valeur = fichiersCSV.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                // On récupères tous les fichiers
                File[] files = fichiersCSV.getSelectedFiles();
                properties.setProperty("importDossiers", files[0].getParent());
                fichiersDossiers.set(files);
                ((ViewImport) openeddialog).getTextFieldFiles().setText(Arrays.toString(files));
            }
        });

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichiersDossiers.get() != null) {
                for (File file : fichiersDossiers.get()) { // On parcours les fichiers qu'il y en ai un ou trois cents
                    try {
                        ddt.parseDossiers(file.toString(), ((ViewImport) openeddialog).getComboBoxAnnee().getSelectedItem().toString().substring(2, 4));
                    } catch (IOException e1) {
                        ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e1.getMessage());
                    }
                }
                ddt.getMapImportExport().put("ImportDossier", LocalDate.now());
                new Notification(view, "Information", "Importation des dossiers terminée");
                ((ViewImport) openeddialog).getTextFieldFiles().setText("");
                fichiersDossiers.set(null);
            } else {
                new Notification(view, "Information", "Aucun fichier à importer");
            }
        });
        ((ViewImport) openeddialog).ouvrir();
    }

    /**
     * Cette fonction permet d'importer une liste de dossiers taxés depuis un fichier csv
     */
    private void importerTaxes() {
        AtomicReference<File[]> fichierTaxes = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Importation d'une liste d'information de montants", false);
        ((ViewImport) openeddialog).getFieldAnnee().setVisible(false);
        ((ViewImport) openeddialog).getComboBoxAnnee().setVisible(false);

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Texte CSV (*.csv)", "csv");

            // Choix des fichiers contenant les dossiers
            JFileChooser fichiersCSV = new JFileChooser();
            fichiersCSV.setDialogTitle("Choix des fichiers contenant les dossiers taxés à importer");
            fichiersCSV.setCurrentDirectory(properties.getProperty("importTaxes").equals("") || !new File(properties.getProperty("importTaxes")).exists() ? cheminCourant : new File(properties.getProperty("importTaxes")));
            fichiersCSV.setMultiSelectionEnabled(true);
            fichiersCSV.setAcceptAllFileFilterUsed(false);
            fichiersCSV.addChoosableFileFilter(fileFilter);
            int valeur = fichiersCSV.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                // On récupères tous les fichiers
                File[] files = fichiersCSV.getSelectedFiles();
                properties.setProperty("importTaxes", files[0].getParent());
                fichierTaxes.set(files);
                ((ViewImport) openeddialog).getTextFieldFiles().setText(Arrays.toString(files));
            }
        });

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichierTaxes.get() != null) {
                for (File file : fichierTaxes.get()) { // On parcours les fichiers qu'il y en ai un ou trois cents
                    try {
                        ddt.parseDossiersTaxes(file.toString());
                    } catch (IOException e1) {
                        ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e1.getMessage());
                    }
                }

                for (Commune commune : ddt.getCommunes())
                    commune.fusionnerUneSeuleEcheanceTaxes();

                ddt.getMapImportExport().put("ImportTaxe", LocalDate.now());
                new Notification(view, "Information", "Importation des montants terminée");
                openeddialog.dispose();
            } else {
                new Notification(view, "Information", "Aucun fichier à importer");
            }
        });

        ((ViewImport) openeddialog).ouvrirPetit();
    }

    /**
     * Cette fonction initialise la combo box des communes dans les différentes vues
     *
     * @param communeBox La combox des communes
     */
    private void initComboBoxCommunes(JComboBox<String> communeBox) {
        communeBox.removeAllItems();
        communeBox.removeAll();
        for (Commune commune : ddt.getCommunes())
            communeBox.addItem(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());
    }

    /**
     * Cette fonction initialise la combo box des années dans les différentes vues
     *
     * @param yearBox La combo box des années
     */
    private void initComboBoxYear(JComboBox<String> yearBox) {
        yearBox.removeAllItems();
        String year = String.valueOf(Year.now());

        int annee = Integer.valueOf(year) + 4;

        for (int index = 10; index > 0; index--) {
            annee = annee - 1;
            yearBox.addItem(String.valueOf(annee));
        }
    }

    /**
     * Fonction initilisation la box contenant les types de dossiers
     *
     * @param box la box à initialiser
     */
    private void initBoxType(JComboBox<String> box) {
        box.removeAllItems();
        box.addItem("DP");
        box.addItem("PC");
        box.addItem("PA");
    }

    /**
     * Cette fonction initialise une combo box des années en y ajoutant un ItemListener
     * Ce listener change l'index de la seconde combo box
     *
     * @param yearBox     La combo box qui aura le listener
     * @param yearBoxDeux La seconde combo box qui aura son index selectionné de changé
     */
    private void initComboxBoxYearWithListener(JComboBox<String> yearBox, JComboBox<String> yearBoxDeux) {
        initComboBoxYear(yearBox);

        yearBox.addItemListener(e -> {
            int year1 = yearBox.getSelectedIndex();
            yearBoxDeux.setSelectedIndex(year1);
        });
    }

    /**
     * Cette fonction initialise les différentes actions des check box et des boutons sur une vue d'édition de dossiers
     *
     * @param viewDossiers La vue d'édition
     */
    private void initEditionDossiers(ViewDossiers viewDossiers) {
        AtomicReference<Boolean> isSelected = new AtomicReference<>(false);
        initComboBoxCommunes(viewDossiers.getCommuneBox());
        initComboBoxYear(viewDossiers.getYearBoxDeux());
        initComboxBoxYearWithListener(viewDossiers.getYearBox(), viewDossiers.getYearBoxDeux());

        viewDossiers.getCommuneBox().addItemListener(e -> {
            viewDossiers.getCheckBoxCommunes().setSelected(false);
            viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getYearBox().addItemListener(e -> viewDossiers.getCheckBoxAnnee().setSelected(false));

        viewDossiers.getCheckBoxCommunes().addActionListener(e -> {
            if (viewDossiers.getCheckBoxCommunes().isSelected())
                viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getCommunesEPCICheckBox().addActionListener(e -> {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected())
                viewDossiers.getCheckBoxCommunes().setSelected(false);
        });

        viewDossiers.getEnregistresRadioButton().addChangeListener(e -> {
            if (viewDossiers.getEnregistresRadioButton().isSelected()) {
                if (isSelected.get())
                    viewDossiers.getNonTaxablesCheckBox().setSelected(true);
                viewDossiers.getNonTaxablesCheckBox().setEnabled(true);
                isSelected.set(false);
            }
        });

        viewDossiers.getManquantsRadioButton().addChangeListener(e -> {
            if (viewDossiers.getManquantsRadioButton().isSelected()) {
                if (viewDossiers.getNonTaxablesCheckBox().isSelected())
                    isSelected.set(true);
                viewDossiers.getNonTaxablesCheckBox().setSelected(false);
                viewDossiers.getNonTaxablesCheckBox().setEnabled(false);
            }
        });

        viewDossiers.getInexistantsRadioButton().addChangeListener(e -> {
            if (viewDossiers.getInexistantsRadioButton().isSelected()) {
                if (viewDossiers.getNonTaxablesCheckBox().isSelected())
                    isSelected.set(true);
                viewDossiers.getNonTaxablesCheckBox().setSelected(false);
                viewDossiers.getNonTaxablesCheckBox().setEnabled(false);
            }
        });

        viewDossiers.getBoutonVisualiser().addActionListener(e -> {
            try {
                actionVisualiserDossiers(viewDossiers);
            } catch (IOException e1) {
                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                new Notification(view, "Erreur", e1.getMessage());
            }
        });
    }

    /**
     * Cette fonction permet de visualiser à l'aide d'une JTable la liste des dossiers demandés lors de l'édition de dossiers
     *
     * @param viewDossiers La vue contenant les paramètres des dossiers à afficher
     * @throws IOException Si un problème a lieu sur le fichier exporté
     */
    private void actionVisualiserDossiers(ViewDossiers viewDossiers) throws IOException {
        AtomicReference<String> dossierExportation = new AtomicReference<>();
        AtomicReference<String> typeExportation = new AtomicReference<>();
        ViewTable viewTable = new ViewTable(viewDossiers, "Visualisation de la liste des dossiers", false);
        boolean enregistres = false;
        boolean inexistants = false;

        ModelTable defaultTableModel;

        if (viewDossiers.getEnregistresRadioButton().isSelected()) {
            defaultTableModel = setModelTableDossiers(viewDossiers);
            enregistres = true; // Si on travaille sur les enregistrés on met le booléen à true
        } else if (viewDossiers.getInexistantsRadioButton().isSelected()) {
            defaultTableModel = setModelTableDossiers(viewDossiers);
            inexistants = true;
        } else {
            defaultTableModel = setModelTableDossiers(viewDossiers);
        }

        if (inexistants) {
            viewTable.getExporterButton().setVisible(false);
        }

        viewTable.getTable().setModel(Objects.requireNonNull(defaultTableModel)); // On set le modèle à l'aide de celui précédemment récupéré
        viewTable.getTable().setAutoCreateRowSorter(true); // On créé un triage automatique
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.LEFT);
        viewTable.getTable().getColumnModel().getColumn(1).setCellRenderer(cellRenderer);

        viewTable.getNbDossiers().setText(String.valueOf(defaultTableModel.getRowCount())); // On récupère le nombre de dossiers

        Boolean finalEnregistres = enregistres;
        /*
        On définie l'action d'exportation des dossiers
         */
        viewTable.getExporterButton().addActionListener(e -> {
            ViewImport viewImport = new ViewImport(viewTable, "Exportation des données", false);
            viewImport.getFieldAnnee().setText("Choix du type d'exportation");
            viewImport.getFieldFichier().setText("Dossier résultat");
            viewImport.getImporterButton().setText("Exporter les données");
            viewImport.getComboBoxAnnee().addItem("En plusieurs fichiers");
            viewImport.getComboBoxAnnee().addItem("En un seul fichier");

            viewImport.getFileChooserButton().addActionListener(e1 -> {
                // Choix du dossier où seront créés les fichiers résultats
                JFileChooser dossierResultat = new JFileChooser();
                dossierResultat.setDialogTitle("Choix du dossier où seront créés le/les fichiers résultats");
                dossierResultat.setCurrentDirectory(properties.getProperty("exportDossiers").equals("") || !new File(properties.getProperty("exportDossiers")).exists() ? cheminCourant : new File(properties.getProperty("exportDossiers")));
                dossierResultat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                dossierResultat.setAcceptAllFileFilterUsed(false);
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Text CSV (*.csv)", "csv")); // Choix exportation csv
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls")); // Choix exportation xls
                int valeur = dossierResultat.showSaveDialog(viewImport);
                if (valeur != JFileChooser.CANCEL_OPTION) {
                    properties.setProperty("exportDossiers", dossierResultat.getSelectedFile().toString());
                    dossierExportation.set(dossierResultat.getSelectedFile().toString());
                    typeExportation.set(dossierResultat.getFileFilter().getDescription());
                    viewImport.getTextFieldFiles().setText(dossierExportation.get());
                }
            });

            viewImport.getImporterButton().addActionListener(e1 -> {
                if (dossierExportation.get() != null) {
                    try {
                        ddt.exporterListeDossier(dossierExportation.get(), typeExportation.get(), viewImport.getComboBoxAnnee().getSelectedItem().toString().equals("En plusieurs fichiers"), defaultTableModel, finalEnregistres);
                    } catch (IOException e2) {
                        ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e2.getMessage());
                    }
                    new Notification(view, "Information", "Exportation terminée");
                    viewImport.dispose();
                } else {
                    new Notification(view, "Information", "Pas de dossier d'exportation ouvert");
                }
            });

            viewImport.ouvrir();
        });

        viewTable.setResizable(false);
        viewTable.ouvrir();
    }

    /**
     * Cette fonction permet de définir la vue pour les paramètres d'édition d'un tableau croisé
     *
     * @param viewDossiers La vue contenant les informations à définir
     */
    private void editionTableauxCroises(ViewDossiers viewDossiers) {
        initComboBoxCommunes(viewDossiers.getCommuneBox()); // On initialise la box des communes
        initComboBoxYear(viewDossiers.getYearBoxDeux()); // On initialise la box des années
        initComboxBoxYearWithListener(viewDossiers.getYearBox(), viewDossiers.getYearBoxDeux()); // On initialise le listener pour la première box des années

        viewDossiers.getCommuneBox().addItemListener(e -> {
            viewDossiers.getCheckBoxCommunes().setSelected(false);
            viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getYearBox().addItemListener(e -> viewDossiers.getCheckBoxAnnee().setSelected(false));

        /*
        On définit l'action pour la check box des communes
         */
        viewDossiers.getCheckBoxCommunes().addActionListener(e -> {
            if (viewDossiers.getCheckBoxCommunes().isSelected())
                viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        /*
        On définit l'action pour la check box des communes EPCI
         */
        viewDossiers.getCommunesEPCICheckBox().addActionListener(e -> {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected())
                viewDossiers.getCheckBoxCommunes().setSelected(false);
        });

        viewDossiers.getBoutonVisualiser().setText("Visualisation du tableau croisé");

        /*
        On définit l'action de visualisation
         */
        viewDossiers.getBoutonVisualiser().addActionListener(e -> {
            try {
                actionVisualiserTableauxCroises(viewDossiers);
            } catch (IOException e1) {
                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                new Notification(view, "Erreur", e1.getMessage());
            }
        });

        /*
        On définit que l'on veut toujours les trois types de dossiers pour les tableaux croisés
         */
        viewDossiers.getCheckBoxAnnee().setSelected(false);
        viewDossiers.getCheckBoxAnnee().setVisible(false);
        viewDossiers.getNonTaxablesCheckBox().setSelected(false);
        viewDossiers.getNonTaxablesCheckBox().setVisible(false);
        viewDossiers.getDPCheckBox().setSelected(true);
        viewDossiers.getDPCheckBox().setEnabled(false);
        viewDossiers.getPCCheckBox().setSelected(true);
        viewDossiers.getPCCheckBox().setEnabled(false);
        viewDossiers.getPACheckBox().setSelected(true);
        viewDossiers.getPACheckBox().setEnabled(false);
        viewDossiers.getInexistantsRadioButton().setSelected(false);
        viewDossiers.getInexistantsRadioButton().setVisible(false);

        viewDossiers.ouvrir(); // On ouvre la vue des paramètres
    }

    /**
     * Cette fonction permet de visualiser un tableau croisé à l'aide d'une vue définissant les paramètres d'affichage
     *
     * @param viewDossiers La vue contenant les paramètres d'affichage
     * @throws IOException Si un problème a lieu sur l'exportation du tableau croisé
     */
    private void actionVisualiserTableauxCroises(ViewDossiers viewDossiers) throws IOException {
        AtomicReference<String> dossierExportation = new AtomicReference<>();
        AtomicReference<String> typeExportation = new AtomicReference<>();

        ViewTable viewTable = new ViewTable(viewDossiers, "Visualisation du tableau croisé", false);
        boolean enregistres = false;

        ModelTable defaultTableModel;

        if (viewDossiers.getEnregistresRadioButton().isSelected()) {
            defaultTableModel = setModelCroises(viewDossiers);
            enregistres = true; // On regarde si on travaille sur les enregistrés
        } else
            defaultTableModel = setModelCroises(viewDossiers);


        viewTable.getTable().setModel(Objects.requireNonNull(defaultTableModel)); // On définit le modèle
        viewTable.getTable().setAutoCreateRowSorter(false);

        viewTable.getNbDossiers().setVisible(false); // On retire l'affichage du nombre de dossier
        viewTable.getNbDossiersTxt().setVisible(false); // Spécial à l'affichage de dossiers

        Boolean finalEnregistres = enregistres;
        /*
        On définit l'action d'exportation du tableau croisé
         */
        viewTable.getExporterButton().addActionListener(e -> {
            ViewImport viewImport = new ViewImport(viewTable, "Exportation des données", false);
            viewImport.getFieldAnnee().setText("Choix du type d'exportation");
            viewImport.getFieldFichier().setText("Dossier résultat");
            viewImport.getImporterButton().setText("Exporter les données");
            viewImport.getComboBoxAnnee().addItem("En plusieurs fichiers");
            viewImport.getComboBoxAnnee().addItem("En un seul fichier");

            viewImport.getFileChooserButton().addActionListener(e1 -> {
                // Choix du dossier où seront créés les fichiers résultats
                JFileChooser dossierResultat = new JFileChooser();
                dossierResultat.setDialogTitle("Choix du dossier où seront créés le/les fichiers résultats");
                dossierResultat.setCurrentDirectory(properties.getProperty("exportTableaux").equals("") || !new File(properties.getProperty("exportTableaux")).exists() ? cheminCourant : new File(properties.getProperty("exportTableaux")));
                dossierResultat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                dossierResultat.setAcceptAllFileFilterUsed(false);
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Texte CSV (*.csv)", "csv"));
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls"));
                int valeur = dossierResultat.showSaveDialog(viewImport);
                if (valeur != JFileChooser.CANCEL_OPTION) {
                    properties.setProperty("exportTableaux", dossierResultat.getSelectedFile().toString());
                    dossierExportation.set(dossierResultat.getSelectedFile().toString());
                    typeExportation.set(dossierResultat.getFileFilter().getDescription());
                    viewImport.getTextFieldFiles().setText(dossierExportation.get());
                }
            });

            viewImport.getImporterButton().addActionListener(e1 -> {
                if (dossierExportation.get() != null) {
                    try {
                        ddt.exportTableauxCroises(dossierExportation.get(), typeExportation.get(), viewImport.getComboBoxAnnee().getSelectedItem().toString().equals("En plusieurs fichiers"), defaultTableModel, finalEnregistres);
                    } catch (IOException e2) {
                        ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e2.getMessage());
                    }
                    new Notification(view, "Information", "Exportation terminée");
                    viewImport.dispose();
                } else {
                    new Notification(view, "Information", "Pas de dossier d'exportation ouvert");
                }
            });

            viewImport.ouvrir();
        });

        viewTable.setResizable(false);
        viewTable.ouvrir();
    }

    /**
     * Cette fonction permet de remplir et d'afficher les différents paramètres utiles à la visualisation des taxes
     *
     * @param viewDossiers La vue contenant les paramètres
     */
    private void editionDossiersTaxes(ViewDossiers viewDossiers) {
        viewDossiers.getEnregistresRadioButton().setVisible(false);
        viewDossiers.getManquantsRadioButton().setVisible(false);
        viewDossiers.getInexistantsRadioButton().setVisible(false);
        viewDossiers.getNonTaxablesCheckBox().setVisible(false);
        viewDossiers.getPACheckBox().setSelected(true);
        viewDossiers.getPCCheckBox().setSelected(true);
        viewDossiers.getDPCheckBox().setSelected(true);
        viewDossiers.getPACheckBox().setEnabled(false);
        viewDossiers.getPCCheckBox().setEnabled(false);
        viewDossiers.getDPCheckBox().setEnabled(false);
        viewDossiers.getCheckBoxAnnee().setSelected(false);
        viewDossiers.getCheckBoxAnnee().setVisible(false);
        viewDossiers.getAnneeLabel().setText("Sélection de(s) (l')année(s) de décision");

        initComboBoxCommunes(viewDossiers.getCommuneBox());
        initComboBoxYear(viewDossiers.getYearBoxDeux());
        initComboxBoxYearWithListener(viewDossiers.getYearBox(), viewDossiers.getYearBoxDeux());

        viewDossiers.getCheckBoxCommunes().addActionListener(e -> {
            if (viewDossiers.getCheckBoxCommunes().isSelected())
                viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getCommuneBox().addItemListener(e -> {
            viewDossiers.getCheckBoxCommunes().setSelected(false);
            viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getYearBox().addItemListener(e -> viewDossiers.getCheckBoxAnnee().setSelected(false));

        viewDossiers.getCommunesEPCICheckBox().addActionListener(e -> {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected())
                viewDossiers.getCheckBoxCommunes().setSelected(false);
        });

        viewDossiers.getBoutonVisualiser().addActionListener(e -> {
            try {
                actionVisualiserTaxes(viewDossiers);
            } catch (IOException e1) {
                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                new Notification(view, "Erreur", e1.getMessage());
            }
        });
    }

    /**
     * Cette fonction permet d'afficher la JTable contenant les informations à afficher sur les taxes
     *
     * @param viewDossiers La vue contenant les paramètres
     * @throws IOException Si une exception est levée au niveau de l'exportation vers un fichier
     */
    private void actionVisualiserTaxes(ViewDossiers viewDossiers) throws IOException {
        AtomicReference<String> dossierExportation = new AtomicReference<>();
        AtomicReference<String> typeExportation = new AtomicReference<>();

        ViewTable viewTable = new ViewTable(viewDossiers, "Visualisation d'information sur les montants de taxes de dossiers", false);

        ModelTable defaultTableModel = setModelTableTaxes(viewDossiers);

        viewTable.getTable().setModel(Objects.requireNonNull(defaultTableModel)); // On définit le modèle
        viewTable.getTable().setAutoCreateRowSorter(true);

        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.LEFT);
        viewTable.getTable().getColumnModel().getColumn(5).setCellRenderer(cellRenderer);

        viewTable.getNbDossiers().setText("" + defaultTableModel.getRowCount());
        viewTable.getNbDossiersTxt().setText("Nombre de montants enregistrés");
        viewTable.getExporterToutesLesCommunesButton().setVisible(true);
        viewTable.getExporterToutesLesCommunesButton().setEnabled(true);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);
        List<String> annees = new ArrayList<>();
        for (int i = anneesCalculees; i >= 0; i--) {
            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            annees.add(anneeString); // On ajoute les années à un liste que l'on comparera plus tard
        }

        /*
        On définit l'action d'exportation du tableau croisé
         */
        viewTable.getExporterButton().addActionListener(e -> {
            ViewImport viewImport = new ViewImport(viewTable, "Exportation des données", false);
            viewImport.getImporterButton().setText("Exporter les données");
            viewImport.getFieldFichier().setText("Dossier résultat");
            viewImport.getFieldAnnee().setVisible(false);
            viewImport.getComboBoxAnnee().setVisible(false);

            viewImport.getFileChooserButton().addActionListener(e1 -> {
                // Choix du dossier où seront créés les fichiers résultats
                JFileChooser dossierResultat = new JFileChooser();
                dossierResultat.setDialogTitle("Choix du dossier où sera créé le fichier résultat");
                dossierResultat.setCurrentDirectory(properties.getProperty("exportTaxes").equals("") || !new File(properties.getProperty("exportTaxes")).exists() ? cheminCourant : new File(properties.getProperty("exportTaxes")));
                dossierResultat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                dossierResultat.setAcceptAllFileFilterUsed(false);
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Texte CSV (*.csv)", "csv"));
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls"));
                int valeur = dossierResultat.showSaveDialog(viewImport);
                if (valeur != JFileChooser.CANCEL_OPTION) {
                    properties.setProperty("exportTaxes", dossierResultat.getSelectedFile().toString());
                    dossierExportation.set(dossierResultat.getSelectedFile().toString());
                    typeExportation.set(dossierResultat.getFileFilter().getDescription());
                    viewImport.getTextFieldFiles().setText(dossierExportation.get());
                }
            });

            viewImport.getImporterButton().addActionListener(e1 -> {
                if (dossierExportation.get() != null) {
                    if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected()) {
                        try {
                            ddt.exporterTaxes(dossierExportation.get(), typeExportation.get(), defaultTableModel, false, false, false, annees);
                        } catch (IOException e2) {
                            ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                            new Notification(view, "Erreur", e2.getMessage());
                        }
                    } else if (viewDossiers.getCommunesEPCICheckBox().isSelected()) {
                        try {
                            ddt.exporterTaxes(dossierExportation.get(), typeExportation.get(), defaultTableModel, false, true, false, annees);
                        } catch (IOException e2) {
                            ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                            new Notification(view, "Erreur", e2.getMessage());
                        }
                    } else {
                        try {
                            ddt.exporterTaxes(dossierExportation.get(), typeExportation.get(), defaultTableModel, false, false, true, annees);
                        } catch (IOException e2) {
                            ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                            new Notification(view, "Erreur", e2.getMessage());
                        }
                    }
                    new Notification(view, "Information", "Exportation terminée");
                    viewImport.dispose();
                } else {
                    new Notification(view, "Information", "Pas de dossier d'exportation ouvert");
                }
            });

            viewImport.ouvrir();
        });

        viewTable.getExporterToutesLesCommunesButton().addActionListener(e -> {
            ViewImport viewImport = new ViewImport(viewTable, "Exportation des données", false);
            viewImport.getImporterButton().setText("Exporter les données");
            viewImport.getFieldFichier().setText("Dossier résultat");
            viewImport.getFieldAnnee().setVisible(false);
            viewImport.getComboBoxAnnee().setVisible(false);

            viewImport.getFileChooserButton().addActionListener(e1 -> {
                // Choix du dossier où seront créés les fichiers résultats
                JFileChooser dossierResultat = new JFileChooser();
                dossierResultat.setDialogTitle("Choix du dossier où seront créés les fichiers résultats");
                dossierResultat.setCurrentDirectory(properties.getProperty("exportTaxes").equals("") || !new File(properties.getProperty("exportTaxes")).exists() ? cheminCourant : new File(properties.getProperty("exportTaxes")));
                dossierResultat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                dossierResultat.setAcceptAllFileFilterUsed(false);
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Texte CSV (*.csv)", "csv"));
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls"));
                int valeur = dossierResultat.showSaveDialog(viewImport);
                if (valeur != JFileChooser.CANCEL_OPTION) {
                    properties.setProperty("exportTaxes", dossierResultat.getSelectedFile().toString());
                    dossierExportation.set(dossierResultat.getSelectedFile().toString());
                    typeExportation.set(dossierResultat.getFileFilter().getDescription());
                    viewImport.getTextFieldFiles().setText(dossierExportation.get());
                }
            });

            viewImport.getImporterButton().addActionListener(e1 -> {
                if (dossierExportation.get() != null) {
                    try {
                        ddt.exporterTaxes(dossierExportation.get(), typeExportation.get(), null, true, false, false, annees);
                    } catch (IOException e2) {
                        ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e2.getMessage());
                    }
                    new Notification(view, "Information", "Exportation terminée");
                    viewImport.dispose();
                } else {
                    new Notification(view, "Information", "Pas de dossier d'exportation ouvert");
                }
            });

            viewImport.ouvrir();
        });

        viewTable.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        viewTable.getTable().getColumnModel().getColumn(0).setPreferredWidth(175);
        viewTable.getTable().getColumnModel().getColumn(1).setPreferredWidth(230);
        viewTable.getTable().getColumnModel().getColumn(2).setPreferredWidth(100);
        viewTable.getTable().getColumnModel().getColumn(3).setPreferredWidth(100);
        viewTable.getTable().getColumnModel().getColumn(4).setPreferredWidth(95);
        viewTable.getTable().getColumnModel().getColumn(5).setPreferredWidth(82);
        viewTable.getTable().getColumnModel().getColumn(6).setPreferredWidth(82);
        viewTable.getTable().getColumnModel().getColumn(7).setPreferredWidth(100);
        viewTable.getTable().getColumnModel().getColumn(8).setPreferredWidth(100);

        viewTable.setResizable(false);
        viewTable.ouvrir();
    }

    /**
     * Cette fonction permet de définir les paramètres d'affichage des tableaux croisés d'information de taxes
     *
     * @param viewDossiers La vue contenant les paramètres
     */
    private void editionTableauxInfos(ViewDossiers viewDossiers) {
        viewDossiers.getNonTaxablesCheckBox().setSelected(false);
        viewDossiers.getNonTaxablesCheckBox().setVisible(false);
        viewDossiers.getDPCheckBox().setSelected(true);
        viewDossiers.getDPCheckBox().setEnabled(false);
        viewDossiers.getPCCheckBox().setSelected(true);
        viewDossiers.getPCCheckBox().setEnabled(false);
        viewDossiers.getPACheckBox().setSelected(true);
        viewDossiers.getPACheckBox().setEnabled(false);
        viewDossiers.getDetailTARAPRadioButton().setVisible(true);
        viewDossiers.getDetailTITMTRRadioButton().setVisible(true);
        viewDossiers.getEnregistresRadioButton().setVisible(false);
        viewDossiers.getManquantsRadioButton().setVisible(false);
        viewDossiers.getEnregistresRadioButton().setSelected(false);
        viewDossiers.getManquantsRadioButton().setSelected(false);
        viewDossiers.getInexistantsRadioButton().setVisible(false);
        viewDossiers.getInexistantsRadioButton().setSelected(false);
        viewDossiers.getCheckBoxAnnee().setSelected(false);
        viewDossiers.getCheckBoxAnnee().setVisible(false);
        viewDossiers.getAnneeLabel().setText("Sélection de(s) (l')année(s) de décision");
        initComboBoxCommunes(viewDossiers.getCommuneBox());
        initComboBoxYear(viewDossiers.getYearBoxDeux());
        initComboxBoxYearWithListener(viewDossiers.getYearBox(), viewDossiers.getYearBoxDeux());

        viewDossiers.getCheckBoxCommunes().addActionListener(e -> {
            if (viewDossiers.getCheckBoxCommunes().isSelected())
                viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getCommunesEPCICheckBox().addActionListener(e -> {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected())
                viewDossiers.getCheckBoxCommunes().setSelected(false);
        });

        viewDossiers.getCommuneBox().addItemListener(e -> {
            viewDossiers.getCheckBoxCommunes().setSelected(false);
            viewDossiers.getCommunesEPCICheckBox().setSelected(false);
        });

        viewDossiers.getYearBox().addItemListener(e -> viewDossiers.getCheckBoxAnnee().setSelected(false));

        viewDossiers.getBoutonVisualiser().addActionListener(e -> {
            try {
                actionVisualiserTableauxTA(viewDossiers);
            } catch (IOException e1) {
                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                new Notification(view, "Erreur", e1.getMessage());
            }
        });
    }

    /**
     * Cette fonction permet d'afficher la JTable contenant un tableau croisé des informations de TA
     *
     * @param viewDossiers La vue contenant les paramètres
     * @throws IOException Si une exception est levée au niveau de l'exportation vers un fichier
     */
    private void actionVisualiserTableauxTA(ViewDossiers viewDossiers) throws IOException {
        AtomicReference<String> dossierExportation = new AtomicReference<>();
        AtomicReference<String> typeExportation = new AtomicReference<>();
        ViewTable viewTable = new ViewTable(viewDossiers, "Visualisation du tableau croisé des informations sur les montants de TA", false);

        ModelTable defaultTableModel = null;

        if (viewDossiers.getDetailTITMTRRadioButton().isSelected())
            defaultTableModel = setModelTableauxTA(viewDossiers);
        else
            defaultTableModel = setModelTableauxTA2(viewDossiers);

        viewTable.getTable().setModel(Objects.requireNonNull(defaultTableModel)); // On définit le modèle
        viewTable.getTable().setAutoCreateRowSorter(true);

        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.LEFT);
        viewTable.getTable().getColumnModel().getColumn(2).setCellRenderer(cellRenderer);
        viewTable.getTable().getColumnModel().getColumn(3).setCellRenderer(cellRenderer);
        viewTable.getTable().getColumnModel().getColumn(4).setCellRenderer(cellRenderer);
        viewTable.getTable().getColumnModel().getColumn(5).setCellRenderer(cellRenderer);
        viewTable.getTable().getColumnModel().getColumn(6).setCellRenderer(cellRenderer);
        viewTable.getTable().getColumnModel().getColumn(7).setCellRenderer(cellRenderer);

        if (viewDossiers.getDetailTITMTRRadioButton().isSelected()) {
            viewTable.getTable().getColumnModel().getColumn(8).setCellRenderer(cellRenderer);
            viewTable.getTable().getColumnModel().getColumn(9).setCellRenderer(cellRenderer);
        }

        viewTable.getNbDossiers().setVisible(false);
        viewTable.getNbDossiersTxt().setVisible(false);
        viewTable.getExporterToutesLesCommunesButton().setVisible(false);
        viewTable.getExporterToutesLesCommunesButton().setEnabled(false);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);
        List<String> annees = new ArrayList<>();
        for (int i = anneesCalculees; i >= 0; i--) {
            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            annees.add(anneeString); // On ajoute les années à un liste que l'on comparera plus tard
        }
        Collections.sort(annees);

        viewTable.getExporterButton().addActionListener(e -> {
            ViewImport viewImport = new ViewImport(viewTable, "Exportation des données", false);
            viewImport.getFieldFichier().setText("Dossier résultat");
            viewImport.getFieldAnnee().setVisible(true);
            viewImport.getFieldAnnee().setText("Communes exportées");
            viewImport.getComboBoxAnnee().setVisible(true);
            viewImport.getComboBoxAnnee().addItem("Toutes les communes");
            viewImport.getComboBoxAnnee().addItem("Groupe de communes");
            viewImport.getImporterButton().setText("Exporter les données");

            viewImport.getFileChooserButton().addActionListener(e1 -> {
                // Choix du dossier où seront créés les fichiers résultats
                JFileChooser dossierResultat = new JFileChooser();
                dossierResultat.setDialogTitle("Choix du dossier où sera créé le fichier résultat");
                dossierResultat.setCurrentDirectory(properties.getProperty("exportTableauxTA").equals("") || !new File(properties.getProperty("exportTableauxTA")).exists() ? cheminCourant : new File(properties.getProperty("exportTableauxTA")));
                dossierResultat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                dossierResultat.setAcceptAllFileFilterUsed(false);
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Texte CSV (*.csv)", "csv"));
                dossierResultat.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Excel XLS (*.xls)", "xls"));
                int valeur = dossierResultat.showSaveDialog(viewImport);
                if (valeur != JFileChooser.CANCEL_OPTION) {
                    properties.setProperty("exportTableauxTA", dossierResultat.getSelectedFile().toString());
                    dossierExportation.set(dossierResultat.getSelectedFile().toString());
                    typeExportation.set(dossierResultat.getFileFilter().getDescription());
                    viewImport.getTextFieldFiles().setText(dossierExportation.get());
                }
            });

            viewImport.getImporterButton().addActionListener(e1 -> {
                if (dossierExportation.get() != null) {
                    try {
                        if (viewDossiers.getDetailTITMTRRadioButton().isSelected())
                            ddt.exporterTableauxTA(dossierExportation.get(), typeExportation.get(), viewImport.getComboBoxAnnee().getSelectedItem().toString().contains("Toutes les communes"), annees);
                        else
                            ddt.exporterTableauxTA2(dossierExportation.get(), typeExportation.get(), viewImport.getComboBoxAnnee().getSelectedItem().toString().contains("Toutes les communes"), annees);
                    } catch (IOException e2) {
                        ddt.getLogs().append(Arrays.toString(e2.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e2.getMessage());
                    }
                    new Notification(view, "Information", "Exportation terminée");
                    viewImport.dispose();
                } else {
                    new Notification(view, "Information", "Pas de dossier d'exportation ouvert");
                }
            });

            viewImport.ouvrir();
        });

        viewTable.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        if (viewDossiers.getDetailTITMTRRadioButton().isSelected()) {
            viewTable.getTable().getColumnModel().getColumn(0).setPreferredWidth(336);
            viewTable.getTable().getColumnModel().getColumn(1).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(2).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(3).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(4).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(5).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(6).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(7).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(8).setPreferredWidth(81);
            viewTable.getTable().getColumnModel().getColumn(9).setPreferredWidth(81);
        } else {
            viewTable.getTable().getColumnModel().getColumn(0).setPreferredWidth(300);
            viewTable.getTable().getColumnModel().getColumn(1).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(2).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(3).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(4).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(5).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(6).setPreferredWidth(109);
            viewTable.getTable().getColumnModel().getColumn(7).setPreferredWidth(109);
        }

        viewTable.setResizable(false);

        viewTable.ouvrir();
    }

    /**
     * Cette fonction permet de choisir les fichiers non taxables à formater
     */
    private void formatageNonTaxable() {
        AtomicReference<File[]> fichierNT = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Formatage de fichiers de dossiers pour importation dans l'application", false);
        ((ViewImport) openeddialog).getFieldFichier().setText("Sélection du/des fichier(s)");
        ((ViewImport) openeddialog).getFieldAnnee().setVisible(true);
        ((ViewImport) openeddialog).getFieldAnnee().setText("Type de fichier");
        ((ViewImport) openeddialog).getComboBoxAnnee().setVisible(true);
        ((ViewImport) openeddialog).getComboBoxAnnee().removeAllItems();
        ((ViewImport) openeddialog).getComboBoxAnnee().addItem("Dossiers non taxables");
        ((ViewImport) openeddialog).getComboBoxAnnee().addItem("Dossiers refusés");
        ((ViewImport) openeddialog).getComboBoxAnnee().addItem("Dossiers inexistants");
        ((ViewImport) openeddialog).getImporterButton().setText("Formater le(s) fichier(s)");

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichierNT.get() != null) {
                try {
                    if (((ViewImport) openeddialog).getComboBoxAnnee().getSelectedItem().toString().equals("Dossiers non taxables")) {
                        for (File file : fichierNT.get())
                            Formatage.formatageImportation(file, true);
                    } else {
                        for (File file : fichierNT.get())
                            Formatage.formatageImportation(file, false);
                    }
                } catch (IOException e1) {
                    ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                    new Notification(view, "Erreur", e1.getMessage());
                }
                new Notification(view, "Information", "Formatage terminé");
            } else {
                new Notification(view, "Information", "Aucun fichier à formater");
            }
        });

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Texte CSV (*.csv)", "csv");

            // Choix du fichier des non taxables
            JFileChooser fichierNonTaxable = new JFileChooser();
            fichierNonTaxable.setDialogTitle("Choix du fichier des dossiers non taxables");
            fichierNonTaxable.setCurrentDirectory(properties.getProperty("formatageNT").equals("") || !new File(properties.getProperty("formatageNT")).exists() ? cheminCourant : new File(properties.getProperty("formatageNT")));
            fichierNonTaxable.setMultiSelectionEnabled(true);
            fichierNonTaxable.setAcceptAllFileFilterUsed(false);
            fichierNonTaxable.addChoosableFileFilter(fileFilter);
            int valeur = fichierNonTaxable.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                fichierNT.set(fichierNonTaxable.getSelectedFiles());
                properties.setProperty("formatageNT", fichierNT.get()[0].getParent());
                ((ViewImport) openeddialog).getTextFieldFiles().setText(Arrays.toString(fichierNT.get()));
            }
        });

        ((ViewImport) openeddialog).ouvrirPetit();
    }

    /**
     * Cette fonction permet de réaliser un formatage xls sur un dossier contenant des fichiers csv
     */
    private void formatageXLS() {
        AtomicReference<File[]> fichierXLS = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Formatage de fichiers CSV en fichiers XLS", false);
        ((ViewImport) openeddialog).getFieldFichier().setText("Sélection du dossier");
        ((ViewImport) openeddialog).getFieldAnnee().setVisible(false);
        ((ViewImport) openeddialog).getComboBoxAnnee().setVisible(false);
        ((ViewImport) openeddialog).getImporterButton().setText("Créer les fichiers XLS");

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            JFileChooser dossierCSV = new JFileChooser();
            dossierCSV.setDialogTitle("Choix du dossier des fichiers CSV à convertir");
            dossierCSV.setCurrentDirectory(properties.getProperty("formatageXLS").equals("") || !new File(properties.getProperty("formatageXLS")).exists() ? cheminCourant : new File(properties.getProperty("formatageXLS")));
            dossierCSV.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int valeur = dossierCSV.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                File[] files = dossierCSV.getSelectedFile().listFiles();
                properties.setProperty("formatageXLS", files[0].getParent());
                fichierXLS.set(files);

            }
        });

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichierXLS.get() != null) {
                for (File file : fichierXLS.get()) {
                    try {
                        Formatage.formatageXLS(file);
                    } catch (IOException e1) {
                        ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                        new Notification(view, "Erreur", e1.getMessage());
                    }
                }
                new Notification(view, "Information", "Formatage XLS terminé");
            } else {
                new Notification(view, "Information", "Aucun dossier ouvert pour le formatage");
            }
        });

        ((ViewImport) openeddialog).ouvrirPetit();
    }

    /**
     * Cette fonction permet de créer et de définir le modèle de la table des communes
     */
    private void setModelTableCommunes() {
        Object[][] data = new Object[ddt.getCommunes().size()][4];
        String[] colonnes = {"Departement", "Numéro INSEE", "Nom de la commune", "Groupe de communes"};

        int nbEPCI = 0;

        int index = 0;
        /*
        Pour toutes les communes
         */
        for (Commune commune : ddt.getCommunes()) {
            data[index][0] = commune.getDepartment(); // On récupère le département
            data[index][1] = commune.getNumeroINSEE(); // On récupère le numéro INSEE
            data[index][2] = commune.getNomCommune(); // On récupère le nom de commune
            data[index][3] = commune.isEpci(); // On regarde si la comme est EPCI ou non
            if (commune.isEpci())
                nbEPCI++;
            index++;
        }

        view.getNbCommunesEPCI().setText(String.valueOf(nbEPCI)); // On note le nombre de commune EPCI

        view.getTableCommunes().setModel(new DefaultTableModel(data, colonnes) {
            @Override
            public boolean isCellEditable(int row, int column) { // On empêche la modification du tableau sauf pour définir une commune EPCI
                return column == 3;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) { // On retourne la classe String pour toutes les colonnes sauf celle définissant l'EPCI
                Class classe = String.class;
                if (columnIndex == 3) {
                    classe = Boolean.class;
                }
                return classe;
            }
        });

        view.getTableCommunes().setAutoCreateRowSorter(true); // On crée un sorteur automatique
    }

    /**
     * Cette fonction permet de définir le modèle du tableau croisé
     *
     * @param viewDossiers La vue contenant les informations sur le modèle à créer
     * @return Le modèle de la JTable
     * @throws IOException Si les années ne sont pas ordonnées dans la vue des paramètres
     */
    private ModelTable setModelCroises(ViewDossiers viewDossiers) throws IOException {
        /*
        On récupère les types de dossiers à afficher
         */
        boolean PA = viewDossiers.getPACheckBox().isSelected();
        boolean PC = viewDossiers.getPCCheckBox().isSelected();
        boolean DP = viewDossiers.getDPCheckBox().isSelected();
        if (!PA && !PC && !DP) { // On vérifie qu'au moins un type de dossier a été sélectionné
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez choisir au moins un type de dossier");
        }

        if (viewDossiers.getCommunesEPCICheckBox().isSelected() && Integer.valueOf(view.getNbCommunesEPCI().getText()) == 0)
            throw new IOException("Aucune commune définie dans le groupe de communes");


        /*
        On initialise les colonnes en fonction des enregistrés ou manquants
         */
        String[] colonnes;
        if (viewDossiers.getEnregistresRadioButton().isSelected())
            colonnes = new String[]{"Départment", "Numéro INSEE", "Nom Commune", "Année", "DP", "PC", "PA", "Total", "Remarque"};
        else
            colonnes = new String[]{"Départment", "Numéro INSEE", "Nom Commune", "Année", "DP", "PC", "PA", "Total"};

        Object[] datas = new Object[9]; // Création d'une ligne

        /*
        On crée le modèle par défaut en retirant l'édition du tableau
         */
        ModelTable defaultTableModel = new ModelTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (String colonne : colonnes)
            defaultTableModel.addColumn(colonne); // On ajoute chaque colonne au modèle

        /*
        On définit les nombres essentiels à compter
         */
        Map<String, List<Integer>> totauxAnnees = new HashMap<>(); // Totaux par année
        List<Integer> zeroAnnee = new ArrayList<>();
        for (int i = 0; i < 4; i++)
            zeroAnnee.add(0);

        /*
        Totaux de toutes les années confondues
         */
        Integer nbPA = 0;
        Integer nbPC = 0;
        Integer nbDP = 0;
        Integer nbTotal = 0;
        Integer nbTotalCommune = 0;

        /*
        On récupère la commune sélectionnée puis on vérifiera si l'utilisateur ne veut travailler que sur celle-ci
         */
        Commune communeINSEE = ddt.rechercherCommuneINSEE(String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[1],
                String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[0]);

        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};

        /*
        On vérifie que les années sont bien ordonnées dans les choix
         */
        if (Integer.valueOf(anneesBox[0]) - Integer.valueOf(anneesBox[1]) > 0) {
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez ordonner les années");
        }

        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]); // On calcul le nombre d'années totales

        /*
        On parcours toutes les communes
         */
        for (Commune commune : ddt.getCommunes()) {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected() && !commune.isEpci()) // On regarde si on ne veut récupérer que les communes EPCI
                continue;
            if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected()) {
                if (!commune.getNumeroINSEE().equals(communeINSEE.getNumeroINSEE()) && !commune.getNomCommune().equals(communeINSEE.getNomCommune()))
                    continue;
            }
            datas[0] = commune.getDepartment(); // On note le département dans la première colonne
            datas[1] = commune.getNumeroINSEE(); // On note le numéro INSEE dans la seconde colonne
            datas[2] = commune.getNomCommune(); // On note le nom de la commune dans la troisième colonne

            /*
            On parcours toutes les années
             */
            for (int i = anneesCalculees; i >= 0; i--) {
                int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
                String anneeString = String.valueOf(anneeInt);

                datas[3] = Year.now().toString().substring(0, 2) + anneeString; // On écrit l'année en cours dans la quatrième colonne

                List<Dossier> dossierDP;
                List<Dossier> dossierPC;
                List<Dossier> dossierPA;

                /*
                On regarde si on travaille sur les dossiers enregistrés ou manquants et on recupère pour l'année une liste de dossiers
                enregistrés ou manquants par type de dossiers
                 */
                if (viewDossiers.getEnregistresRadioButton().isSelected()) {
                    dossierDP = commune.getAnneeTypeWithoutInexistants(anneeString, "DP");
                    dossierPA = commune.getAnneeTypeWithoutInexistants(anneeString, "PA");
                    dossierPC = commune.getAnneeTypeWithoutInexistants(anneeString, "PC");
                } else {
                    dossierDP = commune.rechercherManquantsAnnee(anneeString, "DP");
                    dossierPA = commune.rechercherManquantsAnnee(anneeString, "PA");
                    dossierPC = commune.rechercherManquantsAnnee(anneeString, "PC");
                }

                /*
                On écrit les tailles de chaque liste dans les 5e, 6e et 7e colonne
                 */
                datas[4] = dossierDP.size();
                datas[5] = dossierPC.size();
                datas[6] = dossierPA.size();

                /*
                Si on travaille sur les enregistrés, on note les éventuelles remarques
                 */
                if (viewDossiers.getEnregistresRadioButton().isSelected()) {
                    if (dossierDP.size() == 0)
                        datas[8] = (datas[8] != null ? datas[8] : "") + " Aucune DP ";
                    if (dossierPC.size() == 0)
                        datas[8] = (datas[8] != null ? datas[8] : "") + " Aucun PC ";
                }

                totauxAnnees.putIfAbsent(anneeString, new ArrayList<>(zeroAnnee)); // On créé une année et une liste si les totaux de l'année en cours n'ont pas encore été calculés

                /*
                On calcule les totaux DP pour l'année
                 */
                Integer totalDPAnnee = totauxAnnees.get(anneeString).get(0);
                totalDPAnnee += dossierDP.size();
                totauxAnnees.get(anneeString).set(0, totalDPAnnee);

                /*
                On calcule les totaux PC pour l'année en cours
                 */
                Integer totalPCAnnee = totauxAnnees.get(anneeString).get(1);
                totalPCAnnee += dossierPC.size();
                totauxAnnees.get(anneeString).set(1, totalPCAnnee);

                /*
                On calcule les totaux PA pour l'année en cours
                 */
                Integer totalPAAnnee = totauxAnnees.get(anneeString).get(2);
                totalPAAnnee += dossierPA.size();
                totauxAnnees.get(anneeString).set(2, totalPAAnnee);

                /*
                On calcule les totaux DP sur toutes les années
                 */
                nbDP += dossierDP.size();
                nbTotalCommune += dossierDP.size();
                nbPC += dossierPC.size();
                nbTotalCommune += dossierPC.size();
                nbPA += dossierPA.size();
                nbTotalCommune += dossierPA.size();


                datas[7] = nbTotalCommune; // On écrit le total tout type confondues pour la commune et l'année en cours
                nbTotal += nbTotalCommune;
                nbTotalCommune = 0;

                defaultTableModel.addRow(datas); // On ajoute la ligne au tableau
                datas = new Object[9]; // On réinitialise l'objet des données de la ligne
            }
        }

        /*
        On reparcours toutes les années afin de calculer et d'afficher le total de chaque année
         */
        for (int i = anneesCalculees; i >= 0; i--) {
            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);

            /*
            On calcule le total pour chaque année
             */
            Integer totalAnnee = totauxAnnees.get(anneeString).get(3);
            totalAnnee += totauxAnnees.get(anneeString).get(0) + totauxAnnees.get(anneeString).get(1) + totauxAnnees.get(anneeString).get(2);
            totauxAnnees.get(anneeString).set(3, totalAnnee);

            if (i == anneesCalculees) { // On affiche pour la première année total afin de s'y retrouver
                datas[0] = "---";
                datas[1] = "---";
                datas[2] = "Total";
            }
            datas[3] = Year.now().toString().substring(0, 2) + anneeString; // On écrit l'année en cours

            /*
            On écrit les totaux dans les colonnes 5,6,7,8 pour chaque année (une ligne)
             */
            datas[4] = totauxAnnees.get(anneeString).get(0);
            datas[5] = totauxAnnees.get(anneeString).get(1);
            datas[6] = totauxAnnees.get(anneeString).get(2);
            datas[7] = totauxAnnees.get(anneeString).get(3);

            defaultTableModel.addRow(datas); // On écrit dans le tableau les données
            datas = new Object[9]; // On réinitialise les données de la ligne
        }

        /*
        Enfin on écrit les totaux pour toutes les années confondues
         */
        datas[5] = nbPC;
        datas[4] = nbDP;
        datas[6] = nbPA;
        datas[7] = nbTotal;
        defaultTableModel.addRow(datas);

        return defaultTableModel;
    }

    /**
     * Cette fonction permet de contruire le modèle d'affichage d'une liste de dossiers, manquants ou enregistrés
     *
     * @param viewDossiers La vue contenant les paramètres de sélection des dossiers
     * @return Le modèle contenant la liste de dossiers
     * @throws IOException Si aucun type de dossier n'a été choisi, ou s'il n'y a aucune commune ou s'il n'y a aucun dossier à afficher
     */
    private ModelTable setModelTableDossiers(ViewDossiers viewDossiers) throws IOException {
        /*
        On récupère les types de dossiers à afficher
         */
        boolean PA = viewDossiers.getPACheckBox().isSelected();
        boolean PC = viewDossiers.getPCCheckBox().isSelected();
        boolean DP = viewDossiers.getDPCheckBox().isSelected();
        if (!PA && !PC && !DP) { // On vérifie qu'au moins un type de dossier a été sélectionné
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez choisir au moins un type de dossier");
        }

        if (viewDossiers.getCommunesEPCICheckBox().isSelected() && Integer.valueOf(view.getNbCommunesEPCI().getText()) == 0)
            throw new IOException("Aucune commune définie dans le groupe de communes");

        /*
        On définit les colonnes en fonction de si on travaille sur les dossiers enregistrés ou manquants
         */
        String[] colonnes;
        if (viewDossiers.getEnregistresRadioButton().isSelected() || viewDossiers.getInexistantsRadioButton().isSelected())
            colonnes = new String[]{"Numéro du dossier", "Année de dépôt", "Commune du dossier", "Nom du demandeur", "Etat du dossier", "Groupe de communes"};
        else
            colonnes = new String[]{"Numéro du dossier", "Année de dépôt", "Commune du dossier", "Groupe de communes"};

        if (ddt.getCommunes().size() == 0) // On vérifie qu'il y ai bien au moins une commune dans la DDT
            throw new IOException("Il n'existe aucune commune");

        /*
        On définit le modèle de la table en empechant les modification directs
         */
        ModelTable defaultTableModel = new ModelTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                Class<?> classe = String.class;
                if (columnIndex == 1)
                    classe = Integer.class;
                return classe;
            }
        };
        for (String colonne : colonnes) // On ajoute les colonnes au modèle
            defaultTableModel.addColumn(colonne);

        // On recherche la commune saisie dans tous les cas, on vérifiera plus tard si on travaille sur une commune, les communes EPCI ou toutes
        Commune communeINSEE = ddt.rechercherCommuneINSEE(String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[1],
                String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[0]);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);
        List<String> annees = new ArrayList<>();
        for (int i = anneesCalculees; i >= 0; i--) {
            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            annees.add(anneeString); // On ajoute les années à un liste que l'on comparera plus tard
        }

        /*
        On parcours toutes les communes
         */
        Vector<Object> data = new Vector<>();
        for (Commune commune : ddt.getCommunes()) {
            // On vérifie si l'utilisateur ne travaille que sur les EPCI et que la commune n'est pas EPCI
            if (viewDossiers.getCommunesEPCICheckBox().isSelected() && !commune.isEpci())
                continue;

            // On vérifie si l'utilisateur ne travaille que sur une seule commune
            if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected()) {
                if (!commune.getNomCommune().equals(communeINSEE.getNomCommune()) && !commune.getNumeroINSEE().equals(communeINSEE.getNumeroINSEE()))
                    continue;
            }

            // On récupère l'intégralité des dossiers de la commune (réduction de la complexité car un seul tri)
            Map<String, List<Dossier>> dossiers = commune.getDossiers();
            /*
            On parcours toutes les années par clés
             */
            for (Map.Entry<String, List<Dossier>> annee : dossiers.entrySet()) {
                // On vérifie si on travaille sur une ou plusieurs années et si l'année est comprise dans la liste d'années ou non
                if (!annees.contains(annee.getKey()) && !viewDossiers.getCheckBoxAnnee().isSelected())
                    continue;

                // On vérifie si on travaille sur les dossiers enregistrés
                if (viewDossiers.getEnregistresRadioButton().isSelected()) {
                    /*
                    On parcours l'intégralité des dossiers
                     */
                    for (Dossier dossier : dossiers.get(annee.getKey())) {
                        if (!DP && dossier.getType().equals("DP")) // Si on travaille sur les DP et que le dossier n'est pas de type DP
                            continue;
                        if (!PC && dossier.getType().equals("PC")) // Si on travaille sur les PC et que le dossier n'est pas de type PC
                            continue;
                        if (!PA && dossier.getType().equals("PA")) // Si on travaille sur les PA et que le dossier n'est pas de type PA
                            continue;
                        if (dossier.getEtatDossier().equals("Erreur numérotation") && dossier.getNomDemandeur().equals("Inexistant"))
                            continue;

                        // On vérifie si on travaille seulement sur les dossiers non taxable, et si le dossier l'est ou pas
                        if (viewDossiers.getNonTaxablesCheckBox().isSelected() && !dossier.getEtatDossier().equalsIgnoreCase("NON TAXABLE"))
                            continue;

                        /*
                        On ajoute les éléments à la ligne courante puis on l'ajoute au modèle
                         */
                        data.addElement(dossier.getNumeroDossier());
                        data.addElement(dossier.getAnnee());
                        data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());
                        data.addElement(dossier.getNomDemandeur());
                        data.addElement(dossier.getEtatDossier());
                        data.addElement(commune.isEpci() ? "OUI" : "NON");

                        defaultTableModel.addRow(new Vector<>(data));
                        data.clear(); // On remet la ligne courante à zéro pour la prochaine ligne
                    }
                } else if (viewDossiers.getInexistantsRadioButton().isSelected()) {
                    /*
                    On parcours l'intégralité des dossiers
                     */
                    for (Dossier dossier : dossiers.get(annee.getKey())) {
                        if (!DP && dossier.getType().equals("DP")) // Si on travaille sur les DP et que le dossier n'est pas de type DP
                            continue;
                        if (!PC && dossier.getType().equals("PC")) // Si on travaille sur les PC et que le dossier n'est pas de type PC
                            continue;
                        if (!PA && dossier.getType().equals("PA")) // Si on travaille sur les PA et que le dossier n'est pas de type PA
                            continue;
                        if (!dossier.getEtatDossier().equals("Erreur numérotation") && !dossier.getNomDemandeur().equals("Inexistant"))
                            continue;

                        // On vérifie si on travaille seulement sur les dossiers non taxable, et si le dossier l'est ou pas
                        if (viewDossiers.getNonTaxablesCheckBox().isSelected() && !dossier.getEtatDossier().equalsIgnoreCase("NON TAXABLE"))
                            continue;

                        /*
                        On ajoute les éléments à la ligne courante puis on l'ajoute au modèle
                         */
                        data.addElement(dossier.getNumeroDossier());
                        data.addElement(dossier.getAnnee());
                        data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());
                        data.addElement(dossier.getNomDemandeur());
                        data.addElement(dossier.getEtatDossier());
                        data.addElement(commune.isEpci() ? "OUI" : "NON");

                        defaultTableModel.addRow(new Vector<>(data));
                        data.clear(); // On remet la ligne courante à zéro pour la prochaine ligne
                    }
                } else {
                    /*
                    Si  on travaille sur les dossiers manquants, on récupère les manquants de chaque type de dossiers
                    et on les concatène en une seule et même liste
                     */
                    List<Dossier> manquantsTotal = commune.rechercherManquantsAnnee(annee.getKey(), "DP");
                    List<Dossier> manquantsPC = commune.rechercherManquantsAnnee(annee.getKey(), "PC");
                    List<Dossier> manquantsPA = commune.rechercherManquantsAnnee(annee.getKey(), "PA");
                    manquantsTotal.addAll(manquantsPC);
                    manquantsTotal.addAll(manquantsPA);

                    /*
                    On parcours tous les dossiers
                     */
                    for (Dossier dossier : manquantsTotal) {
                        if (!DP && dossier.getType().equals("DP")) // Si on travaille sur les DP et que le dossier n'est pas de type DP
                            continue;
                        if (!PC && dossier.getType().equals("PC")) // Si on travaille sur les PC et que le dossier n'est pas de type PC
                            continue;
                        if (!PA && dossier.getType().equals("PA")) // Si on travaille sur les PA et que le dossier n'est pas de type PA
                            continue;

                        /*
                        On ajoute les éléments à la ligne courante puis on l'ajoute au modèle
                         */
                        data.addElement(dossier.getNumeroDossier());
                        data.addElement(dossier.getAnnee());
                        data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());
                        data.addElement(commune.isEpci() ? "OUI" : "NON");

                        defaultTableModel.addRow(new Vector<>(data));
                        data.clear(); // On remet à zéro la ligne courante pour la ligne suivante
                    }
                }
            }
        }

        // On vérifie s'il n'y a aucun dossier à afficher
        if (defaultTableModel.getRowCount() == 0)
            throw new IOException("Pas de dossiers à afficher");

        return defaultTableModel;
    }

    /**
     * Cette fonction permet de contruire le modèle d'affichage d'une liste d'information sur des dossiers taxés
     *
     * @param viewDossiers La vue contenant les paramètres de sélection des dossiers
     * @return Le modèle contenant la liste de dossiers
     * @throws IOException Si aucun type de dossier n'a été choisi, ou s'il n'y a aucune commune ou s'il n'y a aucun dossier à afficher
     */
    private ModelTable setModelTableTaxes(ViewDossiers viewDossiers) throws IOException {
        /*
        On récupère les types de dossiers à afficher
         */
        boolean PA = viewDossiers.getPACheckBox().isSelected();
        boolean PC = viewDossiers.getPCCheckBox().isSelected();
        boolean DP = viewDossiers.getDPCheckBox().isSelected();
        if (!PA && !PC && !DP) { // On vérifie qu'au moins un type de dossier a été sélectionné
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez choisir au moins un type de dossier");
        }

        if (viewDossiers.getCommunesEPCICheckBox().isSelected() && Integer.valueOf(view.getNbCommunesEPCI().getText()) == 0)
            throw new IOException("Aucune commune définie dans le groupe de communes");

        /*
        On définit les colonnes en fonction de si on travaille sur les dossiers enregistrés ou manquants
         */
        String[] colonnes = {"Numéro du dossier", "Commune", "Date de décision", "Date de vérification", "Type de TA", "Montant", "Type taxation", "Date d'échéance", "Date d'échéance 2"};

        if (ddt.getCommunes().size() == 0) // On vérifie qu'il y ai bien au moins une commune dans la DDT
            throw new IOException("Il n'existe aucune commune");

        /*
        On définit le modèle de la table en empechant les modification directs
         */
        ModelTable defaultTableModel = new ModelTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                Class<?> classe = String.class;
                if (columnIndex == 2 || columnIndex == 3 || columnIndex == 7 || columnIndex == 8)
                    classe = LocalDate.class;
                if (columnIndex == 5)
                    classe = Integer.class;
                return classe;
            }
        };
        for (String colonne : colonnes) // On ajoute les colonnes au modèle
            defaultTableModel.addColumn(colonne);

        // On recherche la commune saisie dans tous les cas, on vérifiera plus tard si on travaille sur une commune, les communes EPCI ou toutes
        Commune communeINSEE = ddt.rechercherCommuneINSEE(String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[1],
                String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[0]);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);

        /*
        On vérifie que les années sont bien ordonnées dans les choix
         */
        if (Integer.valueOf(anneesBox[0]) - Integer.valueOf(anneesBox[1]) > 0) {
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez ordonner les années");
        }

        List<String> annees = new ArrayList<>();
        for (int i = anneesCalculees; i >= 0; i--) {
            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            annees.add(anneeString); // On ajoute les années à un liste que l'on comparera plus tard
        }

        /*
        On parcours toutes les communes
         */
        Vector<Object> data = new Vector<>();

        // On récupère l'intégralité des dossiers de la commune (réduction de la complexité car un seul tri)

        /*
        On parcours toutes les années par clés
         */
        for (Commune commune : ddt.getCommunes()) {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected() && !commune.isEpci())
                continue;
            if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected() && (!commune.getDepartment().equals(communeINSEE.getDepartment()) || !commune.getNumeroINSEE().equals(communeINSEE.getNumeroINSEE())))
                continue;

            Map<String, List<DossierTaxe>> dossiers = commune.getDossiersTaxes();
            for (Map.Entry<String, List<DossierTaxe>> annee : dossiers.entrySet()) {
                // On vérifie si on travaille sur une ou plusieurs années et si l'année est comprise dans la liste d'années ou non
                if (!annees.contains(annee.getKey()) && !viewDossiers.getCheckBoxAnnee().isSelected())
                    continue;

            /*
            On parcours l'intégralité des dossiers
             */
                for (DossierTaxe dossier : dossiers.get(annee.getKey())) {
                    if (!DP && dossier.getType().equals("DP")) // Si on travaille sur les DP et que le dossier n'est pas de type DP
                        continue;
                    if (!PC && dossier.getType().equals("PC")) // Si on travaille sur les PC et que le dossier n'est pas de type PC
                        continue;
                    if (!PA && dossier.getType().equals("PA")) // Si on travaille sur les PA et que le dossier n'est pas de type PA
                        continue;
                    if (dossier.getTypeEIT().equals("2"))
                        continue;
                /*
                On ajoute les éléments à la ligne courante puis on l'ajoute au modèle
                 */
                    data.addElement(dossier.getNumeroDossier());
                    data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
                    data.addElement(dossier.getDateDecision() == null ? "" : dossier.getDateDecision().format(dateTimeFormatter));
                    data.addElement(dossier.getDateVerification().format(dateTimeFormatter));
                    data.addElement(dossier.getTypeTA());
                    data.addElement(dossier.getMontant());
                    data.addElement(dossier.getTypeTaxation());
                    data.addElement(dossier.getDateEcheance().format(dateTimeFormatter));
                    if (dossier.getDateEcheance2() != null)
                        data.addElement(dossier.getDateEcheance2().format(dateTimeFormatter));

                    defaultTableModel.addRow(new Vector<>(data));
                    data.clear(); // On remet la ligne courante à zéro pour la prochaine ligne
                }
            }
        }

        // On vérifie s'il n'y a aucun dossier à afficher
        if (defaultTableModel.getRowCount() == 0)
            throw new IOException("Pas de dossiers à afficher");

        return defaultTableModel;
    }

    /**
     * Cette fonction permet de contruire le modèle d'affichage d'un tableau croisé d'informations de TA
     *
     * @param viewDossiers La vue contenant les paramètres de sélection des dossiers
     * @return Le modèle contenant la liste de dossiers
     * @throws IOException Si aucun type de dossier n'a été choisi, ou s'il n'y a aucune commune ou s'il n'y a aucun dossier à afficher
     */
    private ModelTable setModelTableauxTA(ViewDossiers viewDossiers) throws IOException {
         /*
        On récupère les types de dossiers à afficher
         */
        boolean PA = viewDossiers.getPACheckBox().isSelected();
        boolean PC = viewDossiers.getPCCheckBox().isSelected();
        boolean DP = viewDossiers.getDPCheckBox().isSelected();
        if (!PA && !PC && !DP) { // On vérifie qu'au moins un type de dossier a été sélectionné
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez choisir au moins un type de dossier");
        }

        if (viewDossiers.getCommunesEPCICheckBox().isSelected() && Integer.valueOf(view.getNbCommunesEPCI().getText()) == 0)
            throw new IOException("Aucune commune définie dans le groupe de communes");

        /*
        On définit les nombres essentiels à compter
         */
        Map<String, List<Integer>> totauxAnnees = new HashMap<>(); // Totaux par année
        List<Integer> zeroAnnee = new ArrayList<>();
        for (int i = 0; i < 8; i++)
            zeroAnnee.add(0);

        /*
        On définit les colonnes en fonction de si on travaille sur les dossiers enregistrés ou manquants
         */
        String[] colonnes = {"Commune", "Année", "Nombre EIT DP", "Nombre EIT PC", "Nombre EIT PA", "Montant TI", "Montant TM", "Montant TR", "Montant AN", "Total Montant"};

        if (ddt.getCommunes().size() == 0) // On vérifie qu'il y ai bien au moins une commune dans la DDT
            throw new IOException("Il n'existe aucune commune");

        /*
        On définit le modèle de la table en empechant les modification directs
         */
        ModelTable defaultTableModel = new ModelTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                Class<?> classe = Integer.class;
                if (columnIndex == 0 || columnIndex == 1)
                    classe = String.class;
                return classe;
            }
        };
        for (String colonne : colonnes) // On ajoute les colonnes au modèle
            defaultTableModel.addColumn(colonne);

        // On recherche la commune saisie dans tous les cas, on vérifiera plus tard si on travaille sur une commune, les communes EPCI ou toutes
        Commune communeINSEE = ddt.rechercherCommuneINSEE(String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[1],
                String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[0]);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);

        /*
        On vérifie que les années sont bien ordonnées dans les choix
         */
        if (Integer.valueOf(anneesBox[0]) - Integer.valueOf(anneesBox[1]) > 0) {
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez ordonner les années");
        }


        /*
        On parcours toutes les communes
         */
        Vector<Object> data = new Vector<>();

        /*
        On parcours toutes les années par clés
         */
        for (Commune commune : ddt.getCommunes()) {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected() && !commune.isEpci())
                continue;
            if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected() && (!commune.getDepartment().equals(communeINSEE.getDepartment()) || !commune.getNumeroINSEE().equals(communeINSEE.getNumeroINSEE())))
                continue;

            data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());

            for (int i = anneesCalculees; i >= 0; i--) {
                int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
                String anneeString = String.valueOf(anneeInt);

                int montantTI = commune.getMontantTotal(anneeString, "TI");
                int montantTM = commune.getMontantTotal(anneeString, "TM");
                int montantTR = commune.getMontantTotal(anneeString, "TR");
                int montantAN = commune.getMontantTotal(anneeString, "AN");
                int nbDP = commune.getAnneeTypeTaxes(anneeString, "DP").size();
                int nbPC = commune.getAnneeTypeTaxes(anneeString, "PC").size();
                int nbPA = commune.getAnneeTypeTaxes(anneeString, "PA").size();

                totauxAnnees.putIfAbsent(anneeString, new ArrayList<>(zeroAnnee)); // On créé une année et une liste si les totaux de l'année en cours n'ont pas encore été calculés
                int totalTI = totauxAnnees.get(anneeString).get(0);
                totalTI += montantTI;
                totauxAnnees.get(anneeString).set(0, totalTI);

                int totalTM = totauxAnnees.get(anneeString).get(1);
                totalTM += montantTM;
                totauxAnnees.get(anneeString).set(1, totalTM);

                int totalTR = totauxAnnees.get(anneeString).get(2);
                totalTR += montantTR;
                totauxAnnees.get(anneeString).set(2, totalTR);

                int totalAN = totauxAnnees.get(anneeString).get(3);
                totalAN += montantAN;
                totauxAnnees.get(anneeString).set(3, totalAN);

                int totalDP = totauxAnnees.get(anneeString).get(4);
                totalDP += nbDP;
                totauxAnnees.get(anneeString).set(4, totalDP);

                int totalPC = totauxAnnees.get(anneeString).get(5);
                totalPC += nbPC;
                totauxAnnees.get(anneeString).set(5, totalPC);

                int totalPA = totauxAnnees.get(anneeString).get(6);
                totalPA += nbPA;
                totauxAnnees.get(anneeString).set(6, totalPA);

                if (data.size() == 0)
                    data.addElement(null);

                data.addElement(anneeString);
                data.addElement(nbDP);
                data.addElement(nbPC);
                data.addElement(nbPA);
                data.addElement(montantTI);
                data.addElement(montantTM);
                data.addElement(montantTR);
                data.addElement(montantAN);
                data.addElement(montantTI + montantAN + montantTM + montantTR);

                defaultTableModel.addRow(new Vector<>(data));
                data.clear(); // On remet la ligne courante à zéro pour la prochaine ligne
            }
            data.clear();
        }


        for (int i = anneesCalculees; i >= 0; i--) {
            if (i == anneesCalculees) {
                data.addElement("Total");
            } else {
                data.addElement(null);
            }

            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            totauxAnnees.putIfAbsent(anneeString, new ArrayList<>(zeroAnnee));

            int total = totauxAnnees.get(anneeString).get(7);
            total += totauxAnnees.get(anneeString).get(3) + totauxAnnees.get(anneeString).get(2) + totauxAnnees.get(anneeString).get(1) + totauxAnnees.get(anneeString).get(0);
            totauxAnnees.get(anneeString).set(7, total);

            data.addElement(anneeString);
            data.addElement(totauxAnnees.get(anneeString).get(4));
            data.addElement(totauxAnnees.get(anneeString).get(5));
            data.addElement(totauxAnnees.get(anneeString).get(6));
            data.addElement(totauxAnnees.get(anneeString).get(0));
            data.addElement(totauxAnnees.get(anneeString).get(1));
            data.addElement(totauxAnnees.get(anneeString).get(2));
            data.addElement(totauxAnnees.get(anneeString).get(3));
            data.addElement(totauxAnnees.get(anneeString).get(7));

            defaultTableModel.addRow(new Vector<>(data));
            data.clear();
        }

        // On vérifie s'il n'y a aucun dossier à afficher
        if (defaultTableModel.getRowCount() == 0)
            throw new IOException("Pas de dossiers à afficher");

        return defaultTableModel;
    }

    /**
     * Cette fonction permet de contruire le modèle d'affichage d'un tableau croisé d'informations de TA
     *
     * @param viewDossiers La vue contenant les paramètres de sélection des dossiers
     * @return Le modèle contenant la liste de dossiers
     * @throws IOException Si aucun type de dossier n'a été choisi, ou s'il n'y a aucune commune ou s'il n'y a aucun dossier à afficher
     */
    private ModelTable setModelTableauxTA2(ViewDossiers viewDossiers) throws IOException {
         /*
        On récupère les types de dossiers à afficher
         */
        boolean PA = viewDossiers.getPACheckBox().isSelected();
        boolean PC = viewDossiers.getPCCheckBox().isSelected();
        boolean DP = viewDossiers.getDPCheckBox().isSelected();
        if (!PA && !PC && !DP) { // On vérifie qu'au moins un type de dossier a été sélectionné
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez choisir au moins un type de dossier");
        }

        if (viewDossiers.getCommunesEPCICheckBox().isSelected() && Integer.valueOf(view.getNbCommunesEPCI().getText()) == 0)
            throw new IOException("Aucune commune définie dans le groupe de communes");

        /*
        On définit les nombres essentiels à compter
         */
        Map<String, List<Integer>> totauxAnnees = new HashMap<>(); // Totaux par année
        List<Integer> zeroAnnee = new ArrayList<>();
        for (int i = 0; i < 8; i++)
            zeroAnnee.add(0);

        /*
        On définit les colonnes en fonction de si on travaille sur les dossiers enregistrés ou manquants
         */
        String[] colonnes = {"Commune", "Année", "Nombre EIT DP", "Nombre EIT PC", "Nombre EIT PA", "Montant TA", "Montant RAP", "Total Montant"};

        if (ddt.getCommunes().size() == 0) // On vérifie qu'il y ai bien au moins une commune dans la DDT
            throw new IOException("Il n'existe aucune commune");

        /*
        On définit le modèle de la table en empechant les modification directs
         */
        ModelTable defaultTableModel = new ModelTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                Class<?> classe = Integer.class;
                if (columnIndex == 0 || columnIndex == 1)
                    classe = String.class;
                return classe;
            }
        };
        for (String colonne : colonnes) // On ajoute les colonnes au modèle
            defaultTableModel.addColumn(colonne);

        // On recherche la commune saisie dans tous les cas, on vérifiera plus tard si on travaille sur une commune, les communes EPCI ou toutes
        Commune communeINSEE = ddt.rechercherCommuneINSEE(String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[1],
                String.valueOf(viewDossiers.getCommuneBox().getSelectedItem()).split(" ")[0]);

        /*
        On récupère les années dans tous les cas, on vérifiera plus tard si on travaille sur une année, plusieurs ou toutes
         */
        String[] anneesBox = {Objects.requireNonNull(viewDossiers.getYearBox().getSelectedItem()).toString(), Objects.requireNonNull(viewDossiers.getYearBoxDeux().getSelectedItem()).toString()};
        int anneesCalculees = Integer.valueOf(anneesBox[1]) - Integer.valueOf(anneesBox[0]);

        /*
        On vérifie que les années sont bien ordonnées dans les choix
         */
        if (Integer.valueOf(anneesBox[0]) - Integer.valueOf(anneesBox[1]) > 0) {
            viewDossiers.setEnabled(true);
            view.setVisible(true);
            viewDossiers.setVisible(true);
            throw new IOException("Veuillez ordonner les années");
        }


        /*
        On parcours toutes les communes
         */
        Vector<Object> data = new Vector<>();

        /*
        On parcours toutes les années par clés
         */
        for (Commune commune : ddt.getCommunes()) {
            if (viewDossiers.getCommunesEPCICheckBox().isSelected() && !commune.isEpci())
                continue;
            if (!viewDossiers.getCommunesEPCICheckBox().isSelected() && !viewDossiers.getCheckBoxCommunes().isSelected() && (!commune.getDepartment().equals(communeINSEE.getDepartment()) || !commune.getNumeroINSEE().equals(communeINSEE.getNumeroINSEE())))
                continue;

            data.addElement(commune.getDepartment() + " " + commune.getNumeroINSEE() + " " + commune.getNomCommune());

            for (int i = anneesCalculees; i >= 0; i--) {
                int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
                String anneeString = String.valueOf(anneeInt);

                int montantTA = commune.getMontantTotalTA(anneeString, "TA");
                int montantRAP = commune.getMontantTotalTA(anneeString, "RAP2012");

                int nbDP = commune.getAnneeTypeTaxes(anneeString, "DP").size();
                int nbPC = commune.getAnneeTypeTaxes(anneeString, "PC").size();
                int nbPA = commune.getAnneeTypeTaxes(anneeString, "PA").size();

                totauxAnnees.putIfAbsent(anneeString, new ArrayList<>(zeroAnnee)); // On créé une année et une liste si les totaux de l'année en cours n'ont pas encore été calculés
                int totalTA = totauxAnnees.get(anneeString).get(0);
                totalTA += montantTA;
                totauxAnnees.get(anneeString).set(0, totalTA);

                int totalRAP = totauxAnnees.get(anneeString).get(1);
                totalRAP += montantRAP;
                totauxAnnees.get(anneeString).set(1, totalRAP);

                int totalDP = totauxAnnees.get(anneeString).get(4);
                totalDP += nbDP;
                totauxAnnees.get(anneeString).set(4, totalDP);

                int totalPC = totauxAnnees.get(anneeString).get(5);
                totalPC += nbPC;
                totauxAnnees.get(anneeString).set(5, totalPC);

                int totalPA = totauxAnnees.get(anneeString).get(6);
                totalPA += nbPA;
                totauxAnnees.get(anneeString).set(6, totalPA);

                if (data.size() == 0)
                    data.addElement(null);

                data.addElement(anneeString);
                data.addElement(nbDP);
                data.addElement(nbPC);
                data.addElement(nbPA);
                data.addElement(montantTA);
                data.addElement(montantRAP);
                data.addElement(montantTA + montantRAP);

                defaultTableModel.addRow(new Vector<>(data));
                data.clear(); // On remet la ligne courante à zéro pour la prochaine ligne
            }
            data.clear();
        }


        for (int i = anneesCalculees; i >= 0; i--) {
            if (i == anneesCalculees) {
                data.addElement("Total");
            } else {
                data.addElement(null);
            }

            int anneeInt = Integer.valueOf(anneesBox[1].substring(2, 4)) - i;
            String anneeString = String.valueOf(anneeInt);
            totauxAnnees.putIfAbsent(anneeString, new ArrayList<>(zeroAnnee));

            int total = totauxAnnees.get(anneeString).get(7);
            total += totauxAnnees.get(anneeString).get(1) + totauxAnnees.get(anneeString).get(0);
            totauxAnnees.get(anneeString).set(7, total);

            data.addElement(anneeString);
            data.addElement(totauxAnnees.get(anneeString).get(4));
            data.addElement(totauxAnnees.get(anneeString).get(5));
            data.addElement(totauxAnnees.get(anneeString).get(6));
            data.addElement(totauxAnnees.get(anneeString).get(0));
            data.addElement(totauxAnnees.get(anneeString).get(1));
            data.addElement(totauxAnnees.get(anneeString).get(7));

            defaultTableModel.addRow(new Vector<>(data));
            data.clear();
        }

        // On vérifie s'il n'y a aucun dossier à afficher
        if (defaultTableModel.getRowCount() == 0)
            throw new IOException("Pas de dossiers à afficher");

        return defaultTableModel;
    }

    /**
     * Fonction initialisant le menu
     */
    private void initJTree() {
        JTree tree = view.getMenu();
        tree.setModel(Menu.toJTree());

        for (int row = 0; row < tree.getRowCount(); row++)
            tree.expandRow(row);

        tree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (openeddialog != null)
                    openeddialog.dispose();
                JTree jTree = (JTree) e.getSource();
                TreePath path = jTree.getPathForLocation(e.getX(), e.getY());
                if (path != null) {
                    DefaultMutableTreeNode item = (DefaultMutableTreeNode) path.getLastPathComponent();

                    //DefaultMutableTreeNode item = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

                    if (item != null) {
                    /*
                    Pour les dossiers
                    */
                        if (item.toString().equals(Menu.getImportFiles())) {
                            importerDossiers();
                        }
                        if (item.toString().equals(Menu.getDisplayFiles())) {
                            openeddialog = new ViewDossiers(view, "Edition d'une liste de dossiers", false);
                            initEditionDossiers((ViewDossiers) openeddialog); // Initialisation des composants swing de la vue
                            ((ViewDossiers) openeddialog).ouvrir(); // Ouverture de la vue
                        }
                        if (item.toString().equals(Menu.getDisplayPivotTables()) && item.getParent().toString().equals(Menu.getDossiers())) {
                            openeddialog = new ViewDossiers(view, "Edition de tableaux croisés", false);
                            editionTableauxCroises((ViewDossiers) openeddialog); // Initialisation des composants de l'édition de tableaux croisés
                        }
                        if (item.toString().equals(Menu.getEditFiles())) {
                            openeddialog = new ViewModifierDossier(view, "Modifier un dossier", false);
                            modifierDossier((ViewModifierDossier) openeddialog); // Initialisation des composants pour la modification d'un dossier
                            openDialog(openeddialog); // Ouverture de la vue de la modification d'un dossier
                        }
                        if (item.toString().equals(Menu.getDeleteYearFiles())) {
                            supprimerListeDossiers();
                        }
                        if (item.toString().equals(Menu.getDeleteFile())) {
                            try {
                                supprimerDossier(); // Réglage de la suppression  d'un dossier
                            } catch (IOException e1) {
                                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                                new Notification(view, "Erreur", e1.getMessage());
                            }
                        }

                    /*
                    Pour les communes
                    */
                        if (item.toString().equals(Menu.getImportCommunes())) {
                            importerCommunes();
                        }
                        if (item.toString().equals(Menu.getAddCommune())) {
                            openeddialog = new ViewAddCommune(view, "Ajouter une commune", false);
                            ajouterCommuneComponent((ViewAddCommune) openeddialog);
                            //openDialog(openeddialog);
                        }
                        if (item.toString().equals(Menu.getEditCommune())) {
                            openeddialog = new ViewModifierCommune(view, "Modifier une commune", false);
                            try {
                                modifierCommune((ViewModifierCommune) openeddialog);
                            } catch (IOException e1) {
                                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                                new Notification(view, "Erreur", e1.getMessage());
                            }
                        }
                        if (item.toString().equals(Menu.getDeleteCommune())) {
                            try {
                                supprimerCommune();
                            } catch (IOException e1) {
                                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                                new Notification(view, "Erreur", e1.getMessage());
                            }
                        }
                        if (item.toString().equals(Menu.getAddException())) {
                            openeddialog = new ViewExceptions(view, "Définition d'exceptions", false);
                            initComboBoxCommunes(((ViewExceptions) openeddialog).getBoxCommunes());
                            initBoxType(((ViewExceptions) openeddialog).getBoxType());
                            try {
                                ajouterException((ViewExceptions) openeddialog);
                            } catch (IOException e1) {
                                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                                new Notification(view, "Erreur", e1.getMessage());
                            }
                        }
                        if (item.toString().equals(Menu.getImportException())) {
                            importerException();
                        }
                        if (item.toString().equals(Menu.getDeleteException())) {
                            try {
                                supprimerException();
                            } catch (IOException e1) {
                                ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                                new Notification(view, "Information", e1.getMessage());
                            }
                        }

                    /*
                    Pour les taxes
                    */
                        if (item.toString().equals(Menu.getImportTaxes())) {
                            importerTaxes();
                        }
                        if (item.toString().equals(Menu.getDisplayTaxes())) {
                            openeddialog = new ViewDossiers(view, "Visualisation d'information sur les montants de taxes de dossiers", false);
                            editionDossiersTaxes((ViewDossiers) openeddialog);
                            ((ViewDossiers) openeddialog).ouvrir();
                        }
                        if (item.toString().equals(Menu.getDisplayPTTaxes()) && item.getParent().toString().equals(Menu.getTaxes())) {
                            openeddialog = new ViewDossiers(view, "Edition de tableaux croisés d'information de TA", false);
                            editionTableauxInfos((ViewDossiers) openeddialog);
                            ((ViewDossiers) openeddialog).ouvrir();
                        }
                        if (item.toString().equals(Menu.getDeleteYearTaxes())) {
                            supprimerAnneeInfo();
                        }

                    /*
                    Pour l'app
                    */
                        if (item.toString().contains(Menu.getConversion())) {
                            formatageXLS();
                        }
                        if (item.toString().equals(Menu.getFormatageFichier())) {
                            formatageNonTaxable();
                        }

                    /*
                    Pour le formatage
                    */
                        if (item.toString().equals(Menu.getAppGest())) {
                            openeddialog = new ViewParam(view, "Modifier l'entête d'exportation", false);
                            modifierEntete((ViewParam) openeddialog);
                        }
                        if (item.toString().equals(Menu.getDeleteData())) {
                            int retour = JOptionPane.showConfirmDialog(view, "Êtes vous sûr de vouloir supprimer l'intégralité des données ?", "Suppression des données", JOptionPane.YES_NO_OPTION);
                            if (retour == JOptionPane.YES_OPTION) {
                                ddt.supprimerDonnees();
                                view.setTitle(ddt.getTitreApp());
                                setModelTableCommunes();
                            }
                        }

                        if (item.toString().equals(Menu.getDoc())) {
                            if (documentation.isOpen())
                                documentation.toFront();
                            else
                                documentation.open();
                        }

                        if (item.toString().equals(Menu.getLogs())) {
                            displayLogs();
                        }
                    }
                }
            }
        });

        tree.setCellRenderer(new Menu.MyRenderer());

        tree.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                JTree jTree = (JTree) e.getSource();
                TreePath path = jTree.getPathForLocation(e.getX(), e.getY());
                //int row = jTree.getRowForLocation(e.getX(), e.getY());
                if (path != null) {
                    jTree.setCursor(new Cursor(Cursor.HAND_CURSOR));
                } else {
                    jTree.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });
    }

    private void displayLogs() {
        openeddialog = new Logs(view, ddt.getLogs());
    }

    private void importerException() {
        AtomicReference<String> fichierExceptions = new AtomicReference<>();
        openeddialog = new ViewImport(view, "Importation d'une liste d'exceptions", false);
        ((ViewImport) openeddialog).getFieldAnnee().setVisible(false);
        ((ViewImport) openeddialog).getComboBoxAnnee().setVisible(false);

        ((ViewImport) openeddialog).getFileChooserButton().addActionListener(e -> {
            FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("Texte CSV (*.csv)", "csv");

            // Choix du fichier des communes
            JFileChooser fichierCommunes = new JFileChooser();
            fichierCommunes.setDialogTitle("Choix du fichier des exceptions");
            fichierCommunes.setCurrentDirectory(properties.getProperty("importExceptions").equals("") || !new File(properties.getProperty("importExceptions")).exists() ? cheminCourant : new File(properties.getProperty("importExceptions")));
            fichierCommunes.setAcceptAllFileFilterUsed(false);
            fichierCommunes.addChoosableFileFilter(fileFilter);
            int valeur = fichierCommunes.showOpenDialog(openeddialog);
            if (valeur != JFileChooser.CANCEL_OPTION) {
                properties.setProperty("importExceptions", fichierCommunes.getSelectedFile().getParent());
                fichierExceptions.set(fichierCommunes.getSelectedFile().toString());
                ((ViewImport) openeddialog).getTextFieldFiles().setText(fichierExceptions.get());
            }
        });

        ((ViewImport) openeddialog).getImporterButton().addActionListener(e -> {
            if (fichierExceptions.get() != null) {
                try {
                    ddt.parseException(fichierExceptions.toString());
                    new Notification(view, "Information", "Importation des exceptions terminée");
                    openeddialog.dispose();
                } catch (IOException e1) {
                    ddt.getLogs().append(Arrays.toString(e1.getStackTrace())).append("\n");
                    new Notification(view, "Erreur", e1.getMessage());
                }
            } else {
                new Notification(view, "Information", "Vous n'avez pas ouvert de fichier contenant des exceptions");
            }
        });
        ((ViewImport) openeddialog).ouvrirPetit();
    }
}

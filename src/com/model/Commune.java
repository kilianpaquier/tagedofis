package com.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 * @author Kilian Paquier
 * Classe Commune correspondant à une commune d'un département
 */
public class Commune {
    //private static final long serialVersionUID = 2587801773079610981L; // La version de sérialisation

    private Map<String, List<Dossier>> dossiers = new HashMap<>(); // La map contenant pour chaque année une liste de dossiers
    private Map<String, List<DossierTaxe>> dossiersTaxes = new HashMap<>();
    private String numeroINSEE; // Le numéro INSEE de la commune
    private String nomCommune; // Le nom de la commune
    private Boolean epci; // Si la commune fait partie de l'EPCI du départmenent
    private String department;
    private List<NumberException> exceptions = new ArrayList<>();

    /**
     * Constructeur de confort prenant en paramètre un numéro INSEE, un nom de commune et un département
     * @param numeroINSEE Le numéro INSEE de la commune
     * @param nomCommune Le nom de la commune
     * @param department Le département de la commune
     */
    public Commune(String numeroINSEE, String nomCommune, String department) {
        this.numeroINSEE = numeroINSEE.trim();
        this.nomCommune = nomCommune.trim();
        this.epci = Boolean.FALSE;
        this.department = department;
    }

    /**
     * Constructeur de confort prenant en paramètre un numéro INSEE, un nom de commune, un département un un booléen EPCI
     * @param numeroINSEE Le numéro INSEE de la commune
     * @param nomCommune Le nom de la commune
     * @param department Le département de la commune
     * @param EPCI Le booléen disant si la commune est EPCI ou non
     */
    public Commune (String numeroINSEE, String nomCommune, String department, Boolean EPCI) {
        this.numeroINSEE = numeroINSEE.trim();
        this.nomCommune = nomCommune.trim();
        this.epci = EPCI;
        this.department = department;
    }

    /**
     * Cette fonction tri les listes de dossiers de chaque années de la map
     */
    private void triDossiers() {
        for (List<Dossier> dossiers : this.dossiers.values()) {
            List<Dossier> typePA = new ArrayList<>();
            List<Dossier> typePC = new ArrayList<>();
            List<Dossier> typeDP = new ArrayList<>();
            for(Dossier dossier : dossiers) {
                if (dossier.getType().equals("PA"))
                    typePA.add(dossier);
                if (dossier.getType().equals("DP"))
                    typeDP.add(dossier);
                if (dossier.getType().equals("PC"))
                    typePC.add(dossier);
            }
            typePC.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
            typeDP.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
            typePA.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
            dossiers.clear();
            dossiers.addAll(typeDP);
            dossiers.addAll(typePC);
            dossiers.addAll(typePA);
        }
    }

    /**
     * Cette fonction tri par numéro de dossier tous les dossiers taxés
     */
    private void triDossiersTaxes() {
        for (List<DossierTaxe> dossierTaxes : this.dossiersTaxes.values()) {
            List<DossierTaxe> typePA = new ArrayList<>();
            List<DossierTaxe> typePC = new ArrayList<>();
            List<DossierTaxe> typeDP = new ArrayList<>();
            for(DossierTaxe dossier : dossierTaxes) {
                if (dossier.getType().equals("PA"))
                    typePA.add(dossier);
                if (dossier.getType().equals("DP"))
                    typeDP.add(dossier);
                if (dossier.getType().equals("PC"))
                    typePC.add(dossier);
            }
            typePC.sort(Comparator.comparing(DossierTaxe::getNumDossier));
            typeDP.sort(Comparator.comparing(DossierTaxe::getNumDossier));
            typePA.sort(Comparator.comparing(DossierTaxe::getNumDossier));
            dossierTaxes.clear();
            dossierTaxes.addAll(typeDP);
            dossierTaxes.addAll(typePC);
            dossierTaxes.addAll(typePA);
        }
    }

    /**
     * Cette fonction ajoute un dossier à la liste des dossiers en fonction de son année
     * @param dossier Le dossier à ajouter
     * @param annee L'année de depôt du dossier
     */
    void addDossier(Dossier dossier, String annee) {
        Dossier dossier1 = rechercherDossier(dossier, annee);
        if (dossier1 == null)
            dossiers.get(annee).add(dossier);
        else if (dossier1 != null && dossier1.getEtatDossier().contains("Saisie en cours") && !dossier.getEtatDossier().contains("Saisie en cours")) {
            supprimerDossier(dossier1, annee);
            dossiers.get(annee).add(dossier);
        }
    }

    /**
     * Cette fonction recherche un dossier et le retourne
     * @param dossier Le dossier que l'on recherche dans la liste correspondant à son année de dépôt
     * @param annee L'année du dossier
     * @return Le dossier s'il existe
     */
    private Dossier rechercherDossier(Dossier dossier, String annee) {
        dossiers.computeIfAbsent(annee, k -> new ArrayList<>());
        for (Dossier dossier1 : getAnneeTypeWithoutInexistants(annee, dossier.getType())) {
            if (dossier1.getNumeroDossier().equals(dossier.getNumeroDossier()))
                return dossier1;
        }
        return null;
    }

    /**
     * Cette fonction supprime un dossier de la liste des dossiers
     * @param dossier Le dossier à supprimer
     * @param annee L'année de dépôt du dossier
     */
    private void supprimerDossier(Dossier dossier, String annee) {
        dossiers.get(annee).remove(dossier);
    }

    /**
     * Cette fonction permet d'ajouter les informations sur un dossier taxé
     * @param dossierTaxe Le dossier taxé et ses informations
     */
    void addDossierTaxe(DossierTaxe dossierTaxe) {
        if (dossierTaxe.getTypeTA().equals("RAP2012"))
            dossierTaxe.setTypeEIT("");

        if (rechercherDossierTaxe(dossierTaxe) == null) {
            if (dossierTaxe.getDateDecision() != null)
                dossiersTaxes.get(String.valueOf(dossierTaxe.getDateDecision().getYear()).substring(2, 4)).add(dossierTaxe);
            else
                dossiersTaxes.get(dossierTaxe.getAnnee()).add(dossierTaxe);
        }
    }

    /**
     * Cette fonction recherche et fusionne les dossiers dont l'EIT2 et l'EIT1 existent
     */
    public void fusionnerUneSeuleEcheanceTaxes() {
        triDossiersTaxes();
        for (Map.Entry annee : dossiersTaxes.entrySet()) {
            for (DossierTaxe dossierTaxe : dossiersTaxes.get(annee.getKey().toString())) {
                if (dossierTaxe.getTypeEIT().equals("1") || dossierTaxe.getTypeTA().equals("RAP2012"))
                    continue;

                DossierTaxe EIT1 = rechercherEIT1(dossierTaxe);
                if (EIT1 != null) {
                    if (EIT1.getDateEcheance().getYear() == dossierTaxe.getDateEcheance().getYear()) {
                        EIT1.setTypeEIT("Une seule échéance");
                        EIT1.ajouterMontant(dossierTaxe.getMontant());
                    }
                    else {
                        EIT1.setTypeEIT("Deux échéances");
                        EIT1.ajouterMontant(dossierTaxe.getMontant());
                        EIT1.setDateEcheance2(dossierTaxe.getDateEcheance());
                    }
                }
            }
        }
    }

    /**
     * Cette fonction recherche l'EIT1 d'un dossierTaxe à partir de son EIT2
     * @param dossierTaxe l'EIT2 du dossierTaxe
     * @return L'EIT1 du dossierTaxe s'il existe
     */
    private DossierTaxe rechercherEIT1(DossierTaxe dossierTaxe) {
        for (List<DossierTaxe> dossierTaxes : dossiersTaxes.values()) {
            for (DossierTaxe dossierTaxe1 : dossierTaxes) {
                if (dossierTaxe1.getTypeEIT().equals("1") && dossierTaxe.equalsWithoutEIT(dossierTaxe1))
                    return dossierTaxe1;
            }
        }

        return null;
    }

    /**
     * Cette fonction retourne le dossierTaxe annulé ou rectifié du dossierTaxe initial
     * @param dossierTaxe Le dossierTaxe initial
     * @return Le dosierTaxe annulé ou rectifié
     */
    public DossierTaxe rechercherTR(DossierTaxe dossierTaxe) {
        if (dossierTaxe.getTypeTaxation().contains("TR") || dossierTaxe.getTypeTaxation().contains("TM") || dossierTaxe.getTypeTaxation().contains("AN"))
            return null;
        for (List<DossierTaxe> dossierTaxes : dossiersTaxes.values()) {
            for (DossierTaxe dossierTaxe1 : dossierTaxes) {
                if (dossierTaxe1.getNumeroDossier().equals(dossierTaxe.getNumeroDossier()) && dossierTaxe1.getTypeTA().equals(dossierTaxe.getTypeTA())
                        && (dossierTaxe1.getTypeTaxation().contains("TR") || dossierTaxe1.getTypeTaxation().contains("AN")))
                    return dossierTaxe1;
            }
        }
        return null;
    }

    /**
     * Cette fonction retourne le dossier transféré d'un dossier initial
     * @param dossierTaxe Le dossier initial
     * @return Le dossier transféré s'il existe
     */
    public DossierTaxe rechercherTransfert(DossierTaxe dossierTaxe) {
        if (dossierTaxe.getTypeTaxation().contains("TR") || dossierTaxe.getTypeTaxation().contains("TM") || dossierTaxe.getTypeTaxation().contains("AN"))
            return null;
        for (List<DossierTaxe> dossierTaxes : dossiersTaxes.values()) {
            for (DossierTaxe dossierTaxe1 : dossierTaxes) {
                if (dossierTaxe1.getNumeroDossier().contains(dossierTaxe.getNumeroDossier() + "-T"))
                    return dossierTaxe1;
            }
        }
        return null;
    }

    /**
     * Cette fonction recherche et retourne s'il existe les informations sur un dossier taxé
     * @param dossierTaxe Le dossier taxé dont on veut savoir s'il existe
     * @return Le dossier s'il existe
     */
    private DossierTaxe rechercherDossierTaxe(DossierTaxe dossierTaxe) {
        if (dossierTaxe.getDateDecision() != null) {
            String year = String.valueOf(dossierTaxe.getDateDecision().getYear()).substring(2, 4);
            dossiersTaxes.computeIfAbsent(year, k -> new ArrayList<>());

            for (DossierTaxe dossierTaxe1 : getAnneeTypeTaxes(year, dossierTaxe.getType())) {
                if (dossierTaxe.equalsWithoutEIT(dossierTaxe1)) {
                    if (dossierTaxe.equalsWithEIT(dossierTaxe1))
                        return dossierTaxe1;
                    if (dossierTaxe1.getTypeEIT().equals("Une seule échéance") || dossierTaxe1.getTypeEIT().equals("Deux échéances"))
                        return dossierTaxe1;
                }
            }
        } else {
            dossiersTaxes.computeIfAbsent(dossierTaxe.getAnnee(), k -> new ArrayList<>());

            for (DossierTaxe dossierTaxe1 : getAnneeTypeTaxes(dossierTaxe.getAnnee(), dossierTaxe.getType())) {
                if (dossierTaxe.equalsWithoutEIT(dossierTaxe1)) {
                    if (dossierTaxe.equalsWithEIT(dossierTaxe1))
                        return dossierTaxe1;
                    if (dossierTaxe1.getTypeEIT().equals("Une seule échéance") || dossierTaxe1.getTypeEIT().equals("Deux échéances"))
                        return dossierTaxe1;
                }
            }
        }
        return null;
    }

    /**
     * Cette fonction retourne la map contenant les listes de dossiers pour chaque année
     * @return La map contenant les listes de dossiers
     */
    public Map<String, List<Dossier>> getDossiers() {
        triDossiers();
        return dossiers;
    }

    /**
     * Cette fonction retourne le numéro INSEE de la commune
     * @return Le numéro INSEE de la commune
     */
    public String getNumeroINSEE() {
        return numeroINSEE;
    }

    /**
     * Cette fonction change le numéro INSEE de la commune (utilisée pour la sérialisation)
     * @param numeroINSEE Le nouveau numéro INSEE de la commune
     */
    private void setNumeroINSEE(String numeroINSEE) {
        this.numeroINSEE = numeroINSEE;
    }

    /**
     * Cette fonction recupère le nom de la commune
     * @return Le nom de la commune
     */
    public String getNomCommune() {
        return nomCommune;
    }

    /**
     * Cette fonction change le nom de la commune (utilisée pour la sérialisation)
     * @param nomCommune Le nouveau nom de la commune
     */
    public void setNomCommune(String nomCommune) {
        this.nomCommune = nomCommune;
    }

    /**
     * Cette fonction permet de changer l'intégralité de la map des listes de dossiers (utilisée pour la sérialisation)
     * @param dossiers La map contenant toutes les listes de dossiers
     */
    private void setDossiers(Map<String, List<Dossier>> dossiers) {
        this.dossiers = dossiers;
    }

    /**
     * Cette fonction permet de définir le choix des attributs à enregistrer lors de la sérialisation
     * @param objectOutputStream L'objectOutputStream qui écrit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de l'écrire ou que l'objectOutputStream est null
     */
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(department);
        objectOutputStream.flush();
        objectOutputStream.writeObject(numeroINSEE);
        objectOutputStream.flush();
        objectOutputStream.writeObject(nomCommune);
        objectOutputStream.flush();
        objectOutputStream.writeObject(epci);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dossiers);
        objectOutputStream.flush();
        objectOutputStream.writeObject(dossiersTaxes);
        objectOutputStream.flush();
    }

    /**
     * Cette fonction permet de lire l'objet précédemment enregistré correctement
     * @param objectInputStream L'objectInputStream qui lit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de la lecture ou que l'objectInputStream est null
     * @throws ClassNotFoundException Si la classe de cast n'existe pas
     */
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        setDepartment((String) objectInputStream.readObject());
        setNumeroINSEE((String) objectInputStream.readObject());
        setNomCommune((String) objectInputStream.readObject());
        setEpci((Boolean) objectInputStream.readObject());
        setDossiers((Map<String, List<Dossier>>) objectInputStream.readObject());
        setDossiersTaxes((Map<String, List<DossierTaxe>>) objectInputStream.readObject());
    }

    /**
     * Cette fonction permet de savoir si la commune fait partie de l'EPCI du départment
     * @return Le Boolean correspondant à true si la commune fait partie de l'EPCI
     */
    public Boolean isEpci() {
        return epci;
    }

    /**
     * Cette fonction permet de dire si la commune fait partie de l'EPCI du départment ou nom
     * @param epci Le booléan correspondant à true si on veut que la commune fasse partie de l'EPCI
     */
    public void setEpci(boolean epci) {
        this.epci = epci;
    }

    /**
     * Cette fonction permet de récupérer une liste de dossiers correspondant à une année précise et à un type de dossier précis
     * @param annee L'année des dossiers à récupérer
     * @param type Le type de dossier à récupérer
     * @return La liste des dossiers
     */
    public List<Dossier> getAnneeTypeWithoutInexistants(String annee, String type) {
        List<Dossier> dossiers = new ArrayList<>();

        if (this.dossiers.get(annee) != null) {
            for (Dossier dossier : this.dossiers.get(annee)) {
                if (dossier.getNomDemandeur().equals("Inexistant") && dossier.getEtatDossier().equals("Erreur numérotation"))
                    continue;
                if (dossier.getType().equals(type))
                    dossiers.add(dossier);
            }
        }

        dossiers.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
        return dossiers;
    }

    /**
     * Cette fonction permet de récupérer une liste de dossiers correspondant à une année précise et à un type de dossier précis
     *
     * @param annee L'année des dossiers à récupérer
     * @param type  Le type de dossier à récupérer
     * @return La liste des dossiers
     */
    private List<Dossier> getAnneeTypeWithInexistants(String annee, String type) {
        List<Dossier> dossiers = new ArrayList<>();

        if (this.dossiers.get(annee) != null) {
            for (Dossier dossier : this.dossiers.get(annee)) {
                if (dossier.getType().equals(type))
                    dossiers.add(dossier);
            }
        }

        dossiers.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
        return dossiers;
    }

    /**
     * Cette fonction permet de récupérer une liste de dossiers taxés correspondant à une année précise et à un type de dossier précis
     * @param annee L'année des dossiers à récupérer
     * @param type Le type de dossier à récupérer
     * @return La liste des dossiers
     */
    public List<DossierTaxe> getAnneeTypeTaxes(String annee, String type) {
        List<DossierTaxe> dossiers = new ArrayList<>();

        if (this.dossiersTaxes.get(annee) != null) {
            for (DossierTaxe dossier : this.dossiersTaxes.get(annee)) {
                if (dossier.getType().equals(type))
                    dossiers.add(dossier);
            }
        }

        dossiers.sort(Comparator.comparing(DossierTaxe::getNumeroDossier));
        return dossiers;
    }

    /**
     * Cette fonction permet de récupérer une liste de dossiers taxés correspondant à une année précise et à un type de dossier précis
     * @param annee L'année des dossiers à récupérer
     * @param typeTaxation Le type de taxation des dossiers à récupérer
     * @return Le montant total
     */
    public Integer getMontantTotal(String annee, String typeTaxation) {
        Integer montant = 0;

        if (this.dossiersTaxes.get(annee) != null) {
            for (DossierTaxe dossierTaxe : this.dossiersTaxes.get(annee)) {
                if (dossierTaxe.getTypeEIT().equals("2"))
                    continue;
                if (dossierTaxe.getTypeTaxation().contains(typeTaxation))
                    montant += dossierTaxe.getMontant();
            }
        }

        return montant;
    }

    /**
     * Cette fonction permet de récupérer une liste de dossiers taxés correspondant à une année précise et à un type de dossier précis
     * mais en ne tenant pas compte de la RAP
     * @param annee L'année des dossiers à récupérer
     * @param typeTA Le type de TA des dossiers à récupérer
     * @return Le montant total
     */
    public Integer getMontantTotalTA(String annee, String typeTA) {
        Integer montant = 0;

        if (this.dossiersTaxes.get(annee) != null) {
            for (DossierTaxe dossierTaxe : this.dossiersTaxes.get(annee)) {
                if (dossierTaxe.getTypeEIT().equals("2"))
                    continue;
                if (dossierTaxe.getTypeTA().contains(typeTA))
                    montant += dossierTaxe.getMontant();
            }
        }

        return montant;
    }

    /**
     * Cette fonction permet de rechercher les dossiers manquants propre à une année et à un type de dossiers
     * @param annee l'année que l'on veut inspecter
     * @param type Le type de dossier à inspecter
     * @return La liste des dossiers manquants pour cette année là et ce type de dossier là
     */
    public List<Dossier> rechercherManquantsAnnee(String annee, String type) {
        List<Dossier> dossiers = getAnneeTypeWithInexistants(annee, type); // On récupère les dossiers enregistrés
        List<Dossier> removable = new ArrayList<>();
        for (Dossier dossier : dossiers) {
            if (!dossier.getAnnee().equals(annee))
                removable.add(dossier);
        }
        dossiers.removeAll(removable);
        dossiers.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
        List<Dossier> manquants = new ArrayList<>(); // On initialise la liste des dossiers manquants
        int index = 0;

        // On regarde si une exception existe pour ce type de dossiers
        int debut = 0;
        NumberException exception = getException(type);
        if (exception != null)
            debut = Integer.valueOf(exception.getNumber());

        /*
        On parcours la totalité des dossiers enregistrés
         */
        for (Dossier dossier : dossiers) {
            if (!dossier.getAnnee().equals(annee))
                continue;
            /*
            Si c'est le premier dossier on va comparer son numéro de dossier avec 1
             */
            if (dossier.getNumeroDossier().contains("9999"))
                continue;
            if (index == 0) {
                if (Integer.valueOf(dossier.getNumeroDossierRapide()) > 1) {
                    int nbManquants = Integer.valueOf(dossier.getNumeroDossierRapide()) - 1;
                    // Si une exception existe on modifie le début des dossiers
                    if (debut != 0)
                        nbManquants = Integer.valueOf(dossier.getNumeroDossierRapide()) - debut;
                    while (nbManquants > 0) { // On ajoute à la liste des manquants tous les dossiers manquants un à un
                        int numeroDossier = Integer.valueOf(dossier.getNumeroDossierRapide()) - nbManquants;
                        if (numeroDossier < 10)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "000" + numeroDossier, "",""));
                        else if (numeroDossier < 100)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "00" + numeroDossier, "", ""));
                        else if (numeroDossier < 1000)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "0" + numeroDossier, "",""));
                        else
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + numeroDossier, "",""));
                        nbManquants--;
                    }
                }
            }
            /*
            Sinon on compare avec le numéro du dossier précédent pour savoir le nombre de dossiers manquants
             */
            else {
                /*
                On vérifie ici si le numéro du dossier et le numéro du précédent sont différents de +1
                 */
                if (Integer.valueOf(dossier.getNumeroDossierRapide()) - 1 > Integer.valueOf(dossiers.get(index - 1).getNumeroDossierRapide())) {
                    int nbManquants = Integer.valueOf(dossier.getNumeroDossierRapide()) - Integer.valueOf(dossiers.get(index - 1).getNumeroDossierRapide()) - 1;
                    while (nbManquants > 0) { // On ajoute à la liste des manquants tous les dossiers manquants un à un
                        int numeroDossier = Integer.valueOf(dossier.getNumeroDossierRapide()) - nbManquants;
                        if (numeroDossier < 10)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "000" + numeroDossier, "",""));
                        else if (numeroDossier < 100)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "00" + numeroDossier, "", ""));
                        else if (numeroDossier < 1000)
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + "0" + numeroDossier, "",""));
                        else
                            manquants.add(new Dossier(dossier.getNumeroDossier().substring(0, 15) + numeroDossier, "",""));
                        nbManquants--;
                    }
                }
                /*
                Sinon s'ils sont égaux on vérifie qu'il n'y a pas eu changement de centre instructeur (lettre du dossier)
                 */
                /*else if (Integer.valueOf(dossier.getNumeroDossierRapide()).equals(Integer.valueOf(dossiers.get(index - 1).getNumeroDossierRapide()))){
                    if (dossier.getNumeroDossier().charAt(14) != dossiers.get(index - 1).getNumeroDossier().charAt(14)) {
                        int nbManquants = Integer.valueOf(dossier.getNumeroDossierRapide()) - 1;
                        while (nbManquants > 0) { // On ajoute à la liste des manquants tous les dossiers manquants un à un
                            int numeroDossier = Integer.valueOf(dossier.getNumeroDossier().substring(16,19)) - nbManquants;
                            if (numeroDossier < 10)
                                manquants.add(new Dossier(dossier.getNumeroDossier().substring(0,16) + "00" + numeroDossier, "",""));
                            else if (numeroDossier < 100)
                                manquants.add(new Dossier(dossier.getNumeroDossier().substring(0,16) + "0" + numeroDossier, "",""));
                            else
                                manquants.add(new Dossier(dossier.getNumeroDossier().substring(0,16) + numeroDossier, "",""));
                            nbManquants--;
                        }
                    }
                }*/
            }
            index++;
        }

        manquants.sort(Comparator.comparing(Dossier::getNumeroDossierRapide));
        return manquants;
    }

    /**
     * Cette fonction retourne le département de la commune
     * @return Le département de la commune
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Cette fonction permet de changer le département de la commune
     * @param department Le nouveau département de la commune
     */
    private void setDepartment(String department) {
        this.department = department;
    }

    /**
     * Cette fonction retourne l'intégralité des dossiers taxés sur toutes les années enregistrés
     * @return La map contenant tous les dossiers taxés par année
     */
    public Map<String, List<DossierTaxe>> getDossiersTaxes() {
        triDossiersTaxes();
        return dossiersTaxes;
    }

    /**
     * Cette fonction permet de modifier la liste complète des dossiers taxés
     * @param dossiersTaxes La nouvelle map changée
     */
    private void setDossiersTaxes(Map<String, List<DossierTaxe>> dossiersTaxes) {
        this.dossiersTaxes = dossiersTaxes;
    }

    /**
     * Fonction permettant d'ajouter une exception à la commune
     * @param exception L'exception à ajouter
     */
    public void addException(NumberException exception) {
        if (exceptions == null)
            exceptions = new ArrayList<>();
        exceptions.add(exception);
    }

    public List<NumberException> getExceptions() {
        return exceptions;
    }

    public Boolean getEpci() {
        return epci;
    }

    public NumberException exceptionExists(NumberException exception) {
        if (exceptions == null)
            return null;
        for (NumberException numberException : exceptions) {
            if (numberException.getType().equals(exception.getType()))
                return numberException;
        }
        return null;
    }

    public NumberException getException(String type) {
        for (NumberException numberException : exceptions) {
            if (numberException.getType().equals(type))
                return numberException;
        }
        return null;
    }
}

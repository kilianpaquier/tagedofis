package com.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

/**
 * @author Kilian Paquier
 * Classe de la DDT
 */
public class DDT {
    private List<Commune> communes = new ArrayList<>(); // La liste des communes
    private String entete = "DDT / SHUT / UFU\n(ADS 2007 ou autres)\n";
    private String titreApp = "T.A.GEDOFIS ";
    private Map<String, LocalDate> mapImportExport = new HashMap<>();
    private String version = "V 1.8.4";
    private StringBuilder logs = new StringBuilder();

    /**
     * Constructeur par défaut d'une DDT
     */
    public DDT() {

    }

    /**
     * On retourne la liste des communes
     * @return La liste des communes
     */
    public List<Commune> getCommunes() {
        return communes;
    }

    /**
     * On ajouter une toute nouvelle liste des communes (Utilisée pour la sérialisation)
     * @param communes La nouvelle liste des communes
     */
    private void setCommunes(List<Commune> communes) {
        this.communes = communes;
    }

    /**
     * Permet d'ajouter une commune à la liste des communes
     * @param commune La commune à ajouter
     */
    public void ajouterCommune(Commune commune) {
        Commune commune1 = rechercherCommuneINSEE(commune.getNumeroINSEE(), commune.getDepartment());
        if (commune1 == null)
            communes.add(commune);
        else if (commune1 != null && !commune.getNomCommune().equals(commune1.getNomCommune())) {
            supprimerCommune(commune1.getNumeroINSEE(), commune1.getDepartment());
            communes.add(commune);
        }
    }

    /**
     * Cette fonction tri les communes pas numéro INSEE
     */
    public void triCommunes() {
        communes.sort(Comparator.comparing(Commune::getNumeroINSEE));
    }

    /**
     * Cette fonction permet de supprimer une commune de la liste des communes
     * @param numeroINSEE Le numéro INSEE de la commune
     * @param departement Le département de la commune
     */
    public void supprimerCommune(String numeroINSEE, String departement) {
        Commune commune = rechercherCommuneINSEE(numeroINSEE, departement);
        Map<String, List<Dossier>> dossiers = commune.getDossiers();
        for (Map.Entry annee : dossiers.entrySet()) {
            dossiers.get(annee.getKey().toString()).clear();
        }
        communes.remove(commune);
        logs.append("Commune : ").append(commune.getNomCommune()).append(" supprimée \n");
    }

    /**
     * On recherche une commune à l'aide de son numéro INSEE et de son département et on la retourne si elle existe
     * @param numeroINSEE Le numéro INSEE de la commune à réchercher
     * @param department Le département de la commune
     * @return La commune si elle existe
     */
    public Commune rechercherCommuneINSEE(String numeroINSEE, String department) {
        for (Commune commune : communes) {
            if (numeroINSEE.trim().equals(commune.getNumeroINSEE()) && commune.getDepartment().equals(department))
                return commune;
        }
        return null;
    }

    /**
     * Cette fonction recherche et retourne un dossier si il existe en fonction de son numéro de dossier
     * @param numeroDossier Le numéro de dossier du dossier à rechercher
     * @return Le dossier s'il existe
     * @throws IOException Si la commune n'existe pas
     */
    public Dossier rechercherDossier(String numeroDossier) throws IOException {
        numeroDossier = numeroDossier.toUpperCase();
        String numeroINSEE = numeroDossier.substring(5,8);
        String department = numeroDossier.substring(2,5);

        Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
        if (commune == null)
            throw new IOException("Commune inexistante");

        if (commune.getDossiers() == null)
            throw new IOException("Le dossier n'existe pas");

        for (List<Dossier> dossiers : commune.getDossiers().values()) {
            for (Dossier dossier : dossiers) {
                if (dossier.getNumeroDossier().replaceAll(" ", "").equals(numeroDossier))
                    return dossier;
            }
        }
        return null;
    }

    /**
     * Cette fonction permet de supprimer un dossier parmis les communes
     * @param numeroDossier Le numéro du dossier à supprimer
     * @throws IOException Si la commune ou le dossier n'existe pas
     */
    public void supprimerDossier(String numeroDossier) throws IOException {
        numeroDossier = numeroDossier.toUpperCase();
        String numeroINSEE = numeroDossier.substring(5,8);
        String department = numeroDossier.substring(2,5);

        Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
        if (commune == null)
            throw new IOException("Commune inexistante");
        if (rechercherDossier(numeroDossier) == null)
            throw new IOException("Le dossier n'existe pas");

        for (List<Dossier> dossiers : commune.getDossiers().values()) {
            for (Dossier dossier : dossiers) {
                if (dossier.getNumeroDossier().replaceAll(" ", "").equals(numeroDossier)) {
                    dossiers.remove(dossier);
                    logs.append("Dossier ").append(numeroDossier).append(" supprimé \n");
                    break;
                }
            }
        }
    }

    /**
     * Cette fonction permet de supprimer une liste de dossiers parmis toutes les communes selon une année saisie
     * @param annee L'année à supprimer parmis toutes les communes
     */
    public void supprimerListeDossiers(String annee) {
        String year = annee.substring(2,4);
        for (Commune commune : communes) {
            Map<String, List<Dossier>> dossiers = commune.getDossiers();
            if (dossiers.get(year) != null) {
                dossiers.remove(year);
                logs.append("Année de dossiers ").append(annee).append(" supprimée \n");
            }
        }
    }

    /**
     * Cette fonction permet de supprimer une année entière d'information sur chaque dossiers taxé pour chaque commune
     * @param annee L'année à supprimer
     */
    public void supprimerListeInfo(String annee) {
        String year = annee.substring(2,4);
        for (Commune commune : communes) {
            Map<String, List<DossierTaxe>> dossiers = commune.getDossiersTaxes();
            if (dossiers.get(year) != null) {
                dossiers.remove(year);
                logs.append("Année de montants ").append(annee).append(" supprimée \n");
            }
        }
    }

    /**
     * Cette fonction parse une liste de communes à l'aide d'un fichier contenant les numéros INSEE et les noms des communes
     * @param file Le fichier contenant les communes
     * @throws IOException Si le fichier n'existe pas ou ne peut pas être ouvert
     */
    public void parseCommunes(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));
        String ligne;
        int nbLigne = 0;
        while ((ligne = reader.readLine()) != null) {
            nbLigne++;
            ligne = ligne.replaceAll("\"", "");
            String numeroINSEE = ligne.split(",")[0].trim().substring(3);
            String nomCommune = ligne.split(",")[1].trim();
            String department;
            if (ligne.split(",")[0].length() == 6)
                department = ligne.split(",")[0].trim().substring(0,3);
            else
                throw new IOException("Numéro de département impossible");
            ajouterCommune(new Commune(numeroINSEE, nomCommune, department));
            logs.append("Commune ").append(nomCommune).append(" ajoutée à la liste des communes - Ligne fichier : ").append(nbLigne).append("\n");
        }

        reader.close();
    }

    /**
     * Cette fonction parse une liste de dossiers à l'aide d'un fichier contenant les dossiers à récupérer
     * @param file Le fichier contenant les dossiers
     * @param annee l'année où les dossiers ont été enregistrés
     * @throws IOException Si le fichier ne peut pas être ouvert ou n'existe pas
     */
    public void parseDossiers(String file, String annee) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));
        String ligne;
        int index = 0;

        /*
        Pour toutes les lignes du fichier
         */
        while ((ligne = reader.readLine()) != null) {
            /*
            On ignore la ligne des entêtes de colonnes
             */
            if (index == 0) {
                index++;
                continue;
            }

            ligne = ligne.replaceAll("\"", ""); // On retire tous les "" en trop
            /*
            On vérifie si le fichier CSV a été tabulé selon les ;
             */
            if (ligne.split(",").length <= 1) {
                String[] dossier = ligne.split(";");
                String numeroDossier = dossier[0].trim();
                String nomDemandeur = dossier[1].trim();
                String etatDossier;
                if (dossier.length < 3)
                    etatDossier = "";
                else
                    etatDossier = dossier[2].trim();
                String numeroINSEE = dossier[0].trim().substring(7,10);
                String department = dossier[0].trim().substring(3,6);

                if (numeroDossier.contains("9999"))
                    continue;

                Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
                if (commune != null) {
                    commune.addDossier(new Dossier(numeroDossier, nomDemandeur, etatDossier), annee);
                    logs.append("Dossier ").append(numeroDossier).append(" ajouté à la liste des dossiers - Ligne fichier : ").append(index).append("\n");
                }
            }
            /*
            Si il a été tabulé selon les ,
             */
            else {
                String[] dossier = ligne.split(",");
                String numeroDossier = dossier[0].trim();
                String nomDemandeur = dossier[1].trim();
                String etatDossier;
                if (dossier.length < 3)
                    etatDossier = "";
                else
                    etatDossier = dossier[2].trim();
                String numeroINSEE = dossier[0].trim().substring(7,10);
                String department = dossier[0].trim().substring(3,6);

                if (numeroDossier.contains("9999"))
                    continue;

                Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
                if (commune != null) {
                    commune.addDossier(new Dossier(numeroDossier, nomDemandeur, etatDossier), annee);
                    logs.append("Dossier ").append(numeroDossier).append(" ajouté à la liste des dossiers - Ligne fichier : ").append(index).append("\n");
                }
            }
            index++;
        }
        reader.close();
    }

    /**
     * Cette fonction permet d'importer une liste d'informations sur des dossiers taxés
     * @param file Le fichier importé
     * @throws IOException Si un problème a lieu sur le fichier CSV
     */
    public void parseDossiersTaxes(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));
        String ligne;
        int index = 0;

        /*
        Pour toutes les lignes du fichier
         */
        while ((ligne = reader.readLine()) != null) {
            /*
            On ignore la ligne des entêtes de colonnes
             */
            if (index == 0) {
                index++;
                continue;
            }
            /*
            On vérifie si le fichier CSV a été tabulé selon les ;
             */
            if (ligne.split(",").length < 5) {
                ligne = ligne.replaceAll("\"", ""); // On retire tous les "" en trop
                String[] dossier = ligne.split(";");

                String numeroDossier = dossier[3].trim();
                String dateVerification = dossier[2].trim();
                LocalDate dateVerif = LocalDate.of(Integer.valueOf(dateVerification.split("/")[2]), Integer.valueOf(dateVerification.split("/")[1]), Integer.valueOf(dateVerification.split("/")[0]));
                String dateDecision = dossier[1].trim();
                LocalDate dateDec = null;
                if (!dateDecision.equals(""))
                    dateDec = LocalDate.of(Integer.valueOf(dateDecision.split("/")[2]), Integer.valueOf(dateDecision.split("/")[1]), Integer.valueOf(dateDecision.split("/")[0]));
                String typeTA = dossier[4].trim();
                String typeEIT = dossier[5].trim();
                Integer montant = Integer.valueOf(dossier[8].trim().replaceAll(" ", ""));
                String typeTaxation = dossier[7].trim();

                String dateEcheance = dossier[9].trim();
                LocalDate dateEche = LocalDate.of(Integer.valueOf(dateEcheance.split("/")[2]), Integer.valueOf(dateEcheance.split("/")[1]), Integer.valueOf(dateEcheance.split("/")[0]));

                String numeroINSEE = dossier[3].trim().substring(7,10);
                String department = dossier[3].trim().substring(3,6);

                /*if (typeTaxation.contains("AN"))
                    continue;
                if (typeTaxation.contains("TR") && !numeroDossier.contains("-T"))
                    continue;*/

                Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
                if (commune != null) {
                    DossierTaxe dossierTaxe = new DossierTaxe(numeroDossier, typeTA, montant, typeEIT, typeTaxation);
                    dossierTaxe.setDateVerification(dateVerif);
                    if (!dateDecision.equals(""))
                        dossierTaxe.setDateDecision(dateDec);
                    dossierTaxe.setDateEcheance(dateEche);
                    commune.addDossierTaxe(dossierTaxe);

                    logs.append("Montant ").append(numeroDossier).append(" ajouté à la liste des montants - Ligne fichier : ").append(index).append("\n");
                }
            }
            /*
            Si il a été tabulé selon les ,
             */
            else {
                String[] dossier = ligne.split("\",\"");
                for (String colonne : dossier)
                    colonne = colonne.replaceAll("\"", "");

                String numeroDossier = dossier[3].trim();
                String dateVerification = dossier[2].trim();
                LocalDate dateVerif = LocalDate.of(Integer.valueOf(dateVerification.split("/")[2]), Integer.valueOf(dateVerification.split("/")[1]), Integer.valueOf(dateVerification.split("/")[0]));
                String dateDecision = dossier[1].trim();
                LocalDate dateDec = null;
                if (!dateDecision.equals(""))
                    dateDec = LocalDate.of(Integer.valueOf(dateDecision.split("/")[2]), Integer.valueOf(dateDecision.split("/")[1]), Integer.valueOf(dateDecision.split("/")[0]));
                String typeTA = dossier[4].trim();
                String typeEIT = dossier[5].trim();
                Integer montant = Integer.valueOf(dossier[8].trim().replaceAll(" ", ""));
                String typeTaxation = dossier[7].trim();

                String dateEcheance = dossier[9].trim();
                LocalDate dateEche = LocalDate.of(Integer.valueOf(dateEcheance.split("/")[2]), Integer.valueOf(dateEcheance.split("/")[1]), Integer.valueOf(dateEcheance.split("/")[0]));

                String numeroINSEE = dossier[3].trim().substring(7,10);
                String department = dossier[3].trim().substring(3,6);

                if (typeTaxation.contains("AN"))
                    continue;
                if (typeTaxation.contains("TR") && !numeroDossier.contains("-T"))
                    continue;

                Commune commune = rechercherCommuneINSEE(numeroINSEE, department);
                if (commune != null) {
                    DossierTaxe dossierTaxe = new DossierTaxe(numeroDossier, typeTA, montant, typeEIT, typeTaxation);
                    dossierTaxe.setDateVerification(dateVerif);
                    if (!dateDecision.equals(""))
                        dossierTaxe.setDateDecision(dateDec);
                    dossierTaxe.setDateEcheance(dateEche);
                    commune.addDossierTaxe(dossierTaxe);

                    logs.append("Montant ").append(numeroDossier).append(" ajouté à la liste des Montants - Ligne fichier : ").append(index).append("\n");
                }
            }
            index++;
        }

        reader.close();
    }

    /**
     * Cette fonction permet de supprimer l'intégralité des données
     */
    public void supprimerDonnees() {
        communes.clear();
        mapImportExport.clear();
        titreApp = "T.A.GEDOFIS ";
        entete = "DDT / SHUT / UFU\n(ADS 2007 ou autres)\n";
        logs.append("Suppression des données réalisée \n");
    }

    /**
     * Cette fonction désérialise la liste des communes
     * @throws IOException Si le fichier n'existe pas ou ne peut pas être ouvert
     */
    public void deserializeCommunes() throws IOException {
        /*FileInputStream fileInputStream = new FileInputStream("dataTAGEDOFIS.ser");

        ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);

        setCommunes((ArrayList<Commune>) inputStream.readObject());
        setEntete((String) inputStream.readObject());
        setMapImportExport((Map<String, LocalDate>) inputStream.readObject());
        setTitreApp((String) inputStream.readObject());

        fileInputStream.close();
        inputStream.close();*/

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream("dataTAGEDOFIS.json"), StandardCharsets.UTF_8));

        DDT ddt = gson.fromJson(reader, DDT.class);
        setMapImportExport(ddt.mapImportExport);
        setEntete(ddt.entete);
        setTitreApp(ddt.titreApp);
        setCommunes(ddt.communes);
    }

    /**
     * Cette fonction retourne l'entête écrite dans chaque fichier exporté de l'application
     * @return L'entête d'exportation
     */
    public String getEntete() {
        return entete;
    }

    /**
     * Cette fonction change l'entête d'exportation
     * @param entete La nouvelle entête d'exportation
     */
    public void setEntete(String entete) {
        this.entete = entete;
    }

    /**
     * Cette fonction retourne la map contenant les dates d'importations des données
     * @return La map contenant les dates d'importations des données
     */
    public Map<String, LocalDate> getMapImportExport() {
        return mapImportExport;
    }

    /**
     * Cette fonction modifie la map des dates d'importations des données avec une nouvelle map
     * @param mapImportExport La nouvelle map des dates
     */
    private void setMapImportExport(Map<String, LocalDate> mapImportExport) {
        this.mapImportExport = mapImportExport;
    }

    /**
     * cette fonction retourne le titre de l'application
     * @return Le titre de l'application
     */
    public String getTitreApp() {
        return titreApp;
    }

    /**
     * Cette fonction change le titre de l'application
     * @param titreApp Le nouveau titre de l'application
     */
    public void setTitreApp(String titreApp) {
        this.titreApp = titreApp;
    }

    /**
     * Cette fonction permet de porter un modèle de JTable vers un ou plusieurs fichier(s) xls ou csv
     * @param file le nom du dossier où sera/seront créé(s) le(s) fichier(s) résultat(s)
     * @param extensionFichier Afin de savoir si on enregistre en csv ou en xls
     * @param multiFiles Le booléen pour savoir si on enregistre dans plusieurs fichiers ou non
     * @param defaultTableModel Le modèle de la table à écrire dans le ou les fichiers csv xls
     * @param enregistres Pour savoir si on travaille sur un modèle de dossiers enregistrés ou manquants
     * @throws IOException Si le fichier n'existe pas ou qu'un autre problème a lieu sur celui-ci
     */
    public void exporterListeDossier(String file, String extensionFichier, Boolean multiFiles, ModelTable defaultTableModel, Boolean enregistres) throws IOException {
        /*
        Déclaration des variables essentielles
         */
        String annee = null;
        StringBuilder fileName = new StringBuilder(file + "\\");
        String commune = null;
        String EPCI = "EPCI";
        boolean isEPCI = true;
        String dpt = null;
        List<String> years = new ArrayList<>();
        boolean multiCommune = false;

        /*
        Si on ne veut pas enregistrer dans plusieurs fichiers
         */
        if (!multiFiles) {
            String colonnes;
            if (enregistres)
                colonnes = "Année de dépôt,Numéro du dossier,Commune du dossier,Nom du demandeur,Etat du dossier\n";
            else
                colonnes = "Année de dépôt,Numéro du dossier,Commune du dossier,Nom du demandeur,Descriptif,Type de décision*,Date de décision,,*Accord,Refus,Non instruit\n";
            Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
            writer.write(entete.split("\n")[0] + "\n");
            if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                writer.write(entete.split("\n")[1] + "\n");
            }
            writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
            writer.write(colonnes); // On écrit les colonnes

            int index = 0;

            /*
            On parcours la totalité des lignes du modèle
             */
            for (Vector vector : defaultTableModel.getDataVector()) {
                // On regarde la première ligne
                if (index == 0) {
                    dpt = vector.get(2).toString().substring(0,3); // On récupère le département
                    annee = Year.now().toString().substring(0,2) + vector.get(1).toString(); // On récupère l'année en cours
                    writer.write(annee + ",");
                    /*
                    On vérifie si on travaille sur les enregistrés ou non
                     */
                    if (enregistres) {
                        if (!EPCI.equals(vector.get(5).toString())) // On regarde si on travaille sur les communes epci
                            isEPCI = false;
                    }
                    else {
                        if (!EPCI.equals(vector.get(3).toString())) // On regarde si on travaille sur les communes epci
                            isEPCI = false;
                    }
                    commune = vector.get(2).toString().split(" ")[1]; // On récupère la commune courante
                    years.add(annee);
                    index++;
                }
                // On regarde les autres lignes
                else {
                    /*
                    On compare l'année à l'année précédente
                     */
                    if (!annee.equals(Year.now().toString().substring(0,2) + vector.get(1).toString())) {
                        annee = Year.now().toString().substring(0,2) + vector.get(1).toString();
                        if (!years.contains(annee)) // On regarde si l'année en cours n'est pas déjà présente parmis les années parcourues
                            years.add(annee);
                        writer.write(annee + ",");
                    }
                    else
                        writer.write(",");
                    /*
                    On regarde si on a pas changé de commune
                     */
                    if (!commune.equals(vector.get(2).toString().split(" ")[1])) // On regarde si on a changé de commune
                        multiCommune = true;
                    /*
                    On regarde à nouveau si on travaille sur les communes ECPI
                     */
                    if (enregistres) {
                        if (!EPCI.equals(vector.get(5).toString())) // On regarde l'ECPI dans la bonne colonne du modèle de la JTable
                            isEPCI = false;
                    }
                    else {
                        if (!EPCI.equals(vector.get(3).toString())) // On regarde l'ECPI dans la bonne colonne du modèle de la JTable
                            isEPCI = false;
                    }
                }

                /*
                On écrit la ligne comme il faut dan sle fichier
                 */
                writer.write(vector.get(0) + ",");
                writer.write(vector.get(2) + ",");
                if (enregistres) {
                    writer.write(vector.get(3).toString().equals("") ? " " : vector.get(3) + ",");
                    writer.write(vector.get(4) + "\n");
                }
                else {
                    writer.write("\n");
                }
            }

            writer.close(); // On ferme le writer

            /*
            On regarde si on travaille sur un modèle de dossiers enregistrés ou manquants
             */
            if (enregistres) {
                if (!multiCommune)
                    fileName.append(dpt).append(commune).append("_Enregistrés"); // Ici on a travaillé sur une seule commune
                else if (!isEPCI)
                    fileName.append("Toutes_Communes_Enregistrés").append(dpt); // Sur toutes les communes
                else
                    fileName.append("Communes_EPCI_Enregistrés").append(dpt); // Sur les communes ECPI
            }
            /*
            Cas des manquants
             */
            else {
                if (!multiCommune)
                    fileName.append(dpt).append(commune).append("_Manquants"); // Une commune sur les manquants
                else if (isEPCI)
                    fileName.append("Communes_EPCI_Manquants").append(dpt); // Les communes ECPI
                else
                    fileName.append("Toutes_Communes_Manquants").append(dpt); // Toutes les communes
            }

            Collections.sort(years); // On tri les années
                /*
                On ajoute les années au nom du fichier
                 */
            for (String year : years)
                fileName.append("_").append(year);
            fileName.append(".csv");
            new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
            new File(file + "\\file.csv").delete(); // On supprime le fichier temp

                /*
                On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
                 */
            if (extensionFichier.contains("xls")) {
                Formatage.formatageXLS(new File(fileName.toString()));
                new File(fileName.toString()).delete();
            }
        }
        /*
        Ici on travaille sur l'enregistrement dans plusieurs fichiers
         */
        else {
            String colonnes;
            if (enregistres)
                colonnes = "Année du dossier,Numéro du dossier,Nom du demandeur,Etat du dossier\n";
            else
                colonnes = "Année du dossier,Numéro du dossier,Nom du demandeur,Descriptif,Type de décision*,Date de décision, *Accord,Refus,Non instruit\n";

            Writer writer = null;
            int index = 0;
            /*
            On parcours toutes les lignes du modèle
             */
            for (Vector vector : defaultTableModel.getDataVector()) {
                StringBuilder ligne = new StringBuilder();
                /*
                On regarde la première ligne
                 */
                if (index == 0) {
                    writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
                    writer.write(entete.split("\n")[0] + "\n");
                    if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                        writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                        writer.write(entete.split("\n")[1] + "\n");
                    }
                    writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
                    writer.write(colonnes);

                    dpt = vector.get(2).toString().substring(0,3); // On récupère le département
                    annee = Year.now().toString().substring(0,2) + vector.get(1).toString(); // On récupère l'année de la ligne
                    ligne.append(annee).append(",");
                    commune = vector.get(2).toString().split(" ")[1]; // On récupère la commune courante
                    years.add(annee);
                    index++;
                }
                /*
                On regarde les autres lignes
                 */
                else {
                    /*
                    On regarde si on a changé de commune
                     */
                    if (!commune.equals(vector.get(2).toString().split(" ")[1])) {
                        writer.close();
                        if (enregistres)
                            fileName.append(dpt).append(commune).append("_Enregistrés"); // Si on travaille sur les enregistrés on
                            // ferme le fichier de la commune précédente
                        else
                            fileName.append(dpt).append(commune).append("_Manquants"); // Si on travaille sur les manquants on ferme le fichier
                        // de la commune précédente
                        for (String year : years)
                            fileName.append("_").append(year); // On ajoute les années au nom du fichier
                        fileName.append(".csv");
                        new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
                        new File(file + "\\file.csv").delete(); // On supprime le fichier temp

                        /*
                        On regarde si on veut enregistrer en xls ou non
                         */
                        if (extensionFichier.contains("xls")) {
                            Formatage.formatageXLS(new File(fileName.toString()));
                            new File(fileName.toString()).delete();
                        }

                        /*
                        On remet les variables à 0 pour la nouvelle commune lu
                         */
                        fileName = new StringBuilder(file + "\\"); // Le nom du fichier
                        writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
                        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
                        writer.write(entete.split("\n")[0] + "\n");
                        if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                            writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                            writer.write(entete.split("\n")[1] + "\n");
                        }
                        writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
                        writer.write(colonnes); // On écrit les colonnes
                        years.clear(); // On clear la liste des années
                        annee = Year.now().toString().substring(0,2) + vector.get(1).toString();
                        years.add(annee);
                        ligne.append(annee).append(",");

                        commune = vector.get(2).toString().split(" ")[1]; // On récupère la commune (numéro INSEE)
                    }
                    /*
                    On regarde si on a changé d'année pour la commune courante
                     */
                    else if (!annee.equals(Year.now().toString().substring(0,2) + vector.get(1).toString())) {
                        annee = Year.now().toString().substring(0,2) + vector.get(1).toString();
                        if (!years.contains(annee)) // On regarde si l'année est déjà dans la liste des années
                            years.add(annee);
                        ligne.append(annee).append(",");
                    }
                    else
                        ligne.append(",");
                }

                /*
                On écrit différemment dans le fichier en fonction de si on travaille sur des dossiers enregistrés ou manquants
                 */
                if (enregistres)
                    ligne.append(vector.get(0)).append(",").append(vector.get(3).toString().equals("") ? " " : vector.get(3)).append(",").append(vector.get(4)).append("\n");
                else
                    ligne.append(vector.get(0)).append(",").append("\n");
                writer.write(ligne.toString()); // On écrit la ligne
            }

            /*
            On ferme le tout dernier fichier et on le renomme
             */
            Objects.requireNonNull(writer).close();
            Collections.sort(years);
            if (enregistres)
                fileName.append(dpt).append(commune).append("_Enregistrés"); // Si on travaille sur un enregistré
            else
                fileName.append(dpt).append(commune).append("_Manquants"); // Si on travaille sur un manquant
            for (String year : years)
                fileName.append("_").append(year); // On ajoute les années
            fileName.append(".csv");
            new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
            new File(file + "\\file.csv").delete(); // On supprime le fichier temp

            /*
            On regarde si on souhaite enregistrer en xls ou non
             */
            if (extensionFichier.contains("xls")) {
                Formatage.formatageXLS(new File(fileName.toString()));
                new File(fileName.toString()).delete();
            }
        }
    }

    /**
     * Cette fonction permet d'exporter dans un fichier csv ou xls un tableau croisé
     * @param file Le chemin du fichier créé
     * @param extensionFichier L'extension du fichier (csv ou xls)
     * @param multiFiles Si on exporte dans plusieurs fichiers ou non
     * @param defaultTableModel Le modèle du tableau croisé à exporter
     * @param enregistres Si on travaille sur un tableau croisé des enregistrés ou des manquants
     * @throws IOException Si un problème a lieu sur l'un des fichiers d'exportation
     */
    public void exportTableauxCroises(String file, String extensionFichier, Boolean multiFiles, ModelTable defaultTableModel, Boolean enregistres) throws IOException {
        /*
        Déclaration des variables essentielles
         */
        StringBuilder fileName = new StringBuilder(file + "\\"); // Nom du fichier final
        boolean isEPCI = true; // Est-ce qu'on travaille sur une liste de communes EPCI
        String dpt = null; // Départment de la commune
        String numeroINSEE = null; // Numéro INSEE de la commune
        List<String> years = new ArrayList<>(); // Les années traitées
        boolean multiCommune = false; // Si on travaille avec plusieurs communes
        String nomCommune; // Le nom de la commune
        String nbPA; // Le nombre total de PA sur l'année
        String nbPC; // Le nombre total de PC sur l'année
        String nbDP; // Le nombre total de DP sur l'année
        String total; // Le nombre total tout confondu
        StringBuilder ligne = new StringBuilder(); // La ligne que l'on va écrire
        String remarque;

        /*
        Récuparation de toutes les années
         */
        int nbAnnees = 1;
        String forColumns = defaultTableModel.getDataVector().get(0).get(3).toString();
        years.add(forColumns);
        String endColumns;
        while (!(endColumns = defaultTableModel.getDataVector().get(nbAnnees).get(3).toString()).equals(forColumns)) {
            years.add(endColumns);
            nbAnnees++;
        }

        /*
        Création des colonnes d'années
         */
        StringBuilder colonnes = new StringBuilder();
        StringBuilder colonnesNoms = new StringBuilder("Numéro INSEE,Nom de la commune,");
        colonnes.append(",").append(",");
        for (String year : years) {
            colonnes.append(year).append(",").append(",").append(",").append(",").append(",");
            colonnesNoms.append("DP,PC,PA,Total,").append(",");
        }
        colonnes.append("\n");
        colonnesNoms.append("\n");


        /*
        Si on ne veut pas enregistrer dans plusieurs fichiers
         */
        if (!multiFiles) {
            /*
            Ecriture des entêtes de colonnes
             */
            Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
            writer.write(entete.split("\n")[0] + "\n");
            if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                writer.write(entete.split("\n")[1] + "\n");
            }
            writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
            writer.write(colonnes.toString()); // On écrit les colonnes
            writer.write(colonnesNoms.toString());


            /*
            On parcours la totalité des lignes du modèle
             */
            int index = 0;
            for (Vector vector : defaultTableModel.getDataVector()) {
                if (index + 1 == defaultTableModel.getDataVector().size())
                    continue;
                if (vector.get(0) != null && !vector.get(0).equals("---")) {
                    if (!vector.get(1).toString().equals(numeroINSEE) && numeroINSEE != null)
                        multiCommune = true;

                    dpt = vector.get(0).toString(); // On récupère le département
                    numeroINSEE = vector.get(1).toString(); // On récupère le numéro INSEE
                    nomCommune = vector.get(2).toString(); // On récupère le nom de la commune
                    nbPC = vector.get(4).toString(); // On récupère le nombre total de pc
                    nbDP = vector.get(5).toString(); // On récupère le nombre total de dp
                    nbPA = vector.get(6).toString(); // On récupère le nombre total de pa
                    total = vector.get(7).toString(); // On récupère le total tout confondu
                    if (enregistres)
                        remarque = vector.get(8) != null ? vector.get(8).toString().trim() : "";
                    else
                        remarque = "";
                    if (!rechercherCommuneINSEE(numeroINSEE, dpt).isEpci())
                        isEPCI = false;

                    if (index != 0) // On écrit à la ligne si on a atteint une seconde commune
                        writer.write("\n");

                    ligne.append(dpt).append(numeroINSEE).append(",").append(nomCommune).append(",").append(nbPC).append(",").append(nbDP).
                            append(",").append(nbPA).append(",").append(total).append(",").append(remarque).append(","); // On écrit la ligne dans une variable

                }
                else {
                    if (vector.get(0) != null && vector.get(0).equals("---")) {
                        nomCommune = vector.get(2).toString(); // On récupère le nom de la commune
                        nbPC = vector.get(4).toString(); // On récupère le nombre total de pc
                        nbDP = vector.get(5).toString(); // On récupère le nombre total de dp
                        nbPA = vector.get(6).toString(); // On récupère le nombre total de pa
                        total = vector.get(7).toString(); // On récupère le total tout confondu

                        if (index != 0) // On écrit à la ligne si on a atteint une seconde commune
                            writer.write("\n");

                        ligne.append(",").append(nomCommune).append(",").append(nbPC).append(",").append(nbDP).
                                append(",").append(nbPA).append(",").append(total).append(",").append(","); // On écrit la ligne dans une variable
                    }
                    else {
                        nbPC = vector.get(4).toString(); // On récupère le nombre total de pc
                        nbDP = vector.get(5).toString(); // On récupère le nombre total de dp
                        nbPA = vector.get(6).toString(); // On récupère le nombre total de pa
                        total = vector.get(7).toString(); // On récupère le nombre total tout confondu
                        if (enregistres)
                            remarque = vector.get(8) != null ? vector.get(8).toString().trim() : "";
                        else
                            remarque = "";

                        ligne.append(nbPC).append(",").append(nbDP).append(",").append(nbPA).append(",").append(total).append(",").append(remarque).append(","); // On
                        // ajout à la suite de la ligne les autres années
                    }
                }

                writer.write(ligne.toString());
                ligne = new StringBuilder();
                index++;
            }

            writer.close(); // On ferme le writer

            /*
            On regarde si on travaille sur un modèle de dossiers enregistrés ou manquants
             */
            if (enregistres) {
                if (!multiCommune)
                    fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Enregistrés"); // Ici on a travaillé sur une seule commune
                else if (!isEPCI)
                    fileName.append("Toutes_Communes_Tableau_Croisé_Enregistrés").append(dpt); // Sur toutes les communes
                else
                    fileName.append("Communes_EPCI_Tableau_Croisé_Enregistrés").append(dpt); // Sur les communes EPCI
            }
            /*
            Cas des manquants
             */
            else {
                if (!multiCommune)
                    fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Manquants"); // Une commune sur les manquants
                else if (isEPCI)
                    fileName.append("Communes_EPCI_Tableau_Croisé_Manquants").append(dpt); // Les communes EPCI
                else
                    fileName.append("Toutes_Communes_Tableau_Croisé_Manquants").append(dpt); // Toutes les communes
            }

            Collections.sort(years);
            /*
            On ajoute les années au nom du fichier
             */
            for (String year : years)
                fileName.append("_").append(year);
            fileName.append(".csv");
            new File(fileName.toString()).delete();
            new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
            new File(file + "\\file.csv").delete(); // On supprime le fichier précédent

            /*
            On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
             */
            if (extensionFichier.contains("xls")) {
                Formatage.formatageXLS(new File(fileName.toString()));
                new File(fileName.toString()).delete();
            }
        }
        /*
        Ici on travaille sur l'enregistrement dans plusieurs fichiers
         */
        else {
            /*
            Ecriture des entêtes de colonnes
             */
            Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
            writer.write(entete.split("\n")[0] + "\n");
            if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                writer.write(entete.split("\n")[1] + "\n");
            }
            writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
            writer.write(colonnes.toString()); // On écrit les colonnes
            writer.write(colonnesNoms.toString());

            /*
            On parcours la totalité des lignes du modèle
             */
            int index = 0;
            int indexTotal = 0;
            for (Vector vector : defaultTableModel.getDataVector()) {
                if (indexTotal + 1 == defaultTableModel.getDataVector().size())
                    continue;
                if (vector.get(0) != null) {
                    if (!vector.get(1).toString().equals(numeroINSEE) && numeroINSEE != null) {
                        writer.close();
                        if (enregistres)
                            fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Enregistrés"); // Ici on a travaillé sur une seule commune
                        else
                            fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Manquants"); // Une commune sur les manquants

                        Collections.sort(years);
                        /*
                        On ajoute les années au nom du fichier
                         */
                        for (String year : years)
                            fileName.append("_").append(year);
                        fileName.append(".csv");
                        new File(fileName.toString()).delete();
                        new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
                        new File(file + "\\file.csv").delete(); // On supprime le fichier précédent

                        /*
                        On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
                         */
                        if (extensionFichier.contains("xls")) {
                            Formatage.formatageXLS(new File(fileName.toString()));
                            new File(fileName.toString()).delete();
                        }

                        fileName = new StringBuilder(file + "\\");

                        index = 0;
                        writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
                        writer.write(entete.split("\n")[0] + "\n");
                        if (mapImportExport != null && mapImportExport.get("ImportDossier") != null) {
                            writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportDossier").format(dateTimeFormatter) + "\n");
                            writer.write(entete.split("\n")[1] + "\n");
                        }
                        writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
                        writer.write(colonnes.toString()); // On écrit les colonnes
                        writer.write(colonnesNoms.toString());
                    }


                    nomCommune = vector.get(2).toString(); // On récupère le nom de la commune
                    nbPC = vector.get(4).toString(); // On récupère le nombre total de pc
                    nbDP = vector.get(5).toString(); // On récupère le nombre total de dp
                    nbPA = vector.get(6).toString(); // On récupère le nombre total de pa
                    total = vector.get(7).toString(); // On récupère le nombre total tout confondu
                    if (enregistres)
                        remarque = vector.get(8) != null ? vector.get(8).toString().trim() : "";
                    else
                        remarque = "";

                    if (!vector.get(0).equals("---")) {
                        dpt = vector.get(0).toString(); // On récupère le département
                        numeroINSEE = vector.get(1).toString(); // On récupère le numéro INSEE

                        if (index != 0)
                            writer.write("\n");
                        ligne.append(dpt).append(numeroINSEE).append(",").append(nomCommune).append(",").append(nbPC).append(",").append(nbDP).
                                append(",").append(nbPA).append(",").append(total).append(",").append(remarque).append(",");
                    }
                    else {
                        dpt = "Total"; // On récupère le département
                        numeroINSEE = ""; // On récupère le numéro INSEE
                        if (index != 0)
                            writer.write("\n");
                        ligne.append(",").append(nomCommune).append(",").append(nbPC).append(",").append(nbDP).
                                append(",").append(nbPA).append(",").append(total).append(",").append(remarque).append(",");
                    }
                }
                else {
                    nbPC = vector.get(4).toString();
                    nbDP = vector.get(5).toString();
                    nbPA = vector.get(6).toString();
                    total = vector.get(7).toString();
                    if (enregistres)
                        remarque = vector.get(8) != null ? vector.get(8).toString().trim() : "";
                    else
                        remarque = "";

                    ligne.append(nbPC).append(",").append(nbDP).append(",").append(nbPA).append(",").append(total).append(",").append(remarque).append(",");
                }

                writer.write(ligne.toString());
                ligne = new StringBuilder();
                index++;
                indexTotal++;
            }

            writer.close(); // On ferme le writer

            /*
            On regarde si on travaille sur un modèle de dossiers enregistrés ou manquants
             */
            if (enregistres) {
                fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Enregistrés"); // Ici on a travaillé sur une seule commune
            }
            else {
                fileName.append(dpt).append(numeroINSEE).append("_Tableau_Croisé_Manquants"); // Une commune sur les manquants
            }

            Collections.sort(years);
            /*
            On ajoute les années au nom du fichier
             */
            for (String year : years)
                fileName.append("_").append(year);
            fileName.append(".csv");
            new File(fileName.toString()).delete();
            new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
            new File(file + "\\file.csv").delete(); // On supprime le fichier précédent

            /*
            On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
             */
            if (extensionFichier.contains("xls")) {
                Formatage.formatageXLS(new File(fileName.toString()));
                new File(fileName.toString()).delete();
            }
        }
    }

    /**
     * Cette fonction exporte toutes les informations d'un tableau sur des dossiers taxés
     * @param file Le nom du dossier où sera créé le fichier ou les fichiers dans le cas d'une exportation de toute les communes
     * @param extension L'extension du fichier créé
     * @param defaultTableModel Le modèle contenant les informations à exporter
     * @param allCommunes Le booléen disant si on exporte toutes les communes ou non
     * @param epci Si on exporte les montants des communes EPCI seulement
     * @param allIn Si on exporte les montants de toutes les communes dans un fichier
     * @param years Les années à exporter
     * @throws IOException Si une exception est levée sur le fichier exporté
     */
    public void exporterTaxes(String file, String extension, ModelTable defaultTableModel, boolean allCommunes, boolean epci, boolean allIn, List<String> years) throws IOException {
        /*
        Déclaration des variables essentielles
         */
        StringBuilder fileName = new StringBuilder(file + "\\");
        String colonnes = "Numéro de dossier,Commune,Date de décision,Date de vérification,Type de TA,Montant,Type de taxation,Date échéance,Date échéance 2\n";
        Commune commune = null;
        StringBuilder ligne = new StringBuilder();

        if (!allCommunes) {
             /*
            Ecriture des entêtes de colonnes
             */
            Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
            writer.write(entete.split("\n")[0] + "\n");
            if (mapImportExport != null && mapImportExport.get("ImportTaxe") != null) {
                writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportTaxe").format(dateTimeFormatter) + "\n");
                writer.write(entete.split("\n")[1] + "\n");
            }
            writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
            writer.write(colonnes); // On écrit les colonnes

            int index = 0;
            for (Vector vector : defaultTableModel.getDataVector()) {
                if (index == 0) {
                    String departement = vector.get(0).toString().substring(3,6);
                    String numeroINSEE = vector.get(0).toString().substring(7,10);
                    commune = rechercherCommuneINSEE(numeroINSEE, departement);
                    fileName.append(commune.getDepartment()).append("_").append(commune.getNumeroINSEE()).append("_InformationTaxes");
                    index++;
                }

                ligne.append(vector.get(0)).append(",").append(vector.get(1)).append(",").append(vector.get(2)).append(",").append(vector.get(3))
                        .append(",").append(vector.get(4)).append(",").append(vector.get(5)).append(",").append(vector.get(6)).append(",").append(vector.get(7)).append(",").append(vector.get(8) != null ? vector.get(8) : "").append("\n");
                writer.write(ligne.toString());
                ligne = new StringBuilder();
            }

            writer.close();
            if (epci) {
                fileName = new StringBuilder(file + "\\");
                fileName.append("Communes_EPCI_InformationTaxes");
            }
            else if (allIn) {
                fileName = new StringBuilder(file + "\\");
                fileName.append("Toutes_Communes_InformationTaxes");
            }

            for (String annee : years) {
                fileName.append("_").append(annee);
            }


            new File(fileName.toString() + ".csv").delete();
            new File(file + "\\file.csv").renameTo(new File(fileName.toString() + ".csv"));
            new File(file + "\\file.csv").delete();

            if (extension.contains("xls")) {
                Formatage.formatageXLS(new File(fileName.toString() + ".csv"));
                new File(fileName.toString() + ".csv").delete();
            }
        }
        else {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);

            for (Commune commune1 : communes) {
                /*
                Ecriture des entêtes de colonnes
                 */
                Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
                writer.write(entete.split("\n")[0] + "\n");
                if (mapImportExport != null && mapImportExport.get("ImportTaxe") != null) {
                    writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportTaxe").format(dateTimeFormatter) + "\n");
                    writer.write(entete.split("\n")[1] + "\n");
                }
                writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
                writer.write(colonnes); // On écrit les colonnes
                boolean pass = false;

                Map<String, List<DossierTaxe>> dossiers = commune1.getDossiersTaxes();
                fileName.append(commune1.getDepartment()).append("_").append(commune1.getNumeroINSEE()).append("_InformationTaxes");
                for (Map.Entry year : dossiers.entrySet()) {
                    if (!years.contains(year.getKey().toString()))
                        continue;

                    for (DossierTaxe dossierTaxe : dossiers.get(year.getKey().toString())) {
                        pass = true;
                        if (dossierTaxe.getTypeEIT().contains("2"))
                            continue;
                        ligne.append(dossierTaxe.getNumeroDossier()).append(",").append(commune1.getDepartment()).append(" ").append(commune1.getNumeroINSEE()).append(" ").append(commune1.getNomCommune()).append(",").append(dossierTaxe.getDateDecision() != null ? dossierTaxe.getDateDecision().format(dateTimeFormatter) : "").append(",").append(dossierTaxe.getDateVerification().format(dateTimeFormatter)).append(",").append(dossierTaxe.getTypeTA())
                                .append(",").append(dossierTaxe.getMontant()).append(",").append(dossierTaxe.getTypeTaxation()).append(",").append(dossierTaxe.getDateEcheance()).append(",").append(dossierTaxe.getDateEcheance2() != null ? dossierTaxe.getDateEcheance2() : "").append("\n");
                        writer.write(ligne.toString());
                        ligne = new StringBuilder();
                    }
                }

                writer.close();

                if (pass) {
                    for (String annee : years) {
                        fileName.append("_").append(annee);
                    }

                    new File(fileName.toString() + ".csv").delete();
                    new File(file + "\\file.csv").renameTo(new File(fileName.toString() + ".csv"));
                    new File(file + "\\file.csv").delete();

                    if (extension.contains("xls")) {
                        Formatage.formatageXLS(new File(fileName.toString() + ".csv"));
                        new File(fileName.toString() + ".csv").delete();
                    }
                }
                else
                    new File(file + "\\file.csv").delete();

                fileName = new StringBuilder(file + "\\");
            }
        }
    }

    /**
     * Cette fonction exporte un tableau croisé d'informations de montants de TA pour les communes
     * @param file Le nom du dossier où sera créé le fichier ou les fichiers dans le cas d'une exportation de toute les communes
     * @param extension L'extension du fichier créé
     * @param allCommunes Le booléen disant si on exporte toutes les communes ou non
     * @param annees La liste des années à exporter
     * @throws IOException Si une exception est levée sur le fichier exporté
     */
    public void exporterTableauxTA(String file, String extension, boolean allCommunes, List<String> annees) throws IOException {
        /*
        Déclaration des variables essentielles
         */
        StringBuilder fileName = new StringBuilder(file + "\\"); // Nom du fichier final
        StringBuilder ligne = new StringBuilder(); // La ligne que l'on va écrire

        /*
        Création des colonnes d'années
         */
        StringBuilder colonnes = new StringBuilder();
        StringBuilder colonnesNoms = new StringBuilder("Numéro INSEE,Nom de la commune,");
        colonnes.append(",").append(",");
        for (String year : annees) {
            colonnes.append(LocalDate.now().toString(), 0, 2).append(year).append(",").append(",").append(",").append(",").append(",").append(",").append(",").append(",");
            colonnesNoms.append("Nombre EIT DP,Nombre EIT PC,Nombre EIT PA,Montant TI,Montant TM,Montant TR,Montant AN,Total Montant,,");
        }
        colonnes.append("\n");
        colonnesNoms.append("\n");

        Map<String, List<Integer>> montants = new HashMap<>();
        List<Integer> zeros = new ArrayList<>();
        for (int i = 0; i < 8; i++)
            zeros.add(0);

        /*
        Ecriture des entêtes de colonnes
         */
        Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
        writer.write(entete.split("\n")[0] + "\n");
        if (mapImportExport != null && mapImportExport.get("ImportTaxe") != null) {
            writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportTaxe").format(dateTimeFormatter) + "\n");
            writer.write(entete.split("\n")[1] + "\n");
        }
        writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
        writer.write(colonnes.toString()); // On écrit les colonnes
        writer.write(colonnesNoms.toString());


        /*
        On parcours toutes les communes
         */
        for (Commune commune : communes) {
            if (!allCommunes && !commune.isEpci())
                continue;

            ligne.append(commune.getDepartment()).append(commune.getNumeroINSEE()).append(",").append(commune.getNomCommune()).append(",");

            for (String annee : annees) {
                montants.putIfAbsent(annee, new ArrayList<>(zeros));

                int nbPA = commune.getAnneeTypeTaxes(annee, "PA").size();
                int total = montants.get(annee).get(0);
                total += nbPA;
                montants.get(annee).set(0, total);
                //
                int nbPC = commune.getAnneeTypeTaxes(annee, "PC").size();
                total = montants.get(annee).get(1);
                total += nbPC;
                montants.get(annee).set(1, total);
                //
                int nbDP = commune.getAnneeTypeTaxes(annee, "DP").size();
                total = montants.get(annee).get(2);
                total += nbDP;
                montants.get(annee).set(2, total);
                //
                int montantTI = commune.getMontantTotal(annee, "TI");
                total = montants.get(annee).get(3);
                total += montantTI;
                montants.get(annee).set(3, total);
                //
                int montantTM = commune.getMontantTotal(annee, "TM");
                total = montants.get(annee).get(4);
                total += montantTM;
                montants.get(annee).set(4, total);
                //
                int montantTR = commune.getMontantTotal(annee, "TR");
                total = montants.get(annee).get(5);
                total += montantTR;
                montants.get(annee).set(5, total);
                //
                int montantAN = commune.getMontantTotal(annee, "AN");
                total = montants.get(annee).get(6);
                total += montantAN;
                montants.get(annee).set(6, total);

                total = montantTI + montantAN + montantTM + montantTR;

                ligne.append(nbDP).append(",").append(nbPC).append(",").append(nbPA).append(",").append(montantTI).append(",")
                        .append(montantTM).append(",").append(montantTR).append(",").append(montantAN).append(",").append(total).append(",").append(",");
            }

            ligne.append("\n");
            writer.write(ligne.toString());
            ligne = new StringBuilder();
        }

        for (String annee : annees) {
            montants.putIfAbsent(annee, new ArrayList<>(zeros));
            int total = montants.get(annee).get(6) + montants.get(annee).get(5) + montants.get(annee).get(4) + montants.get(annee).get(3);
            int totalPA = montants.get(annee).get(0);
            int totalPC = montants.get(annee).get(1);
            int totalDP = montants.get(annee).get(2);
            int totalTI = montants.get(annee).get(3);
            int totalTM = montants.get(annee).get(4);
            int totalTR = montants.get(annee).get(5);
            int totalAN = montants.get(annee).get(6);

            ligne.append("---").append(",").append("Total").append(",").append(totalDP).append(",").append(totalPC)
                .append(",").append(totalPA).append(",").append(totalTI).append(",").append(totalTM).append(",")
                .append(totalTR).append(",").append(totalAN).append(",").append(total).append(",").append(",");
        }
        ligne.append("\n");
        writer.write(ligne.toString());
        ligne = new StringBuilder();

        writer.close(); // On ferme le writer
        Collections.sort(annees);


        if (allCommunes) {
            fileName.append("Toutes_Communes_Tableau_Croisé_InfoTaxe");
        }
        else {
            fileName.append("Groupe_Communes_Tableau_Croisé_InfoTaxe");
        }

        /*
        On ajoute les années au nom du fichier
         */
        for (String year : annees)
            fileName.append("_").append(year);
        fileName.append(".csv");
        new File(fileName.toString()).delete();
        new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
        new File(file + "\\file.csv").delete(); // On supprime le fichier précédent

        /*
        On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
         */
        if (extension.contains("xls")) {
            Formatage.formatageXLS(new File(fileName.toString()));
            new File(fileName.toString()).delete();
        }
    }

    /**
     * Cette fonction exporte un tableau croisé d'informations de montants de TA pour les communes
     * @param file Le nom du dossier où sera créé le fichier ou les fichiers dans le cas d'une exportation de toute les communes
     * @param extension L'extension du fichier créé
     * @param allCommunes Le booléen disant si on exporte toutes les communes ou non
     * @param annees La liste des années à exporter
     * @throws IOException Si une exception est levée sur le fichier exporté
     */
    public void exporterTableauxTA2(String file, String extension, boolean allCommunes, List<String> annees) throws IOException {
        /*
        Déclaration des variables essentielles
         */
        StringBuilder fileName = new StringBuilder(file + "\\"); // Nom du fichier final
        StringBuilder ligne = new StringBuilder(); // La ligne que l'on va écrire

        /*
        Création des colonnes d'années
         */
        StringBuilder colonnes = new StringBuilder();
        StringBuilder colonnesNoms = new StringBuilder("Numéro INSEE,Nom de la commune,");
        colonnes.append(",").append(",");
        for (String year : annees) {
            colonnes.append(LocalDate.now().toString(), 0, 2).append(year).append(",").append(",").append(",").append(",").append(",").append(",").append(",");
            colonnesNoms.append("Nombre EIT DP,Nombre EIT PC,Nombre EIT PA,Montant TA,Montant RAP,Total Montant,,");
        }
        colonnes.append("\n");
        colonnesNoms.append("\n");

        Map<String, List<Integer>> montants = new HashMap<>();
        List<Integer> zeros = new ArrayList<>();
        for (int i = 0; i < 8; i++)
            zeros.add(0);

        /*
        Ecriture des entêtes de colonnes
         */
        Writer writer = new OutputStreamWriter(new FileOutputStream(file + "\\file.csv", false), StandardCharsets.ISO_8859_1);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.FRANCE);
        writer.write(entete.split("\n")[0] + "\n");
        if (mapImportExport != null && mapImportExport.get("ImportTaxe") != null) {
            writer.write("Dernière date d'importation des données :," + mapImportExport.get("ImportTaxe").format(dateTimeFormatter) + "\n");
            writer.write(entete.split("\n")[1] + "\n");
        }
        writer.write("Date d'exportation des données :," + LocalDate.now().format(dateTimeFormatter) + "\n");
        writer.write(colonnes.toString()); // On écrit les colonnes
        writer.write(colonnesNoms.toString());


        /*
        On parcours toutes les communes
         */
        for (Commune commune : communes) {
            if (!allCommunes && !commune.isEpci())
                continue;

            ligne.append(commune.getDepartment()).append(commune.getNumeroINSEE()).append(",").append(commune.getNomCommune()).append(",");

            for (String annee : annees) {
                montants.putIfAbsent(annee, new ArrayList<>(zeros));

                int nbPA = commune.getAnneeTypeTaxes(annee, "PA").size();
                int total = montants.get(annee).get(0);
                total += nbPA;
                montants.get(annee).set(0, total);
                //
                int nbPC = commune.getAnneeTypeTaxes(annee, "PC").size();
                total = montants.get(annee).get(1);
                total += nbPC;
                montants.get(annee).set(1, total);
                //
                int nbDP = commune.getAnneeTypeTaxes(annee, "DP").size();
                total = montants.get(annee).get(2);
                total += nbDP;
                montants.get(annee).set(2, total);
                //
                int montantTI = commune.getMontantTotalTA(annee, "TA");
                total = montants.get(annee).get(3);
                total += montantTI;
                montants.get(annee).set(3, total);
                //
                int montantRAP = commune.getMontantTotalTA(annee, "RAP2012");
                total = montants.get(annee).get(4);
                total += montantRAP;
                montants.get(annee).set(4, total);

                total = montantTI + montantRAP;

                ligne.append(nbDP).append(",").append(nbPC).append(",").append(nbPA).append(",").append(montantTI).append(",")
                        .append(montantRAP).append(",").append(total).append(",").append(",");
            }

            ligne.append("\n");
            writer.write(ligne.toString());
            ligne = new StringBuilder();
        }

        for (String annee : annees) {
            montants.putIfAbsent(annee, new ArrayList<>(zeros));
            int total = montants.get(annee).get(4) + montants.get(annee).get(3);
            int totalPA = montants.get(annee).get(0);
            int totalPC = montants.get(annee).get(1);
            int totalDP = montants.get(annee).get(2);
            int totalTA = montants.get(annee).get(3);
            int totalRAP = montants.get(annee).get(4);

            ligne.append("---").append(",").append("Total").append(",").append(totalDP).append(",").append(totalPC)
                    .append(",").append(totalPA).append(",").append(totalTA).append(",").append(totalRAP).append(",")
                    .append(total).append(",").append(",");
        }
        ligne.append("\n");
        writer.write(ligne.toString());
        ligne = new StringBuilder();

        writer.close(); // On ferme le writer
        Collections.sort(annees);


        if (allCommunes) {
            fileName.append("Toutes_Communes_Tableau_Croisé_InfoTaxe");
        }
        else {
            fileName.append("Groupe_Communes_Tableau_Croisé_InfoTaxe");
        }

        /*
        On ajoute les années au nom du fichier
         */
        for (String year : annees)
            fileName.append("_").append(year);
        fileName.append(".csv");
        new File(fileName.toString()).delete();
        new File(file + "\\file.csv").renameTo(new File(fileName.toString())); // On renomme le fichier
        new File(file + "\\file.csv").delete(); // On supprime le fichier précédent

        /*
        On transforme le fichier csv en xls si on a décidé d'enregistrer en xls
         */
        if (extension.contains("xls")) {
            Formatage.formatageXLS(new File(fileName.toString()));
            new File(fileName.toString()).delete();
        }
    }

    public String getVersion() {
        return version;
    }

    public int nbECPI() {
        int nb = 0;
        for (Commune commune : communes) {
            if (commune.isEpci())
                nb++;
        }
        return nb;
    }

    public void parseException(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1));
        String ligne;
        int index = 0;

        if (communes.size() == 0)
            throw new IOException("Il n'existe pas de communes");

        /*
        Pour toutes les lignes du fichier
         */
        while ((ligne = reader.readLine()) != null) {
            /*
            On ignore la ligne des entêtes de colonnes
             */
            if (index == 0) {
                index++;
                continue;
            }

            ligne = ligne.replaceAll("\"", ""); // On retire tous les "" en trop
            /*
            On vérifie si le fichier CSV a été tabulé selon les ;
             */
            if (ligne.split(",").length <= 1) {
                String[] data = ligne.split(";");
                Commune commune = rechercherCommuneINSEE(data[0].substring(3, 6), data[0].substring(0, 3));
                String type = data[1];
                String debut = data[2];

                NumberException numberException = new NumberException(debut, type);
                if (commune.exceptionExists(numberException) == null)
                    commune.addException(numberException);
            }
            /*
            Si il a été tabulé selon les ,
             */
            else {
                String[] data = ligne.split(",");
                Commune commune = rechercherCommuneINSEE(data[0].substring(3, 6), data[0].substring(0, 3));
                String type = data[1];
                String debut = data[2];

                NumberException numberException = new NumberException(debut, type);
                if (commune.exceptionExists(numberException) == null)
                    commune.addException(numberException);
            }
            index++;
        }
        reader.close();
    }

    public StringBuilder getLogs() {
        return logs;
    }

    public void setLogs(StringBuilder logs) {
        this.logs = logs;
    }
}

package com.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Kilian Paquier
 * Classe Dossier afin de traîté les dossiers enregistrés et manquants
 */
public class Dossier {
    //private static final long serialVersionUID = -1278065948568147114L;

    private String numeroDossier; // Numéro du dossier exemple DP 086 XXX 16 X0001
    private String nomDemandeur; // Nom du demandeur
    private String etatDossier; // Etat du dossier
    private String annee; // Année d'enregistrement du dossier
    private String type; // Type du dossier

    private String numeroDossierRapide; // Le numéro du dossier rapide exemple 001 pour DP 086 XXX 16 H4001

    /**
     * Constructeur de confort prenant en paramètre un numéro de dossier, un nom de demandeur et un état pour le dossier
     * @param numeroDossier Numéro du dossier exemple DP 086 XXX 16 X0001
     * @param nomDemandeur Nom du demandeur
     * @param etatDossier Etat du dossier
     */
    Dossier(String numeroDossier, String nomDemandeur, String etatDossier) {
        this.numeroDossier = numeroDossier.trim();
        this.nomDemandeur = nomDemandeur.trim();
        this.etatDossier = etatDossier.trim();
        this.numeroDossierRapide = this.numeroDossier.substring(15,19);
        this.type = this.numeroDossier.substring(0,2);
        this.annee = this.numeroDossier.substring(11,13);
    }

    @Override
    public String toString() {
        return numeroDossier + "," + nomDemandeur + "," + etatDossier + "\n";
    }

    /**
     * Retourne le type du dossier
     * @return Le type du dossier
     */
    public String getType() {
        return type;
    }

    /**
     * Retourne l'année où le dossier a été enregistré sur ADS 2007
     * @return L'année du dossier
     */
    public String getAnnee() {
        return annee;
    }

    /**
     * Retourne le numéro du dossier
     * @return Le numéro du dossier
     */
    public String getNumeroDossier() {
        return numeroDossier;
    }

    /**
     * Fonction permettant de changer le numéro du dossier
     * @param numeroDossier Le nouveau numéro de dossier
     */
    public void setNumeroDossier(String numeroDossier) {
        this.numeroDossier = numeroDossier;
        this.numeroDossierRapide = this.numeroDossier.substring(15,19);
    }

    /**
     * Fonction qui retourne le nom du demandeur du dossier
     * @return Le nom du demandeur/redevable du dossier
     */
    public String getNomDemandeur() {
        return nomDemandeur;
    }

    /**
     * Fonction qui permet de modifier le nom du demandeur du dossier
     * @param nomDemandeur Le nouveau nom du demandeur
     */
    public void setNomDemandeur(String nomDemandeur) {
        this.nomDemandeur = nomDemandeur;
    }

    /**
     * Fonction qui retourne l'état du dossier
     * @return L'état du dossier
     */
    public String getEtatDossier() {
        return etatDossier;
    }

    /**
     * Fonction qui permet de changer l'état du dossier
     * @param etatDossier Le nouvel état du dossier
     */
    public void setEtatDossier(String etatDossier) {
        this.etatDossier = etatDossier;
    }

    /**
     * Fonction qui retourne le numéro de dossier rapide
     * @return Le numéro de dossier rapide exemple 001 pour DP 086 XXX 16 H4001
     */
    String getNumeroDossierRapide() {
        return numeroDossierRapide;
    }

    /**
     * Cette fonction permet de définir le choix des attributs à enregistrer lors de la sérialisation
     * @param objectOutputStream L'objectOutputStream qui écrit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de l'écrire ou que l'objectOutputStream est null
     */
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(numeroDossier);
        objectOutputStream.flush();
        objectOutputStream.writeObject(nomDemandeur);
        objectOutputStream.flush();
        objectOutputStream.writeObject(etatDossier);
        objectOutputStream.flush();
    }

    /**
     * Cette fonction permet de lire l'objet précédemment enregistré correctement
     * @param objectInputStream L'objectInputStream qui lit les données dans le fichier de sérialisation
     * @throws IOException Si un problème a lieu lors de la lecture ou que l'objectInputStream est null
     * @throws ClassNotFoundException Si la classe de cast n'existe pas
     */
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        setNumeroDossier((String) objectInputStream.readObject());
        setNomDemandeur((String) objectInputStream.readObject());
        setEtatDossier((String) objectInputStream.readObject());

        this.numeroDossierRapide = this.numeroDossier.substring(15,19);
        this.type = this.numeroDossier.substring(0,2);
        this.annee = this.numeroDossier.substring(11,13);
    }
}

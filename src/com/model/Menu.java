package com.model;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

public abstract class Menu {
    private static String menu = "Menu";

    private static String dossiers = "Gestion des dossiers";
    private static String communes = "Gestion des communes";
    private static String taxes = "Information sur les taxes";
    private static String formatage = "Formatage";
    private static String app = "Gestion de l'application";
    private static String doc = "Documentation";

    private static String conversion = "Conversion de fichiers CSV -> XLS";
    private static String formatageFichier = "Formatage de fichiers pour importation";

    private static String importFiles = "Importer des dossiers";
    private static String displayFiles = "Visualisation des dossiers";
    private static String displayPivotTables = "Edition de tableaux croisés";
    private static String editFiles = "Modifier un dossier";
    private static String deleteYearFiles = "Supprimer une année de dossiers";
    private static String deleteFile = "Supprimer un dossier";

    private static String importCommunes = "Importer des communes";
    private static String addCommune = "Ajouter une commune";
    private static String editCommune = "Modifier une commune";
    private static String deleteCommune = "Supprimer une commune";
    private static String importException = "Importer une liste d'exception";
    private static String addException = "Ajouter une exception";
    private static String deleteException = "Supprimer une exception";

    private static String importTaxes = "Importer des montants";
    private static String displayTaxes = "Visualisation de montants";
    private static String displayPTTaxes = "Edition de tableaux croisés";
    private static String deleteYearTaxes = "Supprimer une année de montants";

    private static String appGest = "Gestion de l'entête d'exportation";
    private static String deleteData = "Supprimer l'intégralité des données";
    private static String logs = "Afficher les logs";

    public Menu() {

    }

    public static String getMenu() {
        return menu;
    }

    public static String getDossiers() {
        return dossiers;
    }

    public static String getCommunes() {
        return communes;
    }

    public static String getTaxes() {
        return taxes;
    }

    public static String getFormatage() {
        return formatage;
    }

    public static String getApp() {
        return app;
    }

    public static String getDoc() {
        return doc;
    }

    public static String getImportException() {
        return importException;
    }

    public static String getConversion() {
        return conversion;
    }

    public static String getFormatageFichier() {
        return formatageFichier;
    }

    public static String getImportFiles() {
        return importFiles;
    }

    public static String getDisplayFiles() {
        return displayFiles;
    }

    public static String getDisplayPivotTables() {
        return displayPivotTables;
    }

    public static String getEditFiles() {
        return editFiles;
    }

    public static String getDeleteYearFiles() {
        return deleteYearFiles;
    }

    public static String getDeleteFile() {
        return deleteFile;
    }

    public static String getImportCommunes() {
        return importCommunes;
    }

    public static String getAddCommune() {
        return addCommune;
    }

    public static String getEditCommune() {
        return editCommune;
    }

    public static String getDeleteCommune() {
        return deleteCommune;
    }

    public static String getAddException() {
        return addException;
    }

    public static String getDeleteException() {
        return deleteException;
    }

    public static String getImportTaxes() {
        return importTaxes;
    }

    public static String getDisplayTaxes() {
        return displayTaxes;
    }

    public static String getDisplayPTTaxes() {
        return displayPTTaxes;
    }

    public static String getDeleteYearTaxes() {
        return deleteYearTaxes;
    }

    public static String getAppGest() {
        return appGest;
    }

    public static String getDeleteData() {
        return deleteData;
    }

    public static String getLogs() {
        return logs;
    }

    /**
     * Fonction de création de l'arborescence de l'application
     *
     * @return L'arborescence de l'application
     */
    public static DefaultTreeModel toJTree() {
        DefaultMutableTreeNode model = new DefaultMutableTreeNode(menu);

        DefaultMutableTreeNode dossiers = new DefaultMutableTreeNode(Menu.dossiers);
        DefaultMutableTreeNode communes = new DefaultMutableTreeNode(Menu.communes);
        DefaultMutableTreeNode taxes = new DefaultMutableTreeNode(Menu.taxes);
        DefaultMutableTreeNode formatage = new DefaultMutableTreeNode(Menu.formatage);
        DefaultMutableTreeNode app = new DefaultMutableTreeNode(Menu.app);
        DefaultMutableTreeNode doc = new DefaultMutableTreeNode(Menu.doc);

        formatage.add(new DefaultMutableTreeNode(Menu.formatageFichier));
        formatage.add(new DefaultMutableTreeNode(Menu.conversion));

        dossiers.add(new DefaultMutableTreeNode(Menu.importFiles));
        dossiers.add(new DefaultMutableTreeNode(Menu.displayFiles));
        dossiers.add(new DefaultMutableTreeNode(Menu.displayPivotTables));
        dossiers.add(new DefaultMutableTreeNode(Menu.editFiles));
        dossiers.add(new DefaultMutableTreeNode(Menu.deleteYearFiles));
        dossiers.add(new DefaultMutableTreeNode(Menu.deleteFile));

        communes.add(new DefaultMutableTreeNode(Menu.importCommunes));
        communes.add(new DefaultMutableTreeNode(Menu.addCommune));
        communes.add(new DefaultMutableTreeNode(Menu.editCommune));
        communes.add(new DefaultMutableTreeNode(Menu.deleteCommune));
        communes.add(new DefaultMutableTreeNode(Menu.importException));
        communes.add(new DefaultMutableTreeNode(Menu.addException));
        communes.add(new DefaultMutableTreeNode(Menu.deleteException));

        taxes.add(new DefaultMutableTreeNode(Menu.importTaxes));
        taxes.add(new DefaultMutableTreeNode(Menu.displayTaxes));
        taxes.add(new DefaultMutableTreeNode(Menu.displayPTTaxes));
        taxes.add(new DefaultMutableTreeNode(Menu.deleteYearTaxes));

        app.add(new DefaultMutableTreeNode(Menu.appGest));
        app.add(new DefaultMutableTreeNode(Menu.deleteData));
        app.add(new DefaultMutableTreeNode(Menu.logs));

        model.add(communes);
        model.add(dossiers);
        model.add(taxes);
        model.add(formatage);
        model.add(app);
        model.add(doc);

        return new DefaultTreeModel(model);
    }

    public static class MyRenderer extends DefaultTreeCellRenderer {
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            JLabel component = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            if (!leaf) {
                component.setForeground(Color.RED);
                ImageIcon icon = new ImageIcon(".\\resources\\png\\next.png");
                setIcon(icon);
            } else {
                setTextSelectionColor(Color.GRAY);
                setTextNonSelectionColor(Color.BLACK);
                setBackgroundSelectionColor(Color.WHITE);
                setIcon(new ImageIcon(".\\resources\\png\\chevron.png"));
            }

            return component;
        }
    }
}

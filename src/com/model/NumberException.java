package com.model;

public class NumberException {
    private String number;
    private String type;

    public NumberException(String number, String type) {
        this.type = type;
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }
}

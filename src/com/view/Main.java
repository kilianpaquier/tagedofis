package com.view;

import com.controler.Controler;
import com.view.java.Notification;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File cheminCourant = null;
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            cheminCourant = new File(".").getCanonicalFile();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException | IOException e) {
            System.err.println(e.getMessage());
        }

        if (cheminCourant != null && !new File(cheminCourant + "\\TAGEDOFISlock.lock").exists()) {
            Controler controler = new Controler("TAGEDOFIS");
            controler.getView().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    controler.serializeData(true);
                }
            });
            controler.getView().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            controler.getView().pack();
            //controler.getView().setSize(1300, 800);
            controler.getView().setExtendedState(JFrame.MAXIMIZED_BOTH);
            controler.getView().setLocationRelativeTo(null);
            controler.getView().setTitle(controler.getAppTitle());
            controler.getView().setVisible(true);
            try {
                new File(cheminCourant + "\\TAGEDOFISlock.lock").createNewFile();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        } else {
            Controler controler = new Controler("TAGEDOFIS");
            controler.getView().addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    controler.serializeData(false);
                }
            });
            controler.getView().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            controler.getView().pack();
            //controler.getView().setSize(1000, 800);
            controler.getView().setExtendedState(JFrame.MAXIMIZED_BOTH);
            controler.getView().setLocationRelativeTo(null);
            controler.getView().setTitle(controler.getAppTitle());
            controler.getView().getDataBox().setSelected(false);
            controler.getView().getDataBox().setEnabled(false);
            controler.getView().setVisible(true);
            new Notification(controler.getView(), "Information", "Instance ouverte en lecture seulement");
        }
    }
}

package com.view.java;

import javax.swing.*;

public class ViewAddCommune extends JDialog {
    private JPanel globalPanel;
    private JTextField nomCommuneField;
    private JTextField numeroINSEEField;
    private JTextField departementField;
    private JButton ajouterLaCommuneButton;
    private JCheckBox EPCICheckBox;
    private JButton documentationButton;

    public ViewAddCommune(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
        openDialog(view);
    }

    private void openDialog(JFrame view) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(500, 325);
        setLocationRelativeTo(view);
        setVisible(true);
    }

    public JTextField getNomCommuneField() {
        return nomCommuneField;
    }

    public JTextField getNumeroINSEEField() {
        return numeroINSEEField;
    }

    public JTextField getDepartementField() {
        return departementField;
    }

    public JButton getAjouterLaCommuneButton() {
        return ajouterLaCommuneButton;
    }

    public JCheckBox getEPCICheckBox() {
        return EPCICheckBox;
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }

}

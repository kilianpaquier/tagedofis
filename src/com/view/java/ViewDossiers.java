package com.view.java;

import javax.swing.*;

public class ViewDossiers extends JDialog {
    private JPanel globalPanel;
    private JComboBox<String> communeBox;
    private JComboBox<String> yearBox;
    private JCheckBox DPCheckBox;
    private JCheckBox PCCheckBox;
    private JCheckBox PACheckBox;
    private JRadioButton manquantsRadioButton;
    private JButton boutonVisualiser;
    private JRadioButton enregistresRadioButton;
    private JCheckBox checkBoxCommunes;
    private JCheckBox checkBoxAnnee;
    private JComboBox<String> yearBoxDeux;
    private JCheckBox communesEPCICheckBox;
    private JCheckBox nonTaxablesCheckBox;
    private JButton documentationButton;
    private JLabel anneeLabel;
    private JRadioButton detailTITMTRRadioButton;
    private JRadioButton detailTARAPRadioButton;
    private JRadioButton inexistantsRadioButton;

    public ViewDossiers(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
        ButtonGroup group = new ButtonGroup();
        group.add(manquantsRadioButton);
        group.add(enregistresRadioButton);
        group.add(inexistantsRadioButton);

        ButtonGroup group2 = new ButtonGroup();
        group2.add(detailTARAPRadioButton);
        group2.add(detailTITMTRRadioButton);
    }

    public void ouvrir() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(800, 500);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JRadioButton getDetailTARAPRadioButton() {
        return detailTARAPRadioButton;
    }

    public JRadioButton getDetailTITMTRRadioButton() {
        return detailTITMTRRadioButton;
    }

    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    public JComboBox<String> getCommuneBox() {
        return communeBox;
    }

    public JComboBox<String> getYearBox() {
        return yearBox;
    }

    public JCheckBox getDPCheckBox() {
        return DPCheckBox;
    }

    public JCheckBox getPCCheckBox() {
        return PCCheckBox;
    }

    public JCheckBox getPACheckBox() {
        return PACheckBox;
    }

    public JRadioButton getManquantsRadioButton() {
        return manquantsRadioButton;
    }

    public JButton getBoutonVisualiser() {
        return boutonVisualiser;
    }

    public JRadioButton getEnregistresRadioButton() {
        return enregistresRadioButton;
    }

    public JComboBox<String> getYearBoxDeux() {
        return yearBoxDeux;
    }

    public JCheckBox getCheckBoxAnnee() {
        return checkBoxAnnee;
    }

    public JCheckBox getCheckBoxCommunes() {
        return checkBoxCommunes;
    }

    public JCheckBox getCommunesEPCICheckBox() {
        return communesEPCICheckBox;
    }

    public JCheckBox getNonTaxablesCheckBox() {
        return nonTaxablesCheckBox;
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }

    public JLabel getAnneeLabel() {
        return anneeLabel;
    }

    public JRadioButton getInexistantsRadioButton() {
        return inexistantsRadioButton;
    }
}

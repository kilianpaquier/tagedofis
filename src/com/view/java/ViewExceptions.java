package com.view.java;

import javax.swing.*;

public class ViewExceptions extends JDialog {
    private JButton exceptionButton;
    private JComboBox<String> boxCommunes;
    private JTextField fieldNumber;
    private JComboBox<String> boxType;
    private JPanel globalPanel;
    private JButton documentationButton;

    public ViewExceptions(JFrame parent, String title, boolean modal) {
        super(parent, title, modal);
        setContentPane(globalPanel);
    }

    public JButton getExceptionButton() {
        return exceptionButton;
    }

    public JComboBox<String> getBoxCommunes() {
        return boxCommunes;
    }

    public JComboBox<String> getBoxType() {
        return boxType;
    }

    public JTextField getFieldNumber() {
        return fieldNumber;
    }

    public void ouverture() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(600, 325);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }
}

package com.view.java;

import javax.swing.*;

public class ViewImport extends JDialog {
    private JPanel globalPanel;
    private JButton fileChooserButton;
    private JComboBox<String> comboBoxAnnee;
    private JButton importerButton;
    private JTextField textFieldFiles;
    private JLabel fieldAnnee;
    private JLabel fieldFichier;
    private JButton documentationButton;

    public ViewImport(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
    }

    public ViewImport(JDialog view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
    }

    public void ouvrir() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(500, 300);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void ouvrirPetit() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(500, 225);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JPanel getGlobalPanel() {
        return globalPanel;
    }

    public JButton getFileChooserButton() {
        return fileChooserButton;
    }

    public JComboBox<String> getComboBoxAnnee() {
        return comboBoxAnnee;
    }

    public JButton getImporterButton() {
        return importerButton;
    }

    public JTextField getTextFieldFiles() {
        return textFieldFiles;
    }

    public JLabel getFieldAnnee() {
        return fieldAnnee;
    }

    public JLabel getFieldFichier() {
        return fieldFichier;
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }

}

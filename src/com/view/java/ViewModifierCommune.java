package com.view.java;

import javax.swing.*;
public class ViewModifierCommune extends JDialog {
    private JPanel globalPanel;
    private JComboBox<String> communeBox;
    private JTextField nomField;
    private JTextField numeroINSEEField;
    private JTextField departementField;
    private JButton modifierLaCommuneButton;

    public ViewModifierCommune(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
        openDialog(view);
    }

    private void openDialog(JFrame view) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(600, 400);
        setLocationRelativeTo(view);
        setVisible(true);
    }

    public JComboBox<String> getCommuneBox() {
        return communeBox;
    }

    public JTextField getNomField() {
        return nomField;
    }

    public JTextField getNumeroINSEEField() {
        return numeroINSEEField;
    }

    public JTextField getDepartementField() {
        return departementField;
    }

    public JButton getModifierLaCommuneButton() {
        return modifierLaCommuneButton;
    }

}

package com.view.java;

import javax.swing.*;

public class ViewModifierDossier extends JDialog {
    private JPanel globalPanel;
    private JTextField numeroDossierField;
    private JTextField demandeurField;
    private JTextField etatDossierField;
    private JTextField rechercherField;
    private JButton rechercherButton;
    private JButton modifierButton;
    private JCheckBox modifiableCheckBoxNum;
    private JCheckBox modifiableCheckBoxDemandeur;
    private JCheckBox modifiableCheckBoxEtat;
    private JTextField numDossierField;
    private JButton documentationButton;

    public ViewModifierDossier(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
    }

    public JTextField getDemandeurField() {
        return demandeurField;
    }

    public JTextField getNumeroDossierField() {
        return numeroDossierField;
    }

    public JTextField getEtatDossierField() {
        return etatDossierField;
    }

    public JTextField getRechercherField() {
        return rechercherField;
    }

    public JButton getRechercherButton() {
        return rechercherButton;
    }

    public JButton getModifierButton() {
        return modifierButton;
    }

    public JCheckBox getModifiableCheckBoxNum() {
        return modifiableCheckBoxNum;
    }

    public JCheckBox getModifiableCheckBoxDemandeur() {
        return modifiableCheckBoxDemandeur;
    }

    public JCheckBox getModifiableCheckBoxEtat() {
        return modifiableCheckBoxEtat;
    }

    public JTextField getNumDossierField() {
        return numDossierField;
    }

    public JButton getDocumentationButton() {
        return documentationButton;
    }

}

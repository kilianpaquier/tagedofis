package com.view.java;

import javax.swing.*;

public class ViewParam extends JDialog {
    private JPanel globalPanel;
    private JTextField textFieldEntete;
    private JTextField textFieldProvenance;
    private JButton modifierLEnteteDButton;
    private JTextField textFieldTitre;
    private JTextField textFieldTitre2;
    private JLabel fieldVersion;

    public ViewParam(JFrame view, String name, boolean bool) {
        super(view, name, bool);
        setContentPane(globalPanel);
        openDialog(view);
    }

    private void openDialog(JFrame view) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setSize(700, 400);
        setLocationRelativeTo(view);
        setVisible(true);
    }

    public JTextField getTextFieldEntete() {
        return textFieldEntete;
    }

    public JTextField getTextFieldProvenance() {
        return textFieldProvenance;
    }

    public JButton getModifierLEnteteDButton() {
        return modifierLEnteteDButton;
    }

    public JTextField getTextFieldTitre() {
        return textFieldTitre;
    }

    public JTextField getTextFieldTitre2() {
        return textFieldTitre2;
    }

    public JLabel getFieldVersion() {
        return fieldVersion;
    }

}

package com.view.java;

import javax.swing.*;

public class ViewSave extends JFrame {
    private JPanel globalPanel;
    private JProgressBar progressBar;
    private JLabel label;
    private Timer timer;

    public ViewSave(String title) {
        super(title);
        setContentPane(globalPanel);
        setIconImage(new ImageIcon(".\\resources\\icon.png").getImage());

        timer = new Timer(500, e -> {
            if (label.getText().contains("..."))
                label.setText("Sauvegarde des données veuillez patienter .");
            else
                label.setText(label.getText() + ".");
        });
        setUndecorated(true);

    }

    public void open() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setSize(450, 75);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public JLabel getLabel() {
        return label;
    }

    public Timer getTimer() {
        return timer;
    }

}
